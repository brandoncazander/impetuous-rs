use std::{fs, path::PathBuf};

use anyhow::{Context, Result};

use crate::{
    cli::{Imprint, Styles},
    client, conf, domain,
    migrations::{needs_migrations, Migration, MIGRATIONS_SCHEMA},
    time, tracking,
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to read configuration from {0}", path.display())]
    InvalidConfig {
        path: PathBuf,
        source: anyhow::Error,
    },
    #[error("no {what} path found; try setting {env} in an environment variable?")]
    NoPath {
        what: &'static str,
        env: &'static str,
    },
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

impl Error {
    fn no_path(what: &'static str, env: &'static str) -> Self {
        Error::NoPath { what, env }
    }
}

lazy_static::lazy_static! {
    // TODO move to domain?
    pub static ref MIGRATIONS: Vec<Migration<&'static str>> =
        vec![
            Migration::new("pre-init", MIGRATIONS_SCHEMA),
            Migration::new("init", include_str!("../../sql/00-init.sql")),
            Migration::new("posting", include_str!("../../sql/01-posting.sql")),
        ];
}

static DEFAULT_CONFIG_NAME: &'static str = "config.toml";
static DEFAULT_SECRETS_NAME: &'static str = "secrets.toml";
static DEFAULT_DB_NAME: &'static str = "impetuous2.sqlite";

fn env_var<K: AsRef<std::ffi::OsStr>>(key: K) -> Result<Option<String>> {
    use std::env::{self, VarError::*};
    match env::var(key) {
        Ok(var) => Ok(Some(var)),
        Err(NotPresent) => Ok(None),
        Err(e) => Err(e.into()),
    }
}

#[derive(Debug)]
pub struct Impetuous {
    /// used to open the database? TODO move the database/migration stuff somewhere else?
    pub db_path: PathBuf,
    pub config_path: PathBuf,
    pub log: slog::Logger,
    pub config: conf::Config,
    pub secrets: conf::Secrets,
    pub tz: chrono_tz::Tz,
}

impl Impetuous {
    pub fn from_system() -> Result<Self, Error> {
        let db_path = env_var("IM_DB")?
            .map(PathBuf::from)
            .or_else(|| dirs::data_dir().map(|d| d.join("impetuous").join(DEFAULT_DB_NAME)))
            .ok_or(Error::no_path("database", "IM_DB"))?;

        let config_path = env_var("IM_CONFIG")?
            .map(PathBuf::from)
            .or_else(|| dirs::config_dir().map(|d| d.join("impetuous").join(DEFAULT_CONFIG_NAME)))
            .ok_or(Error::no_path("config", "IM_CONFIG"))?;

        let secrets_path = env_var("IM_SECRETS")?
            .map(PathBuf::from)
            .or_else(|| dirs::config_dir().map(|d| d.join("impetuous").join(DEFAULT_SECRETS_NAME)))
            .ok_or(Error::no_path("secrets", "IM_SECRETS"))?;

        let config = conf::read_or_write(&config_path, || conf::ExampleConfig).map_err(|e| {
            Error::InvalidConfig {
                path: config_path.clone(),
                source: e,
            }
        })?;

        let secrets = conf::read_or_write_default(&secrets_path)
            .with_context(|| format!("failed to read secrets from {}:", secrets_path.display()))?;

        let tz = crate::time::current_tz().context("error getting timezone")?;

        Ok(Impetuous {
            log: crate::logging::please().clone(),
            db_path,
            config_path,
            config,
            secrets,
            tz,
        })
    }

    pub fn log(&self) -> &slog::Logger {
        &self.log
    }

    /// Create and return a new [Imprint].
    pub fn print(&self) -> Imprint<chrono_tz::Tz> {
        // TODO take a AsRawFd? as an arg
        let use_colors = self
            .config
            .colors
            .unwrap_or_else(|| termion::is_tty(&std::io::stdout()));
        if use_colors {
            Imprint::from_im(&self)
        } else {
            Imprint {
                style: Styles::unstyled(),
                ..Imprint::from_im(&self)
            }
        }
    }

    pub fn now_utc(&self) -> time::DateTime {
        chrono::Utc::now().into()
    }

    pub fn now_local(&self) -> chrono::DateTime<chrono_tz::Tz> {
        chrono::Utc::now().with_timezone(&self.tz)
    }

    pub fn tokio(&self) -> tokio::io::Result<tokio::runtime::Runtime> {
        use tokio::runtime;
        runtime::Builder::new()
            .enable_io()
            .enable_time()
            .basic_scheduler()
            .build()
    }

    pub fn open_ledger(
        &self,
        posting: &domain::Posting,
    ) -> anyhow::Result<Box<dyn tracking::LedgerErased + Send + Sync>> {
        let section = posting.source.as_ref().unwrap();
        let uuid = posting.uuid.unwrap();
        let config = self.config();

        let i = config
            .integrations
            .get(section)
            .ok_or_else(|| anyhow::Error::msg(format!("config missing for [ext.{}]", section)))?;

        let secrets = self
            .secrets()
            .get(section)
            .ok_or_else(|| anyhow::Error::msg(format!("secrets missing for [{}]", section)))?;

        i.open_ledger(secrets.clone())
    }

    pub fn block_on<F>(&self, fut: F) -> F::Output
    where
        F: std::future::Future + Send,
        F::Output: Send,
    {
        let mut rt = self.tokio().expect("tokio runtime");
        let local = tokio::task::LocalSet::new();
        local.block_on(&mut rt, fut)
    }

    pub fn client<'a>(&'a self, uri: Option<hyper::Uri>) -> anyhow::Result<client::Client<'a>> {
        let log = crate::logging::please().clone();
        let im = self;
        let via = match uri {
            None => self.db().map(|db| client::Via::InProcess { db })?,
            Some(uri) => client::Via::http(uri),
        };
        Ok(client::Client { log, im, via })
    }

    fn db_unmigrated(&self) -> anyhow::Result<rusqlite::Connection> {
        if let Some(parent) = self.db_path.parent() {
            fs::create_dir_all(parent).with_context(|| format!("create {}", parent.display()))?
        }
        let mut db = rusqlite::Connection::open(&self.db_path)?;
        db.pragma_update(None, "foreign_keys", &true)?;
        crate::sql_funks::add_functions(&mut db)?;
        Ok(db)
    }

    /// Open and return a SQLite database connection. Fails if migrations need to be run.
    pub fn db(&self) -> anyhow::Result<rusqlite::Connection> {
        let mut db = self.db_unmigrated()?;

        let migrations = needs_migrations(&mut db, MIGRATIONS.as_slice())
            .context("problem checking the database version")?;

        if let Some(m) = migrations.map(|m| m.display()) {
            Err(anyhow::Error::msg(format!(
                "Migrations ({}) must be run before this database can be used.",
                m
            )))
        } else {
            Ok(db)
        }
    }

    /// TODO move this and the migration stuff into cli or a different object or something?
    pub fn run_migrations_if_any(&self) -> anyhow::Result<()> {
        let mut db = self.db_unmigrated().context("opening database")?;

        let migrations = needs_migrations(&db, MIGRATIONS.as_slice())
            .context("Error checking the database version.")?;

        let migrations = match migrations {
            Some(migrations) => migrations,
            None => return Ok(()),
        };

        if migrations.len() == MIGRATIONS.len() {
            let meta = self
                .db_path
                .metadata()
                .context("check database file size")?;

            if meta.len() == 0 {
                // new database - skip interactivity - report only errors

                let tx = db.transaction()?;
                for (version, result) in migrations.start(&tx) {
                    result.with_context(|| format!("failed migration `{}`", version))?;
                }
                tx.commit()?;

                if let Err((_, err)) = db.close() {
                    return Err(anyhow::Error::new(err).context("closing database"));
                }

                return Ok(());
            }
        }

        let imprint = self.print();

        println!(
            "A database {} appears to be {} before use. ({})",
            imprint.info("migration"),
            imprint.bad("necessary"),
            migrations.display(),
        );
        println!("The database is at: {}", imprint.path(&self.db_path));

        let path = self
            .db_path
            .parent()
            .ok_or_else(|| anyhow::Error::msg("could not get database path parent directory?"))?
            .join(format!("impetuous2-{}.sqlite", self.now_utc().to_rfc3339()));

        println!("I will copy the database to: {}", imprint.path(&path));
        println!(
            "If any other programs are using this database right now, they and/or I will {}",
            imprint.underline("probably have a bad time."),
        );

        if !dialoguer::Confirm::new()
            .with_prompt(
                &imprint
                    .ask("Would you like to perform the migration?")
                    .to_string(),
            )
            .default(false)
            .interact()?
        {
            println!("Good, I didn't want migrate your database either.");
            return Err(anyhow::Error::msg("database migration declined"));
        }

        std::fs::copy(&self.db_path, &path).context("copy database file")?;
        // todo fsync the parent directory or something

        eprintln!("Copy complete...");

        let tx = db.transaction()?;
        for (version, result) in migrations.start(&tx) {
            result.with_context(|| format!("failed migration `{}`", version))?;
            eprintln!(
                "- migration to {} was a {}",
                imprint.info(version),
                imprint.good("success"),
            );
        }
        tx.commit()?;

        eprintln!("Migrations {}...", imprint.good("complete"));

        if let Err((_, err)) = db.close() {
            return Err(anyhow::Error::new(err).context("closing database"));
        }

        Ok(())
    }

    pub fn config(&self) -> &conf::Config {
        &self.config
    }

    pub fn config_mut(&mut self) -> &mut conf::Config {
        &mut self.config
    }

    pub fn secrets(&self) -> &conf::Secrets {
        &self.secrets
    }
}
