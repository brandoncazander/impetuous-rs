//! Parsing imql.
//!
//! TODO handle dotted/qualified notation?
//!
//! https://github.com/Geal/nom/blob/master/examples/json.rs
use super::*;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while},
    character::complete::{char, digit1 as digits, multispace0},
    combinator::{cut, map, map_res, opt, recognize},
    error::{context, ParseError},
    multi::{many0, separated_list},
    sequence::{preceded, separated_pair, terminated, tuple},
    IResult,
};

#[cfg(test)]
use nom::{
    combinator::all_consuming,
    error::{convert_error, VerboseError},
    Err,
};

pub fn parse_i64<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, i64, E> {
    map_res(recognize(tuple((opt(char('-')), digits))), |s: &str| {
        s.parse::<i64>()
    })(i)
}

pub fn parse_u64<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, u64, E> {
    map_res(digits, |s: &str| s.parse::<u64>())(i)
}

pub fn identifier<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    let ident_char = |c: char| {
        !c.is_ascii() || c.is_ascii_alphanumeric() || c == '-' || c == '_' || c == '/' || c == ':'
    };
    take_while(ident_char)(i)
}

pub fn param<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, ParamExpr<&'a str>, E> {
    map(preceded(char('$'), cut(identifier)), ParamExpr)(i)
}

pub fn field<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, FieldExpr<&'a str>, E> {
    map(identifier, FieldExpr)(i)
}

pub fn simple_expr<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, SimpleExpr<&'a str>, E> {
    alt((
        // TODO a lot over laps, if user models have fields named null,true,false...
        map(tag("null"), |_| SimpleExpr::Null),
        map(tag("true"), |_| SimpleExpr::Bool(true)),
        map(tag("false"), |_| SimpleExpr::Bool(false)),
        map(param, SimpleExpr::Param),
        map(field, SimpleExpr::Field),
    ))(i)
}

pub fn comparison_expr<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, ComparisonExpr<&'a str>, E> {
    let compare = alt((
        map(tag(".eq."), |_| ComparisonOp::Eq),
        map(tag(".not.eq."), |_| ComparisonOp::Ne),
        map(tag(".gt."), |_| ComparisonOp::Gt),
        map(tag(".ge."), |_| ComparisonOp::Ge),
        map(tag(".lt."), |_| ComparisonOp::Lt),
        map(tag(".le."), |_| ComparisonOp::Le),
        map(tag(".is.not."), |_| ComparisonOp::IsNot),
        map(tag(".is."), |_| ComparisonOp::Is),
    ));

    // TODO this is werid because eventually field should be able to use dot notation, but this
    // mixes up operator parsing and stuff
    map(tuple((field, compare, simple_expr)), |(lh, op, rh)| {
        ComparisonExpr(lh, op, rh)
    })(i)
}

pub fn logic_expr<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, LogicExpr<&'a str>, E> {
    let ws = multispace0;

    map(
        tuple((
            alt((
                map(tag("and("), |_| LogicOp::And),
                map(tag("or("), |_| LogicOp::Or),
            )),
            cut(terminated(
                // separated_list(preceded(ws, char(',')), complex_expr),
                // FIXME
                separated_pair(
                    complex_expr,
                    preceded(ws, char(',')),
                    preceded(ws, complex_expr),
                ),
                preceded(ws, char(')')),
            )),
        )),
        |(op, (a, b))| LogicExpr(op, vec![a, b]),
    )(i)
}

pub fn complex_expr<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, ComplexExpr<&'a str>, E> {
    context(
        "expr",
        alt((
            map(logic_expr, ComplexExpr::Logic),
            map(comparison_expr, ComplexExpr::Comparison),
        )),
    )(i)
}

/// {foo,bar}
pub fn shape<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Vec<Query<&'a str>>, E> {
    let ws = multispace0;
    let shape = preceded(
        char('{'),
        cut(terminated(
            separated_list(preceded(ws, char(',')), preceded(ws, query)),
            preceded(ws, char('}')),
        )),
    );
    shape(i)
}

/// looks like ..[0-9]+ or ..$param
pub fn limit<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Limit<&'a str>, E> {
    let ws = multispace0;
    let limit = alt((map(parse_u64, Limit::N), map(param, Limit::Param)));
    preceded(tag(".."), preceded(ws, limit))(i)
}

/// sort(foo,bar)
pub fn ordering<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, Vec<OrderExpr<&'a str>>, E> {
    let ws = multispace0;
    let dir = map(opt(char('-')), |dir| {
        dir.map(|_| Direction::Desc).unwrap_or(Direction::Asc)
    });
    let order_field = tuple((dir, preceded(ws, field)));
    preceded(
        tag("sort("),
        cut(terminated(
            separated_list(preceded(ws, char(',')), preceded(ws, order_field)),
            preceded(ws, char(')')),
        )),
    )(i)
}

/// ? foo.eq.bar
pub fn filter<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, ComplexExpr<&'a str>, E> {
    let ws = multispace0;
    preceded(char('?'), preceded(ws, cut(complex_expr)))(i)
}

// /// + $foo
// pub fn mutation<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Mutation<&'a str>, E> {
//     let ws = multispace0;
//     alt((
//         map(preceded(char('+'), preceded(ws, cut(param))), |expr| {
//             Mutation::Upsert { expr }
//         }),
//         map(preceded(char('!'), preceded(ws, cut(param))), |expr| {
//             Mutation::Delete { expr }
//         }),
//     ))(i)
// }

/// Doesn't deal well with leading or trailing whitespace, so call trim on the thing you pass
/// first.
///
/// An identifier followed by zero or more of the following:
///     ?filter
///     {shape}
///     sort(...)
///     ..limit
///
/// what about arguments?
///   metrics{temperature C}
/// I guess for complex cases, arguments might be documents?
/// Also, we liked {} for gathers because it looks like shells;
/// but we also like {} for documents because it looks like ELM...
pub fn query<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Query<&'a str>, E> {
    let ws = multispace0;

    let pattern = alt((
        map(filter, Pattern::Filter),
        map(shape, Pattern::Shape),
        map(limit, Pattern::Limit),
        map(ordering, Pattern::Order),
        // map(mutation, Pattern::Mutation),
    ));

    map(
        tuple((identifier, many0(preceded(ws, pattern)))),
        |(on, patterns)| Query { on, patterns },
    )(i)
}

#[cfg(test)]
fn must_parse<'a, T, F>(func: F, data: &'a str) -> (&'a str, T)
where
    T: 'a,
    F: FnOnce(&'a str) -> Result<(&'a str, T), nom::Err<VerboseError<&'a str>>>,
{
    func(data)
        .map_err(|e| match e {
            Err::Error(e) | Err::Failure(e) => panic!("{}", convert_error(data, e)),
            _ => e,
        })
        .unwrap()
}

#[test]
fn test_complex_expr() {
    use ComparisonOp::*;
    let data = r#"and(started.lt.$until,or(ended.ge.$since,ended.is.null))"#;
    assert_eq!(
        must_parse(complex_expr::<VerboseError<&str>>, data),
        (
            "",
            LogicExpr(
                LogicOp::And,
                vec![
                    ComparisonExpr(FieldExpr("started"), Lt, ParamExpr("until").into()).into(),
                    LogicExpr(
                        LogicOp::Or,
                        vec![
                            ComparisonExpr(FieldExpr("ended"), Ge, ParamExpr("since").into())
                                .into(),
                            ComparisonExpr(FieldExpr("ended"), Is, SimpleExpr::Null).into(),
                        ]
                    )
                    .into(),
                ]
            )
            .into()
        )
    );
}

#[test]
fn test_limit() {
    assert_eq!(
        must_parse(all_consuming(limit::<VerboseError<&str>>), "..123",),
        ("", Limit::N(123))
    );
    assert_eq!(
        must_parse(all_consuming(limit::<VerboseError<&str>>), "..$derp",),
        ("", Limit::Param(ParamExpr("derp")))
    );
}

#[test]
fn test_query() {
    use ComparisonOp::*;
    assert_eq!(
        must_parse(
            all_consuming(query::<VerboseError<&str>>),
            "articles?slug.eq.$derp"
        ),
        (
            "",
            Query {
                patterns: vec![Pattern::Filter(
                    ComparisonExpr(FieldExpr("slug"), Eq, ParamExpr("derp").into()).into()
                )],
                ..Query::on("articles")
            }
        ),
    );

    assert_eq!(
        must_parse(
            all_consuming(query::<VerboseError<&str>>),
            "articles {published_at} ? title.eq.$derp ..123"
        ),
        (
            "",
            Query {
                patterns: vec![
                    Pattern::Shape(vec![Query::on("published_at")]),
                    Pattern::Filter(
                        ComparisonExpr(FieldExpr("title"), Eq, ParamExpr("derp").into()).into()
                    ),
                    Pattern::Limit(Limit::N(123)),
                ],
                ..Query::on("articles")
            }
        ),
    );

    assert_eq!(
        must_parse(
            all_consuming(query::<VerboseError<&str>>),
            "articles sort(-published_at) {title}"
        ),
        (
            "",
            Query {
                patterns: vec![
                    Pattern::Order(vec![(Direction::Desc, FieldExpr("published_at"))]),
                    Pattern::Shape(vec![Query::on("title")]),
                ],
                ..Query::on("articles")
            }
        ),
    );

    // "doings ? | (& end.is.null start.le.$since) (& end.gt.$since start.le.$until)";
    // "doings ? (end.is.null) & (start.le.$since) | (end.gt.$since) & (start.le.$until)";

    assert_eq!(
        must_parse(
            all_consuming(query::<VerboseError<&str>>),
            "doings ? or(and(end.is.null,start.le.$since),and(end.gt.$since,start.le.$until))",
        ),
        (
            "",
            Query {
                patterns: vec![Pattern::Filter(
                    LogicExpr(
                        LogicOp::Or,
                        vec![
                            LogicExpr(
                                LogicOp::And,
                                vec![
                                    ComparisonExpr(FieldExpr("end"), Is, SimpleExpr::Null.into())
                                        .into(),
                                    ComparisonExpr(
                                        FieldExpr("start"),
                                        Le,
                                        ParamExpr("since").into()
                                    )
                                    .into(),
                                ]
                            )
                            .into(),
                            LogicExpr(
                                LogicOp::And,
                                vec![
                                    ComparisonExpr(FieldExpr("end"), Gt, ParamExpr("since").into())
                                        .into(),
                                    ComparisonExpr(
                                        FieldExpr("start"),
                                        Le,
                                        ParamExpr("until").into()
                                    )
                                    .into(),
                                ]
                            )
                            .into()
                        ]
                    )
                    .into()
                )],
                ..Query::on("doings")
            }
        ),
    );

    // assert_eq!(
    //     must_parse(
    //         all_consuming(query::<VerboseError<&str>>),
    //         "articles + $new_things ! $old_things {comments + $first}",
    //     ),
    //     (
    //         "",
    //         Query {
    //             patterns: vec![
    //                 Mutation::Upsert {
    //                     expr: ParamExpr("new_things")
    //                 }
    //                 .into(),
    //                 Mutation::Delete {
    //                     expr: ParamExpr("old_things")
    //                 }
    //                 .into(),
    //                 Pattern::Shape(vec![Query {
    //                     patterns: vec![Mutation::Upsert {
    //                         expr: ParamExpr("first")
    //                     }
    //                     .into()],
    //                     ..Query::on("comments")
    //                 }]),
    //             ],
    //             ..Query::on("articles")
    //         }
    //     ),
    // );
}
