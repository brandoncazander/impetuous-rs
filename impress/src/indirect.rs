//! http://lukaskalbertodt.github.io/2018/08/03/solving-the-generalized-streaming-iterator-problem-without-gats.html#workaround-c-wrapper-type
use std::convert::From;

use serde::de::{Deserialize, Deserializer};

pub trait Indirect {
    type Ref;
    type Owned;
}

pub struct ByRef;

impl Indirect for ByRef {
    type Ref = ();
    type Owned = !;
}

pub struct ByOwned;

impl Indirect for ByOwned {
    type Ref = !;
    type Owned = ();
}

pub enum Indirection<'a, T: 'a, I: Indirect> {
    Ref(&'a T, I::Ref),
    Owned(T, I::Owned),
}

impl<'a, T> From<T> for Indirection<'a, T, ByOwned> {
    fn from(t: T) -> Self {
        Indirection::Owned(t, ())
    }
}

impl<'a, T> From<&'a T> for Indirection<'a, T, ByRef> {
    fn from(t: &'a T) -> Self {
        Indirection::Ref(t, ())
    }
}

impl<'de, 'a, T> Deserialize<'de> for Indirection<'a, T, ByOwned>
where
    T: Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        T::deserialize(deserializer).map(Into::into)
    }
}
