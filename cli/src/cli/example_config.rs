use structopt::StructOpt;

use crate::{
    cli::{Command, General},
    Impetuous,
};

/// Print an example config.toml file.
#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub struct ExampleConfig {}

impl Command for ExampleConfig {
    fn run(self, gen: &General, im: &Impetuous) -> anyhow::Result<()> {
        println!("{}", crate::conf::EXAMPLE);
        Ok(())
    }
}
