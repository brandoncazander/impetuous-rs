//! some sort of abstraction around sql querying - very coupled to sql
//!
//! todo keep an eye on https://sqlite.org/limits.html#max_variable_number
//!
//! FIXME the "linking"/joining stuff really should be fixed but I keep fighting the
//! compiler with really trivial borrowing shit. The API for creating links basically
//! doesn't exist and what does is incredibly painful/ugly. Also, it might be worth
//! thinking about allowing multiple joins on the same relationship, with the extras
//! being only used to filter the left table, so that the extra joins don't return
//! columns. And this is just to help with ACL/policy stuff in case it might otherwise
//! interact strangely with user joins ... but this might only happen if we allow inner
//! joins (which we don't (yet (and probably will never because I need to stop working
//! on this and do something meaningful with my life (and also abstracting sql databases
//! into a graph or even just an orm might be needlessly complex compared to querying
//! with datalog/something like what datomic does)))).
#[cfg(backtrace)]
use std::backtrace::Backtrace;
use std::cell::RefCell;
use std::fmt::Debug;
use std::rc::Rc;
use std::{cmp, collections::HashMap, fmt, hash};

use rusqlite::types::ToSql;

use crate::sql::{self, BuildSql, ColumnSource, CursoredGet, FromColumns};

mod loading;

pub use loading::{BackLoadIntent, FlatLoader, MergeLoader, MergePoint, ModelLoader};

// https://sqlite.org/limits.html#max_variable_number
const MAX_PARAMS: usize = 999;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("database error: {source}")]
    Db {
        #[from]
        source: rusqlite::Error,
        #[cfg(backtrace)]
        backtrace: Backtrace,
    },
    #[error("your query has too many parameters; sometimes you can get away with more than 500, but keeping it at or below 500 is most reliable")]
    TooManyParameters,
    #[error("failed to merge model manifests: {reason}")]
    Merge {
        reason: String,
        #[cfg(backtrace)]
        backtrace: Backtrace,
    },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

/// todo move to sql.rs?
#[derive(Debug)]
pub struct SeqAliasing(std::ops::RangeFrom<u32>);

impl std::default::Default for SeqAliasing {
    fn default() -> Self {
        SeqAliasing(0..)
    }
}

impl SeqAliasing {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn next_alias(&mut self) -> sql::Ident<u32> {
        let n = self.next().expect("table alias ran out of numbers in u32");
        sql::ident(n)
    }

    pub fn alias(&mut self, n: TableName) -> TableRef {
        sql::Aliased::new(sql::ident(n), self.next_alias())
    }
}

impl Iterator for SeqAliasing {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

pub trait IsEmpty {
    fn is_empty(&self) -> bool;
}

/// TODO is this backwards?
pub trait Patch {
    /// for a sparse object made from Maybe fields, copy existant fields on self to other
    fn patch(&self, _: &mut Self);
}

impl<T> Patch for Box<T>
where
    T: Patch,
{
    fn patch(&self, other: &mut Self) {
        self.as_ref().patch(other)
    }
}

impl<T> Patch for Option<T>
where
    T: Patch + Default,
{
    fn patch(&self, other: &mut Self) {
        if let Some(this) = self {
            let other = other.get_or_insert_with(|| Default::default());
            this.patch(other)
        }
    }
}

/// Mostly a way to deal with nullable and non-nullable foreign keys in a similar way.
pub trait ForeignKey<T>
where
    T: HasPrimaryKey,
{
    /// `Some(Self::Value)` if this foreign-key points to some relationship value.
    /// `None if it doesn't point to anything.
    ///
    /// Note, pointing to nothing `Some(None)` is different from not pointing to
    /// anything `None`.
    fn parent_pk(&self) -> Option<&<T as HasPrimaryKey>::Type>;

    fn parent(&self) -> Option<&T>;

    fn parent_mut(&mut self) -> Option<&mut T>;
}

impl<T> ForeignKey<T> for T
where
    T: HasPrimaryKey,
{
    fn parent_pk(&self) -> Option<&<T as HasPrimaryKey>::Type> {
        HasPrimaryKey::get(self)
    }

    fn parent(&self) -> Option<&T> {
        Some(&self)
    }

    fn parent_mut(&mut self) -> Option<&mut T> {
        // Why isn't this Some(&mut self) but parent() is Some(&self) ??? I don't even
        Some(self)
    }
}

impl<T> ForeignKey<T> for Option<T>
where
    T: HasPrimaryKey,
{
    fn parent_pk(&self) -> Option<&<T as HasPrimaryKey>::Type> {
        self.as_ref().and_then(|l| HasPrimaryKey::get(l))
    }

    fn parent(&self) -> Option<&T> {
        self.as_ref()
    }

    fn parent_mut(&mut self) -> Option<&mut T> {
        self.as_mut()
    }
}

pub const NULL: Expr = Expr::Null;
pub const BIND: Expr = sql::bind();
pub const TRUE: Expr = sql::lit("true");
pub const FALSE: Expr = sql::lit("false");

/// FieldExpr and Q are kind of factory types for query building ...

pub type TableName = &'static str;
pub type TableRef = sql::Aliased<sql::Ident<TableName>, sql::Ident<u32>>;
pub type SelectColumn = sql::SelectColumn<sql::Ident<u32>, &'static sql::Ident<&'static str>>;
pub type ColumnName = &'static sql::Ident<&'static str>;
pub type Expr = sql::Expr<SelectColumn>;
pub type Select<'a> = sql::Select<
    // Columns
    Vec<Expr>,
    // From
    sql::ExprTuple<(&'a TableRef,)>,
    // With
    Vec<&'a dyn BuildSql>,
    // Filter
    Vec<&'a Expr>,
    // Join
    Vec<sql::Join<TableRef, Expr>>,
    // Order By
    Vec<&'a sql::OrderBy<SelectColumn>>,
>;

pub trait Modeled: Debug + Clone + Sized {
    type FieldSet: Default + IsEmpty;
    type LinkSet: Default + IsEmpty;
    type FieldExpr;
    type BackLoad: BackLoadIntent<Self>;
    // type Pk: Clone;

    const NAME: &'static str;

    fn q() -> Q<Self> {
        Q::new()
    }

    fn logger() -> Option<slog::Logger> {
        None
    }

    fn log_sql(sql: &str, params: &[&dyn ToSqlDebug]) {
        if let Some(l) = Self::logger() {
            debug!(l, "{} ... {:?}", sql, params);
        }
    }

    fn table_name() -> TableName;

    fn field_expr(_: TableRef) -> Self::FieldExpr;

    // todo use a GAT when they exist
    fn from_columns(q: &Q<Self>, row: &mut sql::CursoredGet) -> rusqlite::Result<Self>;

    fn insert(&self, tx: &rusqlite::Transaction) -> Result<i64>;

    fn update(&self, pk: &i64, tx: &rusqlite::Transaction) -> Result<()>;

    fn delete(pk: &i64, tx: &rusqlite::Transaction) -> Result<()>;

    fn q_params(q: &Q<Self>) -> Vec<&dyn ToSqlDebug>;

    fn empty_select(q: &Q<Self>) -> Select
    where
        Self: Sized,
    {
        Select {
            from: sql::ExprTuple::new(&q.t),
            columns: Default::default(),
            with: Default::default(),
            filter: Default::default(),
            join: Default::default(),
            limit: Default::default(),
            order_by: Default::default(),
        }
    }

    fn do_select(q: &Q<Self>, tx: &rusqlite::Transaction) -> Result<Vec<Self>>
    where
        Self: Sized,
    {
        let mut out = vec![];
        let loader = FlatLoader::new(&q, &mut out);
        Self::select_with_loader(&q, loader, &tx)?;
        Ok(out)
    }

    fn select_with_loader<'a, L: ModelLoader<Self>>(
        q: &'a Q<Self>,
        mut loader: L,
        tx: &rusqlite::Transaction,
    ) -> Result<()>
    where
        Self: Sized,
    {
        let mut rows_loaded = 0;
        let mut sel: Select = Self::empty_select(&q);

        // If we are loading models through as a child of a relationship,
        // the MergeLoader will need to modify the select to fetch specific
        // rows and information that make sense given some parents.
        loader.add_to_select(&mut sel);

        // Add the Q's (user's) stuff to the select, ultimately to build the manifest.
        Self::add_to_select(&q, &mut sel);

        // Back loading is a confusing name given to loading child records (loading
        // models that reference/have foreign keys to this one).
        //
        // If our `q` is meant to load these child records, a BackLoadIntent is created
        // to set that up. That's what `back_load` is.
        let mut back_load = Self::BackLoad::for_select(&q, &mut sel);

        // In the event that nobody (including the user) request any columns,
        // throw something in there to make the SQL valid. Nobody is reading the row,
        // so nobody should care. The user will just get a bunch of empty nothings.
        if sel.columns.is_empty() {
            sel.columns.push(Expr::Literal("0"))
        }

        let sql = sel.as_sql();

        if let Some(l) = Self::logger() {
            debug!(l, "{}", &sql);
        }

        let mut stmt = tx.prepare(&sql)?;

        while let Some(params) = loader.take_params() {
            if let Some(l) = Self::logger() {
                debug!(l, "{:?}", params);
            }

            let mut rows = stmt.query(params)?;
            rows_loaded += 1;

            while let Some(row) = rows.next()? {
                let mut cursor = CursoredGet::from(row);
                let dest = loader.landing(&mut cursor)?;
                dest.push(q.from_columns(&mut cursor)?);

                if let Some(back_load) = &mut back_load {
                    let m = dest.last().unwrap();
                    back_load.tap_model(&m)?;
                    back_load.eat_columns(&mut cursor)?;
                }
            }
        }

        if let Some(back_load) = back_load {
            let mut parents = Vec::with_capacity(rows_loaded);
            loader.weird_backloading_thing(&mut parents);
            back_load.load_back_links(&q, &mut parents, &tx)?;
        }

        loader.finalize();

        Ok(())
    }

    fn add_to_select<'q: 's, 's>(q: &'q Q<Self>, sel: &'s mut Select<'q>)
    where
        Self: Sized;
}

pub trait ToSqlDebug: ToSql + Debug {}

impl<T: ToSql + Debug> ToSqlDebug for T {}

#[derive(Debug)]
pub enum Limit {
    N(i64),
    Bind(Box<dyn ToSqlDebug>),
}

impl Limit {
    pub fn is_bind(&self) -> bool {
        match self {
            Limit::Bind(_) => true,
            _ => false,
        }
    }
}

impl From<i64> for Limit {
    fn from(v: i64) -> Limit {
        Limit::N(v)
    }
}

impl From<Box<dyn ToSqlDebug>> for Limit {
    fn from(v: Box<dyn ToSqlDebug>) -> Limit {
        Limit::Bind(v)
    }
}

// impl std::ops::Deref for Limit {
//     type Target = Either<i64, Box<dyn ToSqlDebug>>;
//
//     fn deref(&self) -> &Self::Target {
//         &self.0
//     }
// }

#[derive(Debug)]
pub struct Q<M: Modeled> {
    a: Rc<RefCell<SeqAliasing>>,
    t: TableRef,
    /// If this Q is used from a link and no fields are being asked for, then, normally,
    /// it returns empty objects. But, if is_silent is set, it won't and the link field
    /// should not be rendered in the parent object instead.
    pub is_silent: bool,
    pub params: Vec<Box<dyn ToSqlDebug>>,
    pub get: M::FieldSet,
    pub links: M::LinkSet,
    pub filter: Vec<Expr>,
    pub limit: Option<Limit>,
    pub order_by: Vec<sql::OrderBy<SelectColumn>>,
}

impl<M: Modeled> std::default::Default for Q<M> {
    fn default() -> Self {
        let mut a = SeqAliasing::default();
        let t = a.alias(M::table_name());
        Q {
            a: Rc::new(RefCell::new(a)),
            t,
            is_silent: false,
            params: Default::default(),
            get: M::FieldSet::default(),
            filter: Default::default(),
            links: Default::default(),
            limit: None,
            order_by: Default::default(),
        }
    }
}

impl<M: Modeled> Q<M> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn all_params(&self) -> Vec<&dyn ToSqlDebug> {
        M::q_params(&self)
    }

    /// a nice way to execute this query using a transaction...
    pub fn list(&self, tx: &rusqlite::Transaction) -> Result<Vec<M>> {
        M::do_select(&self, tx)
    }

    pub fn one(&mut self, tx: &rusqlite::Transaction) -> Result<Option<M>> {
        self.limit(1).list(tx).map(|v| v.into_iter().next())
    }

    pub fn again<O: Modeled>(&self) -> Q<O> {
        Q::<O>::new_with_alias(Rc::clone(&self.a))
    }

    fn new_with_alias(a: Rc<RefCell<SeqAliasing>>) -> Self {
        let t = a.borrow_mut().alias(M::table_name());
        Q {
            a,
            t,
            ..Q::default()
        }
    }

    pub fn table(&self) -> &TableRef {
        &self.t
    }

    pub fn expr(&self) -> M::FieldExpr {
        M::field_expr(self.t.clone())
    }

    /// q.get(|x| x.foo().bar()) is equivalent to q.get.foo().bar(), except, in the first version,
    /// get() returns a &mut Q for chaining with other methods on Q
    pub fn get<F>(&mut self, func: F) -> &mut Self
    where
        F: FnOnce(&mut M::FieldSet) -> &mut M::FieldSet,
    {
        func(&mut self.get);
        self
    }

    /// q.filter(|x| ...) is equivalent to q.filter = Some(q.expr()...), except, in the first
    /// version, filter() returns a &mut Q for chaining with other methods on Q
    pub fn filter<F>(&mut self, func: F) -> &mut Self
    where
        F: FnOnce(&mut M::FieldExpr) -> Expr,
    {
        let sql_expr = func(&mut self.expr());
        self.filter.push(sql_expr);
        self
    }

    /// q.order_by(|x| ...) is equivalent to q.order_by.push(sql::OrderBy<SelectColumn>(...)), except, in the first
    /// version, order_by() returns a &mut Q for chaining with other methods on Q
    pub fn order_by<F>(&mut self, func: F) -> &mut Self
    where
        F: FnOnce(&mut M::FieldExpr) -> sql::OrderBy<SelectColumn>,
    {
        let ordering = func(&mut self.expr());
        self.order_by.push(ordering);
        self
    }

    pub fn limit<I: Into<Limit>>(&mut self, limit: I) -> &mut Self {
        self.limit = Some(limit.into());
        self
    }

    // I don't care anymore ... todo come back later when you do again?
    // pub fn link<'a, F, O>(&'a mut self, func: F) -> &'a mut Box<crate::orm::Q<O>>
    // where
    //     F: FnOnce(&'a mut M::LinkSet) -> &'a mut Option<Box<crate::orm::Q<O>>>,
    //     O: Modeled,
    // {
    //     // fucking god damn rust
    //     let opt = func(&mut self.links);
    //     if opt.is_none() {
    //         opt.replace(Box::new(self.again()));
    //     }
    //     opt.as_mut().unwrap()
    // }

    // pub fn bind<B: Into<Box<dyn ToSqlDebug>>>(&mut self, b: B) -> &mut Self {
    // wtf if I use ^^^ I can give this method literally a fucking box
    // and it will have no idea how to convert from a box to a box
    pub fn bind(&mut self, b: Box<dyn ToSqlDebug>) -> &mut Self {
        self.params.push(b);
        self
    }
}

impl<M: Modeled> FromColumns for Q<M> {
    type Out = M;

    fn from_columns(&self, row: &mut sql::CursoredGet) -> rusqlite::Result<Self::Out> {
        M::from_columns(self, row)
    }
}

////////////////////////////////////////////////////////////////////////////////

/// - Each model's primary key must be an integer column called "id". (In sqlite, making
///   an integer primary key column will just alias the implicit "rowid" column anyway...)
/// - Foreign keys MUST point to the primary key of their target/foreign table.
#[macro_export]
macro_rules! model {
    (

    $name:ident($pk:ident: $pk_type:ty) $manifest:ident {
        $( $field:ident : $type:ty ,)*
        $( @link $link:ident : $link_type:ty => $link_scope:ident (id),)*
        $( @link-back $back_link:ident : Vec<$back_manifest:ident> <= $back_scope:ident ($back_column:ident),)*
    }

    $(@log $log:expr)?
    ) => {
        pub use $name::$manifest;

        has_pk!($manifest $pk: $pk_type);

        pub mod $name {
            use super::*;

            use $crate::sql;

            // needed for the serde(default="...") path to be valid for both inside this crate and
            // when used outside. Since I can't use $crate in the string literal.
            use $crate::maybe as __maybe;

            #[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq)]
            // A really awful sparse structure that represents the model.
            // It's very ugly to use and makes everyone sad.
            pub struct $manifest {
                #[serde(skip)]
                pub $pk: $crate::maybe::Maybe<$pk_type>,
                $(
                #[serde(
                    default = "__maybe::maybe_not",
                    deserialize_with = "__maybe::deserialize",
                    skip_serializing_if = "Option::is_none"
                )]
                pub $field: $crate::maybe::Maybe<$type>,
                )*
                $(
                #[serde(
                    default = "__maybe::maybe_not",
                    deserialize_with = "__maybe::deserialize",
                    skip_serializing_if = "Option::is_none"
                )]
                pub $link: $crate::maybe::Maybe<$link_type>,
                )*
                $(
                #[serde(
                    default = "__maybe::maybe_not",
                    deserialize_with = "__maybe::deserialize",
                    skip_serializing_if = "Option::is_none"
                )]
                pub $back_link: $crate::maybe::Maybe<Vec<$back_manifest>>,
                )*
            }

            pub type _Manifest = $manifest;

            #[allow(non_upper_case_globals)]
            pub static table: sql::Ident<&'static str> = sql::ident(stringify!($name));

            #[allow(non_upper_case_globals)]
            pub static $pk: sql::Ident<&'static str> = sql::ident(stringify!($pk));

            $(
            #[allow(non_upper_case_globals)]
            pub static $field: sql::Ident<&'static str> = sql::ident(stringify!($field));
            )*

            $(
            #[allow(non_upper_case_globals)]
            pub static $link: sql::Ident<&'static str> = sql::ident(stringify!($link));
            )*

            /// implements FromColumns
            pub struct _Shape<'a>(pub &'a _FieldSet, pub &'a _LinkSet);

            /// local fields only, so valid for mutations too?
            #[derive(Debug, Default, PartialEq)]
            pub struct _FieldSet {
                pub $pk: bool,
                $( pub $field: bool, )*
            }

            /// links/joins - basically Q storage ...?
            #[derive(Debug, Default)]
            pub struct _LinkSet {
                $( pub $link: Option<Box<$crate::orm::Q<super::$link_scope::_Manifest>>>, )*
                $( pub $back_link: Option<Box<$crate::orm::Q<super::$back_manifest>>>, )*
            }

            /// Like a super-factory, for sql::Expr
            ///
            /// - Our local fields are available as methods returning column expressions.
            /// - Our links are methods that alias & return a FieldExpr for the model
            ///   they link to.
            #[derive(Debug)]
            pub struct _FieldExpr {
                pub t: $crate::orm::TableRef,
            }

            /// This implements BackLoadIntent<$manifest>
            #[derive(Debug)]
            pub struct _BackLoadThis {
                /// When implementing BackLoadIntent<T>, the primary key of T may or may
                /// not be fetched by the query Q<T>.
                ///
                /// Since, for every T, the primary keys is required to load its
                /// children, _BackLoadThis will add T's primary key column the select
                /// statement and consume it from the query result row. In this case,
                /// use_row is true.
                ///
                /// Otherwise, if Q<T> is already fetching the primary key,
                /// use_row is false and the primary key for each T is taken from the T
                /// instances themselves.
                pub use_row: bool,
                /// Every primary key we've seen that we need to fetch children for.
                pub pks: Vec<i64>,
            }

            #[derive(Debug)]
            pub struct _BackLoadThisWithLinks {
                // This can back load $model
                pub _this: Option<_BackLoadThis>,
                // Each $link is loaded through a joins, we're responsible for back
                // loading those too.
                $( pub $link: Option<Box<<super::$link_scope::_Manifest as $crate::orm::Modeled>::BackLoad>>, )*
            }
        }

        impl $crate::orm::Patch for $manifest
            // where $( $link_scope::_Manifest: $crate::orm::Patch<$link_scope::_Manifest> ),*
        {
            fn patch(&self, other: &mut $manifest) {
                $(
                if let Some(v) = self.$field.as_ref() {
                    other.$field = $crate::maybe::maybe(v.clone());
                }
                )*
                $(
                if let Some(v) = self.$link.as_ref() {
                    let l = other.$link.get_or_insert_with(||
                        Default::default()
                    );
                    v.patch(l);
                }
                )*
            }
        }

        impl Default for $manifest {
            fn default() -> Self {
                $manifest {
                    $pk: None.into(),
                    $( $field: None.into(), )*
                    $( $link: None.into(), )*
                    $( $back_link: None.into(), )*
                }
            }
        }

        impl $crate::orm::Modeled for $manifest {
            type FieldSet = $name::_FieldSet;
            type FieldExpr = $name::_FieldExpr;
            type LinkSet = $name::_LinkSet;
            type BackLoad = $name::_BackLoadThisWithLinks;
            // type Pk = i64;

            const NAME: &'static str = stringify!($name);

            $( fn logger() -> Option<slog::Logger> { Some($log) } )?

            /// This returns all of the q's parameters & its links that are joined when querying.
            fn q_params(q: &$crate::orm::Q<Self>) -> Vec<&dyn $crate::orm::ToSqlDebug> {
                use $crate::orm::{Limit, ToSqlDebug};
                // The order here is _super important_ because bindings are unnamed and
                // positional only ....
                //
                // In our impl of add_to_select(), we recurse into our $links before
                // adding our own filter clauses that might contain bindings...
                let mut all = vec![];

                $( if let Some(link_q) = q.links.$link.as_ref() {
                    // Never get the limit parameter of joined queries, they're silently
                    // ignored (TODO; not good) when producing the query string
                    all.extend(params_unboxed(&link_q.params));
                } )*

                // Our filter parameters ...
                all.extend(params_unboxed(&q.params));

                // Our filter parameters ...
                if let Some(Limit::Bind(b)) = &q.limit {
                    all.push(b.as_ref() as &dyn ToSqlDebug);
                }

                return all;

                fn params_unboxed(p: &Vec<Box<dyn ToSqlDebug>>) -> impl Iterator<Item = &dyn ToSqlDebug> {
                    p.iter().map(|b| b.as_ref() as &dyn ToSqlDebug)
                }
            }

            fn field_expr(t: $crate::orm::TableRef) -> Self::FieldExpr {
                $name::_FieldExpr { t }
            }

            fn table_name() -> &'static str {
                &stringify!($name)
            }

            fn from_columns(q: &$crate::orm::Q<Self>, row: &mut $crate::sql::CursoredGet) -> rusqlite::Result<Self> {
                use $crate::sql::from_columns::FromColumns;

                $name::_Shape(&q.get, &q.links).from_columns(row)
            }

            fn insert(&self, tx: &rusqlite::Transaction) -> $crate::orm::Result<i64> {
                use $crate::orm::{ColumnName, ToSqlDebug};
                use $crate::sql::{self, BuildSql};

                let mut params: Vec<&dyn ToSqlDebug> = Default::default();
                let mut columns: Vec<ColumnName> = Default::default();
                let mut values: Vec<sql::Expr<()>> = Default::default();

                $(
                if let Some(value) = self.$field.as_ref() {
                    columns.push(&$name::$field);
                    values.push(sql::bind());
                    params.push(value);
                }
                )*

                $(
                if let Some(value) = self.$link.as_ref().and_then(|link| $crate::orm::ForeignKey::parent_pk(link)) {
                    columns.push(&$name::$link);
                    values.push(sql::bind());
                    params.push(value);
                }
                )*

                let sql = sql::Insert::default_tuples()
                    .table(&$name::table)
                    .columns(columns)
                    .expr(sql::Values::new(vec![values]))
                    .as_sql();

                Self::log_sql(&sql, &params);

                let mut stmt = tx.prepare(&sql)?;
                if stmt.execute(params)? == 0 {
                    // better this than return an incorrect rowid
                    panic!("unexpectedly inserted zero rows")
                };

                Ok(tx.last_insert_rowid())
            }

            fn update(&self, pk: &i64, tx: &rusqlite::Transaction) -> $crate::orm::Result<()> {
                use $crate::orm::{ColumnName, ToSqlDebug};
                use $crate::sql::{bind, Expr, BuildSql, SelectColumn, Update};

                let mut params: Vec<&dyn ToSqlDebug> = vec![];
                let mut set: Vec<Expr<ColumnName>> = vec![];

                $(
                if let Some(value) = self.$field.as_ref() {
                    set.push(Expr::Column(&$name::$field).eq(bind()));
                    params.push(value);
                }
                )*

                $(
                if let Some(value) = self.$link.as_ref().and_then(|link| $crate::orm::ForeignKey::parent_pk(link)) {
                    set.push(Expr::Column(&$name::$link).eq(bind()));
                    params.push(value);
                }
                )*

                let for_pk = SelectColumn::new(&$name::table, &$name::$pk).expr().eq(bind());
                params.push(pk);

                let sql = Update::default_tuples()
                    .table(&$name::table)
                    .set(set)
                    .filter(for_pk)
                    .as_sql();

                Self::log_sql(&sql, &params);

                let mut stmt = tx.prepare(&sql)?;
                stmt.execute(params)?;

                Ok(())
            }

            fn delete(pk: &i64, tx: &rusqlite::Transaction) -> $crate::orm::Result<()> {
                use $crate::{
                    orm::ToSqlDebug,
                    sql::{Expr, BuildSql, SelectColumn, Delete},
                };

                let for_pk = SelectColumn::new(&$name::table, &$name::$pk).expr().eq(Expr::Bind);

                let sql = Delete::default_tuples()
                    .table(&$name::table)
                    .filter(for_pk)
                    .as_sql();

                let params: Vec<&dyn ToSqlDebug> = vec![pk];

                Self::log_sql(&sql, &params);
                let mut stmt = tx.prepare(&sql)?;
                stmt.execute(params)?;

                Ok(())
            }

            /// add the Q's columns, ordering, limit, filter, and forward joins to the select
            fn add_to_select<'q: 's, 's>(
                q: &'q $crate::orm::Q<Self>,
                sel: &'s mut $crate::orm::Select<'q>
            )
            where
                Self: Sized
            {
                use $crate::orm::Limit;
                use $crate::sql::{self, ColumnSource};

                if q.get.$pk {
                    sel.add_column(q.table().c(&$name::$pk).expr());
                }

                $(
                if q.get.$field {
                    sel.add_column(q.table().c(&$name::$field).expr());
                }
                )*

                $(
                if let Some(link_q) = q.links.$link.as_ref() {
                    // create a join ... and recurse?
                    let lc = &$name::$link;
                    let fc = &$link_scope::id;
                    let on = q.table().c(lc).expr().eq(link_q.table().c(fc));
                    let join = $crate::sql::Join {
                        op: $crate::sql::JoinOperator::Left,
                        target: link_q.table().clone(),
                        on,
                    };
                    sel.add_join(join);

                    <$link_scope::_Manifest as $crate::orm::Modeled>::add_to_select(&link_q, sel);
                }
                )*

                for ord in q.order_by.iter() {
                    sel.add_order_by(ord);
                }

                // always set the limit, even if just to the default, since this doesn't
                // aggregate and we don't work right if our $link Qs set it to a
                // binding/query parameter TODO this is weird
                // maybe a better way to go about this is look for the first link with a
                // limit and use it?
                match q.limit.as_ref() {
                    None => sel.set_limit(sql::Limit::default()),
                    Some(Limit::N(n)) => sel.set_limit(*n),
                    Some(Limit::Bind(_)) => sel.set_limit(sql::Limit::Bind),
                };

                for sql_expr in q.filter.iter() {
                    sel.add_filter(sql_expr);
                }
            }
        }

        impl $name::_FieldSet {
            pub fn $pk (&mut self) -> &mut Self {
                self.$pk = true;
                self
            }

            $(
            pub fn $field (&mut self) -> &mut Self {
                self.$field = true;
                self
            }
            )*
        }

        impl $crate::orm::IsEmpty for $name::_FieldSet {
            fn is_empty(&self) -> bool {
                self == &Self::default()
            }
        }

        impl $name::_LinkSet {
            $(
            /// link factories??? these should accumulate, so if they already exist, don't recreate
            pub fn $link (q: &mut $crate::orm::Q<$manifest>) -> &mut $crate::orm::Q<$link_scope::_Manifest> {
                if q.links.$link.is_none() {
                    q.links.$link = Some(Box::new(q.again::<$link_scope::_Manifest>()));
                }
                q.links.$link.as_mut().unwrap()
            }
            )*

            $(
            /// back links use a clean alias because they are run as separate statements
            pub fn $back_link (q: &mut $crate::orm::Q<$manifest>) -> &mut $crate::orm::Q<$back_manifest> {
                if q.links.$back_link.is_none() {
                    q.links.$back_link = Some(Box::new($crate::orm::Q::<$back_manifest>::default()));
                }
                q.links.$back_link.as_mut().unwrap()
            }
            )*
        }

        impl $crate::orm::IsEmpty for $name::_LinkSet {
            fn is_empty(&self) -> bool {
                true $(&& self.$link.is_none())* $(&& self.$back_link.is_none())*
            }
        }

        impl $name::_FieldExpr {
            pub fn $pk (&self) -> $crate::orm::Expr {
                use $crate::sql::ColumnSource;

                self.t.c(&$name::$pk).expr()
            }

            $(
            pub fn $field (&self) -> $crate::orm::Expr {
                use $crate::sql::ColumnSource;

                self.t.c(&$name::$field).expr()
            }
            )*

            $(
            pub fn $link (&self) -> $crate::orm::Expr {
                use $crate::sql::ColumnSource;

                self.t.c(&$name::$link).expr()
            }
            )*
        }

        impl $crate::orm::BackLoadIntent<$manifest> for $name::_BackLoadThis {
            fn for_select(q: &$crate::orm::Q<$manifest>, sel: &mut $crate::orm::Select) -> Option<Self> {
                use $crate::sql::ColumnSource;

                if false $( || q.links.$back_link.is_some() )* {
                    if !q.get.id {
                        let pk = q.table().c(&$name::id).expr();
                        sel.add_column(pk);
                        Some($name::_BackLoadThis { use_row: true,  pks: Default::default() })
                    } else {
                        Some($name::_BackLoadThis { use_row: false, pks: Default::default() })
                    }
                } else {
                    None
                }
            }

            fn eat_columns(&mut self, c: &mut $crate::sql::CursoredGet) -> $crate::orm::Result<()> {
                if self.use_row {
                    self.pks.push(c.get()?);
                }

                Ok(())
            }

            fn tap_model(&mut self, m: &$manifest) -> $crate::orm::Result<()> {
                #[cfg(backtrace)]
                use std::backtrace::Backtrace;

                if !self.use_row {
                    let pk = (*m.id).ok_or_else(|| {
                        $crate::orm::Error::Merge {
                            reason: format!("back load intent failed to find id in {:?}", m),
                            #[cfg(backtrace)]
                            backtrace: Backtrace::capture(),
                        }
                    })?;
                    self.pks.push(pk);
                }

                Ok(())
            }

            #[allow(unused_variables)]
            fn load_back_links(
                &self,
                q: &$crate::orm::Q<$manifest>,
                #[allow(unused_mut)]
                mut parents: &mut Vec<&mut $manifest>,
                tx: &rusqlite::Transaction,
            ) -> $crate::orm::Result<()>
            where
                Self: Sized
            {
                #[allow(unused_imports)]
                use $crate::orm::{MergePoint, MergeLoader, Modeled};

                $(
                if let Some(back_q) = q.links.$back_link.as_ref() {
                    let merge_points = self.pks
                        .iter()
                        .cloned()
                        .zip(
                            parents
                            .iter_mut()
                            .map(|parent| {
                                parent.$back_link
                                    .get_or_insert_with(|| Default::default())
                                    .as_mut()
                            })
                        )
                        .collect::<Vec<MergePoint<'_, $back_manifest>>>();

                    if !merge_points.is_empty() {
                        let loader = MergeLoader::new(&back_q, merge_points, &$back_scope::$back_column)?;

                        $back_manifest::select_with_loader(&back_q, loader, &tx)?;
                    }
                }
                )*
                Ok(())
            }
        }

        impl $crate::orm::BackLoadIntent<$manifest> for $name::_BackLoadThisWithLinks {
            fn for_select(q: &$crate::orm::Q<$manifest>, mut sel: &mut $crate::orm::Select) -> Option<Self> {
                let _this = $name::_BackLoadThis::for_select(&q, &mut sel);

                $(
                let $link = q.links.$link
                    .as_ref()
                    .and_then(|q| <$link_scope::_Manifest as $crate::orm::Modeled>::BackLoad::for_select(&q, &mut sel))
                    .map(Box::new);
                )*

                if _this.is_some() $( || $link.is_some() )* {
                    Some($name::_BackLoadThisWithLinks { _this, $( $link, )* })
                } else {
                    None
                }
            }

            fn eat_columns(&mut self, c: &mut $crate::sql::CursoredGet) -> $crate::orm::Result<()> {
                if let Some(this_intent) = &mut self._this {
                    this_intent.eat_columns(c)?;
                }

                $(
                if let Some(link_intent) = &mut self.$link {
                    link_intent.eat_columns(c)?;
                }
                )*

                Ok(())
            }

            fn tap_model(&mut self, m: &$manifest) -> $crate::orm::Result<()> {
                if let Some(this_intent) = &mut self._this {
                    this_intent.tap_model(&m)?;
                }

                $(
                if let Some(link_intent) = &mut self.$link {
                    if let Some(m) = m.$link.as_ref().and_then($crate::orm::ForeignKey::parent) {
                        link_intent.tap_model(m)?;
                    }
                }
                )*

                Ok(())
            }

            fn load_back_links(
                &self,
                q: &$crate::orm::Q<$manifest>,
                #[allow(unused_mut)]
                mut parents: &mut Vec<&mut $manifest>,
                tx: &rusqlite::Transaction,
            ) -> $crate::orm::Result<()>
            where
                Self: Sized
            {
                if let Some(this_intent) = &self._this {
                    // Load children with foreign keys to $model
                    this_intent.load_back_links(q, &mut parents, &tx)?;
                }

                $(
                if let (Some(link_intent), Some(link_q)) = (&self.$link, &q.links.$link) {
                    // Load children with foreign keys to $link
                    let mut parents = parents
                        .iter_mut()
                        .filter_map(|m| m.$link.as_mut().and_then($crate::orm::ForeignKey::parent_mut))
                        .map(|boxed| boxed.as_mut())
                        .collect::<Vec<_>>();
                    link_intent.load_back_links(&link_q, &mut parents, &tx)?;
                }
                )*

                Ok(())
            }
        }

        impl<'a> $crate::sql::FromColumns for $name::_Shape<'a> {
            type Out = $manifest;

            fn from_columns(&self, row: &mut $crate::sql::CursoredGet) -> rusqlite::Result<Self::Out> {
                #[allow(unused_imports)]
                use $crate::orm::IsEmpty;
                use $crate::maybe::{Maybe};

                #[allow(unused_variables)]
                let (get, links) = (self.0, self.1);

                let $pk: Maybe<_> = if get.$pk {
                    Some(row.get()?).into()
                } else {
                    None.into()
                };

                $(
                let $field: Maybe<_> = if get.$field {
                    Some(row.get()?).into()
                } else {
                    None.into()
                };
                )*

                $(
                let $link: Maybe<_> = if let Some(foreign) = &links.$link {
                    if foreign.is_silent && IsEmpty::is_empty(&foreign.get) {
                        None.into()
                    } else {
                        Some(foreign.from_columns(row)?.into()).into()
                    }
                } else {
                    None.into()
                };
                )*

                Ok($manifest { $pk, $( $field, )* $( $link, )* ..$manifest::default() })
            }
        }

    };
}

pub trait HasPrimaryKey {
    type Type: Clone;

    fn column() -> Column<Self::Type>;

    /// get the value of the primary key if it exists
    fn get(&self) -> Option<&Self::Type>;

    /// set the value of the primary key
    fn set(&mut self, _: Self::Type);
}

impl<T> HasPrimaryKey for Box<T>
where
    T: HasPrimaryKey,
{
    type Type = <T as HasPrimaryKey>::Type;

    fn column() -> Column<Self::Type> {
        <T as HasPrimaryKey>::column()
    }

    fn get(&self) -> Option<&Self::Type> {
        (**self).get()
    }

    fn set(&mut self, v: Self::Type) {
        (**self).set(v)
    }
}

pub trait HasSurrogateKey {
    type Type: Clone;

    fn column() -> Column<Self::Type>;

    /// get the value of the surrogate key if it exists
    fn get(&self) -> Option<&Self::Type>;

    /// set the value of the surrogate key
    fn set(&mut self, _: Self::Type);

    fn default_with<F>(&mut self, new: F)
    where
        F: FnOnce() -> Self::Type,
    {
        if self.get().is_none() {
            self.set(new())
        }
    }
}

pub trait FromKey: HasSurrogateKey {
    fn from_key(key: <Self as HasSurrogateKey>::Type) -> Self;
}

impl<M: HasSurrogateKey + Default> FromKey for M {
    fn from_key(key: <Self as HasSurrogateKey>::Type) -> Self {
        let mut s = Self::default();
        HasSurrogateKey::set(&mut s, key);
        s
    }
}

impl<T> HasSurrogateKey for Box<T>
where
    T: HasSurrogateKey,
{
    type Type = <T as HasSurrogateKey>::Type;

    fn column() -> Column<Self::Type> {
        <T as HasSurrogateKey>::column()
    }

    fn get(&self) -> Option<&Self::Type> {
        (**self).get()
    }

    fn set(&mut self, v: Self::Type) {
        (**self).set(v)
    }
}

#[macro_export]
macro_rules! has_pk {
    ($model:ident $key:ident: $type:ty) => {
        impl $crate::orm::HasPrimaryKey for $model {
            type Type = $type; // lol

            fn column() -> $crate::orm::Column<Self::Type> {
                $crate::orm::column(stringify!($key))
            }

            fn get(&self) -> Option<&Self::Type> {
                self.$key.as_ref()
            }

            fn set(&mut self, v: Self::Type) {
                self.$key = Some(v).into();
            }
        }
    };
}

#[macro_export]
macro_rules! has_sk {
    ($model:ident $key:ident: $type:ty) => {
        impl $crate::orm::HasSurrogateKey for $model {
            type Type = $type;

            fn column() -> $crate::orm::Column<Self::Type> {
                $crate::orm::column(stringify!($key))
            }

            fn get(&self) -> Option<&Self::Type> {
                self.$key.as_ref()
            }

            fn set(&mut self, v: Self::Type) {
                self.$key = Some(v).into();
            }
        }
    };
}

#[derive(Default, Debug)]
pub struct SurrogateToPrimary<T>
where
    T: HasPrimaryKey + HasSurrogateKey + fmt::Debug,
    <T as HasSurrogateKey>::Type: cmp::Eq + hash::Hash + fmt::Debug,
    <T as HasPrimaryKey>::Type: fmt::Debug,
{
    pub map: HashMap<<T as HasSurrogateKey>::Type, Option<<T as HasPrimaryKey>::Type>>,
}

impl<T> SurrogateToPrimary<T>
where
    T: HasPrimaryKey + HasSurrogateKey + fmt::Debug,
    <T as HasSurrogateKey>::Type: cmp::Eq + hash::Hash + fmt::Debug,
    <T as HasPrimaryKey>::Type: fmt::Debug,
{
    pub fn add_surrogate(&mut self, t: &T) {
        if let Some(surrogate) = HasSurrogateKey::get(t) {
            self.map.insert(surrogate.clone(), None);
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Column<T> {
    pub name: &'static str,
    t: std::marker::PhantomData<T>,
}

pub fn column<T>(name: &'static str) -> Column<T> {
    Column {
        name,
        t: Default::default(),
    }
}

pub fn lookup_primary_keys<S, P>(
    map: &mut HashMap<S, Option<P>>,
    table: &'static str,
    surrogate: &Column<S>,
    primary: &Column<P>,
    tx: &rusqlite::Transaction,
    log: &slog::Logger,
) -> Result<()>
where
    S: rusqlite::ToSql + rusqlite::types::FromSql + cmp::Eq + hash::Hash + fmt::Debug,
    P: rusqlite::types::FromSql,
{
    let mut num_left = map.len();
    let mut all_keys = map.keys();
    let mut found = vec![];

    while num_left > 0 {
        let chunk = all_keys.by_ref().take(MAX_PARAMS);
        let chunk_len = std::cmp::min(num_left, MAX_PARAMS);
        num_left -= chunk_len;

        let mut a = SeqAliasing::default();

        let with = sql::WithAs {
            stmt: sql::binds::<()>(chunk_len).collect::<sql::Values<_>>(),
            name: a.next_alias(),
            columns: vec![sql::ident(surrogate.name)],
        };

        let table = a.alias(table);

        let on = table
            .c(sql::ident(surrogate.name))
            .expr()
            .eq(with.name.clone().c(sql::ident(surrogate.name)));

        let sql = sql::Select::default_tuples()
            .column(table.c(sql::ident(surrogate.name)))
            .column(table.c(sql::ident(primary.name)))
            .from(with.name.clone())
            .with(with)
            .join(sql::Join {
                op: sql::JoinOperator::Inner,
                target: table,
                on,
            })
            .as_sql();

        debug!(log, "{} ... ({} params omitted)", sql, chunk_len);

        let mut stmt = tx.prepare(&sql)?;

        let rows = stmt.query_map(chunk, |row| {
            let s: S = row.get(0)?;
            let p: P = row.get(1)?;
            Ok((s, Some(p)))
        })?;

        found.push(rows.collect::<Result<Vec<_>, rusqlite::Error>>()?);
    }

    map.extend(found.into_iter().flatten());

    return Ok(());
}

#[cfg(test)]
pub mod tests {
    use super::*;

    use uuid::Uuid;

    // TODO this syntax is weird, maybe generate the structs below, also Box the vector
    // contents because the structs are kind of big?
    model! {
        people(id: i64) Person {
            uuid: Uuid,
            name: String,
            // Does not work yet
            // @link fav_article: Option<Box<Article>> => articles(id),
            @link-back articles: Vec<Article> <= articles(author),
            @link-back comments: Vec<Comment> <= comments(author),
        }

        @log crate::test_logger()
    }

    has_sk!(Person uuid: Uuid);

    model! {
        articles(id: i64) Article {
            uuid: Uuid,
            slug: String,
            title: String,
            content: String,
            published_at: i64,
            @link author: Box<Person> => people(id),
            @link-back comments: Vec<Comment> <= comments(article),
        }

        @log crate::test_logger()
    }

    has_sk!(Article uuid: Uuid);

    model! {
        comments(id: i64) Comment {
            uuid: Uuid,
            content: String,
            published_at: i64,
            @link article: Box<Article> => articles(id),
            @link author: Box<Person> => people(id),
        }

        @log crate::test_logger()
    }

    has_sk!(Comment uuid: Uuid);

    #[test]
    fn test_query() -> rusqlite::Result<()> {
        let mut q = Q::<Article>::new();
        q.get.id().published_at().title();
        q.order_by.push(q.expr().published_at().asc());

        // // link back, comments point to an article
        // let mut comments_q = <Article as Modeled>::LinkSet::comments(&mut q);
        // comments_q.get.id().content();

        // // get article comment authors too
        // <Comment as Modeled>::LinkSet::author(&mut comments_q)
        //     .get
        //     .name();

        // link forward, articles reference an author
        let mut author_q =
            <Article as Modeled>::LinkSet::author(&mut q).get(|author| author.id().name());

        // get all the articles written by each the authors of the root list of articles
        <Person as Modeled>::LinkSet::articles(&mut author_q)
            .get(|article| article.title().published_at())
            .order_by(|article| article.published_at().desc());

        // // and all the comments on those articles ...
        // let mut more = <Article as Modeled>::LinkSet::comments(&mut more)
        //     .get(|comment| comment.content().published_at());

        // // and all those comment authors ...
        // <Comment as Modeled>::LinkSet::author(&mut more).get(|author| author.name());

        // ... filter must come near the end for some reason; I think maybe because we hold
        // references to linked Qs here so we aren't allowed to tamper with them further ...
        q.filter(|author| author.published_at().le(BIND));

        let until = 2000 as i64;
        q.params.push(Box::new(until));

        let mut conn = articles_database()?;
        let tx = conn.transaction()?;
        let wow = Article::do_select(&q, &tx).unwrap();
        eprintln!("{:#?}", wow);

        assert_eq!(
            // author names ... they're joined by the articles orderd by published asc
            wow.iter()
                .map(|article| {
                    article
                        .author
                        .as_ref()
                        .and_then(|author| author.name.as_ref())
                        .unwrap()
                })
                .map(String::as_str)
                .collect::<Vec<&str>>(),
            vec!["Spongebob", "Steve Carell", "Steve Carell"]
        );

        assert_eq!(
            // articles by the above authors, order by published desc ... backwards
            wow.iter()
                .map(|article: &Article| {
                    article
                        .author
                        .as_ref()
                        .and_then(|author| author.articles.as_ref())
                        .unwrap()
                        .into_iter()
                        .map(|article: &Article| article.title.as_ref().unwrap())
                        .map(String::as_str)
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>(),
            vec![
                vec!["Sponges are the best", "article title"],
                vec!["Anchorman", "The Office"],
                vec!["Anchorman", "The Office"],
            ]
        );

        Ok(())
    }

    #[test]
    pub fn test_write() -> Result<()> {
        let steve_carell = Person {
            id: Some(4).into(),
            ..Person::default()
        };

        let article = Article {
            slug: Some("anchorman-2".to_owned()).into(),
            title: Some("Anchorman 2".to_owned()).into(),
            content: Some("".to_owned()).into(),
            author: Some(Box::new(steve_carell)).into(),
            published_at: Some(6789).into(),
            ..Article::default()
        };

        let mut conn = articles_database()?;

        let tx = conn.transaction()?;

        let id = Article::insert(&article, &tx)?;

        Article::update(
            &Article {
                content: Some("I play Brick Tamland in this film.".to_owned()).into(),
                ..Article::default()
            },
            &id,
            &tx,
        )?;

        let mut q = Q::<Article>::new();
        q.get(|article| article.id().slug().title().content().published_at())
            .filter(|expr| expr.id().eq(BIND));
        q.params.push(Box::new(id.clone()));
        let articles = Article::do_select(&q, &tx)?;
        assert_eq!(
            articles.first().unwrap(),
            &Article {
                id: Some(id).into(),
                content: Some("I play Brick Tamland in this film.".to_owned()).into(),
                author: None.into(),
                ..article
            }
        );

        Ok(())
    }

    #[test]
    pub fn test_limit_param() -> Result<()> {
        let mut conn = articles_database()?;
        let tx = conn.transaction()?;

        let articles = Q::<Article>::new()
            .get(|a| a.title())
            .filter(|a| a.author().eq(BIND))
            .limit(Limit::Bind(Box::new(1)))
            .bind(Box::new(3))
            .list(&tx)?
            .into_iter()
            .map(|a| a.title.into_option().unwrap())
            .collect::<Vec<_>>();
        assert_eq!(articles, vec!["The Office"]);

        Ok(())
    }

    pub fn articles_database() -> rusqlite::Result<rusqlite::Connection> {
        use regex::Regex;
        use rusqlite::{functions::FunctionFlags, Error};

        let conn = rusqlite::Connection::open_in_memory()?;

        conn.create_scalar_function("uuid_generate_v4", 0, FunctionFlags::empty(), move |_| {
            Ok(Uuid::new_v4())
        })?;

        conn.create_scalar_function(
            "regexp",
            2,
            FunctionFlags::SQLITE_UTF8 | FunctionFlags::SQLITE_DETERMINISTIC,
            move |ctx| {
                let pat = ctx.get::<String>(0)?;
                let text = ctx.get::<String>(1)?;
                // TODO use cache compiled patterns
                let re = Regex::new(&pat).map_err(|e| Error::UserFunctionError(Box::new(e)))?;
                Ok(re.is_match(&text))
            },
        )?;

        conn.execute_batch(
            r#"
         create table people
             ( id       integer primary key
             , uuid     blob not null default (uuid_generate_v4())
             , name     text not null
             , unique (name)
             );

         create table articles
             ( id       integer primary key
             , uuid     blob not null default (uuid_generate_v4())
             , slug     text not null check ("slug" regexp '^[a-z0-9-]*$')
             , title    text not null
             , content  text default ''
             , author   integer not null references people
             , published_at integer not null
             , unique (slug)
             );


         alter table people add column
             fav_article integer references "articles";

         create table comments
             ( id       integer primary key
             , uuid     blob not null default (uuid_generate_v4())
             , article  integer not null references articles
             , content  text not null
             , author   integer not null references people
             , published_at integer not null
             );

         create table tags
             ( id       integer primary key
             , uuid     blob not null default (uuid_generate_v4())
             , name     text not null
             );

         insert into people (name)
              values ('Spongebob')
                   , ('Patrick Star')
                   , ('Steve Carell')
                   , ('Bob Ross');

         insert into articles (slug, title, content, author, published_at)
              values ('article', 'article title', 'article content', 1, 1234)
                   , ('the-office', 'The Office', 'a tv series', 3, 1235)
                   , ('anchorman', 'Anchorman', 'a film', 3, 1236)

                   , ('happy-little-trees', 'Happy Little Trees', '', 4, 2345)
                   , ('starfish', 'starfish title', 'starfish content', 2, 2346)

                   , ('fuck-starfish', 'Starfish are dumb, fuck them.', '', 4, 3456)

                   , ('sponges', 'Sponges are the best', '... because they are so absorbent', 1, 4567)
                   ;

         insert into comments (article, content, author, published_at)
              values (3, 'I love trees too!', 1, 1234)
                   , (3, 'Trees are useless, they do not even taste good.', 3, 2345)
                   , (4, 'Prison Mike doesn''t know anything about starfish because everyone in prison is someone''s biach.', 3, 3456)
                   , (5, 'I love lamp.', 3, 5678)
                   ;
         "#,
        )?;
        Ok(conn)
    }
}
