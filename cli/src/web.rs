//! https://url.spec.whatwg.org/#c0-control-percent-encode-set

use std::string::ToString;

use percent_encoding::{utf8_percent_encode, AsciiSet, PercentEncode, CONTROLS};

/// The fragment percent-encode set is the C0 control percent-encode set and U+0020
/// SPACE, U+0022 ("), U+003C (<), U+003E (>), and U+0060 (`).
pub const FRAGMENT: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');

/// The path percent-encode set is the fragment percent-encode set and U+0023 (#),
/// U+003F (?), U+007B ({), and U+007D (}).
pub const PATH: &AsciiSet = &FRAGMENT.add(b'#').add(b'?').add(b'{').add(b'}');

pub fn encode_path<'a>(thing: &'a str) -> PercentEncode<'a> {
    utf8_percent_encode(thing, PATH)
}

pub trait JoinWith {
    fn join_with<S1, S2>(&self, sep: S1, tail: S2) -> String
    where
        S1: AsRef<str>,
        S2: AsRef<str>,
        Self: ToString,
    {
        let mut s = self.to_string();
        let sep = sep.as_ref();
        let tail = tail.as_ref();

        if s.ends_with(sep) != tail.starts_with(sep) {
            s.push_str(tail);
        } else if tail.starts_with(sep) {
            s.push_str(&tail[1..]);
        } else {
            s.push_str(sep);
            s.push_str(tail);
        }

        s
    }
}

impl<I: ToString> JoinWith for I {}

#[test]
fn test() {
    assert_eq!(&"url:/".join_with("/", "/hi"), "url:/hi");
    assert_eq!(&"url:".join_with("/", "/hi"), "url:/hi");
    assert_eq!(&"url:/".join_with("/", "hi"), "url:/hi");
    assert_eq!(&"url:".join_with("/", "hi"), "url:/hi");
}
