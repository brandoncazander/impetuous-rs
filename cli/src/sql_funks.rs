//! Application defined functions called from SQLite registered to a connection.

use crate::time::DateTime;
use regex::Regex;
use rusqlite::Error;
use std::sync::Arc;
use uuid::Uuid;

/// Registers:
/// - uuid_generate_v4
/// - regexp
pub fn add_functions(conn: &mut rusqlite::Connection) -> rusqlite::Result<()> {
    use rusqlite::functions::FunctionFlags;

    conn.create_scalar_function("uuid_generate_v4", 0, FunctionFlags::empty(), move |_| {
        Ok(Uuid::new_v4())
    })?;

    conn.create_scalar_function("utc_now", 0, FunctionFlags::empty(), move |_| {
        Ok(DateTime::now())
    })?;

    conn.create_scalar_function(
        "regexp",
        2,
        FunctionFlags::SQLITE_UTF8 | FunctionFlags::SQLITE_DETERMINISTIC,
        move |ctx| {
            let re: Arc<Regex> = ctx.get_or_create_aux(0, |pat| -> anyhow::Result<_> {
                Ok(Regex::new(pat.as_str()?)?)
            })?;

            let text = ctx
                .get_raw(1)
                .as_str()
                .map_err(|e| Error::UserFunctionError(e.into()))?;

            Ok(re.is_match(text))
        },
    )?;

    Ok(())
}
