//! This is used to build SQL query strings. It's very bad and very hard to use :(
//!
//! Designed for SQLite.
use std::convert;
use std::fmt::Debug;
use std::iter::{repeat_with, FromIterator};

pub mod cursored_get;
pub mod from_columns;

pub use cursored_get::CursoredGet;
pub use from_columns::FromColumns;

pub mod traits {
    pub use super::BuildSql;
}

// todo identifier type support Cow or no-copy thing like str from bytes
pub fn quote_ident(s: &str) -> String {
    format!(r#""{}""#, s.replace("\"", "\"\""))
}

pub trait BuildSql {
    fn build_sql(&self, s: &mut String) -> ();

    fn as_sql(&self) -> String {
        let mut s = String::new();
        self.build_sql(&mut s);
        s
    }
}

// holy shit fucking end me please
impl<T: ?Sized> BuildSql for &'_ T
where
    T: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        (*self).build_sql(s)
    }
}

// impl<T: ?Sized> BuildSql for &'_ mut T
// where
//     T: BuildSql,
// {
//     fn build_sql(&self, s: &mut String) {
//         (**self).build_sql(s)
//     }
// }

impl<T: ?Sized> BuildSql for Box<T>
where
    T: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        (**self).build_sql(s)
    }
}

/// this is useful for type-parameterized BuildSql implementations
impl BuildSql for () {
    fn build_sql(&self, _: &mut String) -> () {
        ()
    }
}

pub trait IsEmpty {
    fn is_empty(&self) -> bool;
}

impl<I> IsEmpty for Vec<I> {
    fn is_empty(&self) -> bool {
        self.is_empty()
    }
}

impl<T> IsEmpty for Option<T> {
    fn is_empty(&self) -> bool {
        self.is_none()
    }
}

impl IsEmpty for () {
    fn is_empty(&self) -> bool {
        true
    }
}

pub trait DelimitSql {
    fn delimit_sql(&self, delimiter: &str, s: &mut String);
}

// /// if T is clonable and implements MaybeSql, then MaybeSql is implemented on &T
// impl<T: ?Sized> DelimitSql for &'_ T
// where
//     T: MaybeSql + Clone,
// {
//     fn maybe_sql(&self) -> Option<Self::Iter> {
//         (*self).clone().maybe_sql()
//     }
// }

impl DelimitSql for () {
    fn delimit_sql(&self, _delimiter: &str, _s: &mut String) {
        ()
    }
}

impl<E> DelimitSql for Vec<E>
where
    E: BuildSql,
{
    fn delimit_sql(&self, delimiter: &str, s: &mut String) {
        for (i, e) in self.iter().enumerate() {
            if i > 0 {
                s.push_str(delimiter);
            }
            e.build_sql(s);
        }
    }
}

pub trait PushSql<I> {
    type Out; //: PushSql<I>;

    fn push_sql(self, _: I) -> Self::Out;
}

impl<I> PushSql<I> for () {
    type Out = I;

    fn push_sql(self, v: I) -> Self::Out {
        v
    }
}

impl<I> PushSql<I> for &mut Vec<I> {
    type Out = ();

    fn push_sql(self, v: I) -> Self::Out {
        self.push(v);
    }
}

impl<E> BuildSql for Vec<E>
where
    E: BuildSql,
{
    fn build_sql(&self, s: &mut String) -> () {
        Separated::by_comma(&self[..]).build_sql(s)
    }
}

#[derive(Debug)]
pub struct ExprTuple<E>(E);

impl<V> ExprTuple<(V,)> {
    /// builds a 1-tuple from the given value
    pub fn new(v: V) -> Self {
        ExprTuple((v,))
    }
}

macro_rules! _fucking_tuple_shit {
    ( $head:ident $($tail:ident)* ) => {

        /// impl BuildSql; this is sketchy because it's just DelimitSql with commas and may not be
        /// what you want...
        impl<$head, $($tail),*> BuildSql for ExprTuple<($head, $($tail),*)>
        where
            $head: BuildSql,
            $( $tail: BuildSql ),*
        {
             fn build_sql(&self, s: &mut String) -> () {
                 #[allow(non_snake_case)]
                 let ExprTuple(($head, $($tail),*)) = self;
                 $head.build_sql(s);
                 $(
                     s.push_str(", ");
                     $tail.build_sql(s);
                 )*
             }
        }

        // impl IsEmpty
        impl<$head, $($tail),*> IsEmpty for ExprTuple<($head, $($tail),*)> {
            fn is_empty(&self) -> bool {
                false
            }
        }

        impl<'a, $head: BuildSql, $($tail: BuildSql),*> DelimitSql for ExprTuple<($head, $($tail),*)> {
            fn delimit_sql(&self, _delimiter: &str, s: &mut String) {
                 #[allow(non_snake_case)]
                let ExprTuple(($head, $($tail),*)) = self;
                $head.build_sql(s);
                $(
                    s.push_str(_delimiter);
                    $tail.build_sql(s);
                )*
            }
        }

        // impl PushSql
        impl<Ext, $head, $($tail),*> PushSql<Ext> for ExprTuple<($head, $($tail),*)> {
            type Out = ExprTuple<($head, $($tail,)* Ext)>;

            fn push_sql(self, ext: Ext) -> Self::Out {
                 #[allow(non_snake_case)]
                let ExprTuple(($head, $($tail),*)) = self;
                ExprTuple(($head, $($tail,)* ext))
            }
        }

        // impl convert::From
        impl<$head, $($tail),*> convert::From<($head, $($tail),*)> for ExprTuple<($head, $($tail),*)> {
            fn from(tuple: ($head, $($tail),*)) -> Self {
                ExprTuple(tuple)
            }
        }

        _fucking_tuple_shit!($( $tail )*);
    };

    () => {
        impl Default for ExprTuple<()>
        {
            fn default() -> Self {
                ExprTuple(())
            }
        }

        impl BuildSql for ExprTuple<()>
        {
             fn build_sql(&self, _: &mut String) -> () {
                 ();
             }
        }

        impl IsEmpty for ExprTuple<()> {
            fn is_empty(&self) -> bool {
                true
            }
        }

        impl DelimitSql for ExprTuple<()> {
            fn delimit_sql(&self, _delimiter: &str, _s: &mut String) {
                ()
            }
        }

        impl<Ext> PushSql<Ext> for ExprTuple<()> {
            type Out = ExprTuple<(Ext,)>;

            fn push_sql(self, ext: Ext) -> Self::Out {
                ExprTuple((ext,))
            }
        }

    };
}

_fucking_tuple_shit!(A B C D E F G H I);

#[derive(Debug, Clone)]
pub struct Ident<S>(pub S);

impl<S> Ident<S> {
    pub fn alias<A>(self, alias: A) -> Aliased<Self, A>
    where
        Self: Sized,
        A: BuildSql,
    {
        Aliased::new(self, alias)
    }
}

pub const fn ident<S>(s: S) -> Ident<S> {
    Ident(s)
}

// impl<S: AsRef<str>> BuildSql for Ident<S> {
//     = note: upstream crates may add a new impl of trait
//     `std::convert::AsRef<str>` for type `u32` in future versions
// as if I fucking care ... ^^^^^^^^^^^^^^^^^^^^
impl<'a> BuildSql for Ident<&'a str> {
    fn build_sql(&self, s: &mut String) {
        s.push_str(&quote_ident(self.0));
    }
}

impl BuildSql for Ident<u32> {
    fn build_sql(&self, s: &mut String) {
        use std::fmt::Write;

        let _ = write!(s, "_a{}", self.0);
    }
}

impl ColumnSource for Ident<u32> {
    type Source = Self;

    fn c<O>(self, o: O) -> SelectColumn<Self, O>
    where
        Self: Sized,
    {
        SelectColumn(self, o)
    }
}

impl<T> Ident<T>
where
    Self: BuildSql,
{
    pub fn to_escaped(&self) -> EscapedIdent {
        let mut escaped = String::new();
        self.build_sql(&mut escaped);
        EscapedIdent(escaped)
    }
}

#[derive(Debug)]
pub struct EscapedIdent(String);

/// TODO make a EscapedIdent type so we don't have to keep checking strings ...
/// you have a type system ...
impl BuildSql for EscapedIdent {
    fn build_sql(&self, s: &mut String) {
        s.push_str(self.0.as_ref());
    }
}

impl<C> convert::From<EscapedIdent> for Expr<C> {
    fn from(quoted: EscapedIdent) -> Self {
        Expr::Ident(quoted)
    }
}

#[derive(Debug, Clone)]
pub struct Aliased<T, A> {
    pub inner: T,
    pub alias: A,
}

impl<T, A> Aliased<T, A> {
    pub fn new(inner: T, alias: A) -> Self {
        Aliased { inner, alias }
    }
}

impl<T, A> ColumnSource for &Aliased<T, A>
where
    A: Clone,
{
    type Source = A;

    fn c<O>(self, other: O) -> SelectColumn<A, O>
    where
        Self: Sized,
    {
        SelectColumn(self.alias.clone(), other)
    }
}

impl<T, A> BuildSql for Aliased<T, A>
where
    T: BuildSql,
    A: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        self.inner.build_sql(s);
        s.push_str(" AS ");
        self.alias.build_sql(s);
    }
}

// pub trait Aliasable {
//     fn alias<A>(self, alias: A) -> Aliased<Self, A>
//     where
//         Self: BuildSql + Sized,
//         A: BuildSql,
//     {
//         Aliased::new(self, alias)
//     }
// }
//
// impl<B: BuildSql> Aliasable for B {}

#[derive(Debug, Clone)]
pub struct SelectColumn<T, C>(T, C);

impl<T, C> SelectColumn<T, C> {
    pub fn new(t: T, c: C) -> SelectColumn<T, C> {
        SelectColumn(t, c)
    }

    pub fn expr(self) -> Expr<Self> {
        Expr::Column(self)
    }
}

impl<T, C> BuildSql for SelectColumn<T, C>
where
    T: BuildSql,
    C: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        self.0.build_sql(s);
        s.push_str(".");
        self.1.build_sql(s);
    }
}

//

#[derive(Debug, Clone)]
struct Separated<'a, S, B: BuildSql> {
    pub sep: S,
    pub items: &'a [B],
}

impl<'a, S, B: BuildSql> Separated<'a, S, B> {
    fn by(sep: S, items: &'a [B]) -> Self {
        Separated { sep, items }
    }
}

impl<'a, B: BuildSql> Separated<'a, &'static str, B> {
    fn by_comma(items: &'a [B]) -> Self {
        Separated { sep: ", ", items }
    }
}

impl<'a, B: BuildSql> BuildSql for Separated<'a, &'static str, B> {
    fn build_sql(&self, s: &mut String) -> () {
        for (i, term) in self.items.into_iter().enumerate() {
            if i > 0 {
                s.push_str(self.sep);
            }

            term.build_sql(s);
        }
    }
}

impl<'a, S: BuildSql, B: BuildSql> BuildSql for Separated<'a, S, B> {
    fn build_sql(&self, s: &mut String) -> () {
        for (i, term) in self.items.into_iter().enumerate() {
            if i > 0 {
                self.sep.build_sql(s);
            }

            term.build_sql(s);
        }
    }
}

/// todo use MaybeSql here once that API is less stupid (no &dyn BuildSql)
#[derive(Debug, Clone, Default)]
pub struct Values<V> {
    items: Vec<V>,
}

impl<V> Values<V> {
    pub fn new(items: Vec<V>) -> Self {
        Values { items }
    }

    pub fn one(value: V) -> Self {
        Values { items: vec![value] }
    }
}

impl<V> FromIterator<V> for Values<V> {
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = V>,
    {
        Values::new(iter.into_iter().collect())
    }
}

impl<V> BuildSql for Values<V>
where
    V: BuildSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        match self.items.first().map(IsEmpty::is_empty) {
            None | Some(true) => {
                s.push_str("DEFAULT VALUES");
                return;
            }
            _ => (),
        };

        s.push_str("VALUES (");

        Separated::by("), (", &self.items[..]).build_sql(s);

        s.push_str(")");
    }
}

/// TODO for next refactor, if this can implement Clone that would be nice
#[derive(Debug)]
pub enum Expr<C> {
    Null,
    /// Literals must be static because it simplifies the type signature AND you probably don't
    /// want to be coming up with literals dynamically anyway ...
    Literal(&'static str),
    Bind,
    Binary(BinOperator, Vec<Expr<C>>),
    // Unary(UnaryOperator, Box<Expr<C>>),
    /// technically this is whatever type ... it's just supposed to be for columns I guess?
    Column(C),
    /// ...
    Ident(EscapedIdent),
    /// For sub-selects ... wraps the whatever here in parenthesis ...
    Sub(Box<dyn DebugBuildSql>),
    // /// [NOT] EXISTS (select ...)
    // Exists(C),
    // /// expr [NOT] IN (select ...) TODO my life is in shambles
    // InSelect(Expr<C>, bool, ()),
}

impl<C> IsEmpty for Expr<C> {
    fn is_empty(&self) -> bool {
        false // probably not technically correct for Expr::Sub ...
    }
}

impl<C: BuildSql> BuildSql for Expr<C> {
    fn build_sql(&self, s: &mut String) {
        match self {
            Expr::Null => s.push_str("NULL"),
            Expr::Literal(lit) => s.push_str(lit.as_ref()),
            Expr::Bind => s.push_str("?"),
            Expr::Binary(op, exprs) => {
                // with any hope, wrapping logical operators in () will solve any order of
                // evaluation problems?????????
                let is_logic = match op {
                    BinOperator::And | BinOperator::Or => true,
                    _ => false,
                };

                if is_logic {
                    s.push_str("(");
                }

                Separated::by(op.clone(), &exprs[..]).build_sql(s);

                if is_logic {
                    s.push_str(")");
                }
            }
            Expr::Column(column) => column.build_sql(s),
            Expr::Ident(ident) => ident.build_sql(s),
            Expr::Sub(whatever) => {
                s.push_str("(");
                whatever.build_sql(s);
                s.push_str(")");
            }
        }
    }
}

pub trait DebugBuildSql: Debug + BuildSql {}

impl<B: Debug + BuildSql> DebugBuildSql for B {}

pub const fn lit<C>(s: &'static str) -> Expr<C> {
    Expr::Literal(s)
}

pub const fn bind<C>() -> Expr<C> {
    Expr::Bind
}

pub fn binds<C>(n: usize) -> impl Iterator<Item = Expr<C>> {
    repeat_with(|| bind::<C>()).take(n)
}

// TODO move alias into a trait? cause we already implement it three times
impl<C> Expr<C> {
    pub fn alias<A>(self, alias: A) -> Aliased<Self, A>
    where
        Self: Sized,
        A: BuildSql,
    {
        Aliased::new(self, alias)
    }
}

impl<A, B> convert::From<SelectColumn<A, B>> for Expr<SelectColumn<A, B>> {
    fn from(other: SelectColumn<A, B>) -> Self {
        Expr::Column(other)
    }
}

macro_rules! op {
    ($name:ident -> $variant:expr) => {
        pub fn $name<O: Into<Self>>(self, other: O) -> Self {
            Expr::Binary($variant, vec![self, other.into()])
        }
    }
}

impl<C> Expr<C> {
    op!(eq -> BinOperator::Eq);
    op!(ne -> BinOperator::Ne);
    op!(gt -> BinOperator::Gt);
    op!(ge -> BinOperator::Ge);
    op!(lt -> BinOperator::Lt);
    op!(le -> BinOperator::Le);
    op!(is -> BinOperator::Is);
    op!(is_not -> BinOperator::IsNot);
    op!(and -> BinOperator::And);
    op!(or -> BinOperator::Or);
    op!(is_in -> BinOperator::In);

    // /// [NOT] IN (expr,*)
    // /// [NOT] IN (select ...) <- only this is supported I guess ...???
    // /// [NOT] IN table
    // /// TODO improve this and fix the expression type??????
    // pub fn is_in2<O: BuildSql>(self, other: O) -> Expr<C> {
    //     Expr::In(self, true, other)
    // }

    pub fn is_null(self) -> Expr<C> {
        self.is(Expr::Null)
    }

    pub fn is_not_null(self) -> Expr<C> {
        self.is_not(Expr::Null)
    }

    pub fn desc(self) -> OrderBy<C> {
        (self, Order::Desc)
    }

    pub fn asc(self) -> OrderBy<C> {
        (self, Order::Asc)
    }
}

pub trait Expr2: DebugBuildSql {}

impl<T: ?Sized> Expr2 for &'_ T where T: Expr2 {}

impl<I> Expr2 for Ident<I> where Ident<I>: BuildSql + Debug {}

impl<A: Expr2, B: Expr2> Expr2 for SelectColumn<A, B> {}

#[derive(Debug, Clone)]
pub enum SimpleExpr {
    Null,
    /// Literals must be static because it simplifies the type signature AND you probably don't
    /// want to be coming up with literals dynamically anyway ...
    Literal(&'static str),
    Bind,
}

impl Expr2 for SimpleExpr {}

impl BuildSql for SimpleExpr {
    fn build_sql(&self, s: &mut String) {
        match self {
            SimpleExpr::Null => s.push_str("NULL"),
            SimpleExpr::Literal(lit) => s.push_str(lit.as_ref()),
            SimpleExpr::Bind => s.push_str("?"),
        }
    }
}

/// warning, this kind of only works if the rh side is a sub-select ...
#[derive(Debug)]
pub struct InExpression<A: Expr2, B: BuildSql>(A, B);

impl<A, B> BuildSql for InExpression<A, B>
where
    A: Expr2 + BuildSql,
    B: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        self.0.build_sql(s);
        s.push_str(" IN (");
        self.1.build_sql(s);
        s.push_str(")");
    }
}

impl<A: Expr2, B: Debug + BuildSql> Expr2 for InExpression<A, B> {}

#[derive(Debug)]
pub struct BinaryExpression<A: Expr2, B: Expr2>(A, BinOperator, B);

impl<A, B> BuildSql for BinaryExpression<A, B>
where
    A: Expr2 + BuildSql,
    B: Expr2 + BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        self.0.build_sql(s);
        self.1.build_sql(s);
        self.2.build_sql(s);
    }
}

impl<A: Expr2, B: Expr2> Expr2 for BinaryExpression<A, B> {}

// pub struct BinaryExpr<e> {
//     op: BinOperator,
//     expr: e,
// }
//
// impl BuildSql for BinaryExpr {}

// impl<I, O> Extend<I> for BinOperator<O> {
//     where O: Extend<I>
// }

#[derive(Debug, PartialEq, Clone)]
pub enum BinOperator {
    // ILike,
    Eq,
    Ne,
    Gt,
    Ge,
    Lt,
    Le,
    Is,
    IsNot,
    And,
    Or,
    // TODO in is not a proper binary operator, the right hand side is not another expression ...
    // it's like a sub-select or table or some such shit ... FIXME
    In,
}

impl BuildSql for BinOperator {
    fn build_sql(&self, s: &mut String) {
        use BinOperator::*;

        match self {
            Eq => s.push_str(" = "),
            Ne => s.push_str(" != "),
            Gt => s.push_str(" > "),
            Ge => s.push_str(" >= "),
            Lt => s.push_str(" < "),
            Le => s.push_str(" <= "),
            Is => s.push_str(" IS "),
            IsNot => s.push_str(" IS NOT "),
            And => s.push_str(" AND "),
            Or => s.push_str(" OR "),
            In => s.push_str(" IN "),
        }
    }
}

// #[derive(Debug)]
// pub enum UnaryOperator {
//     Not,
// }

/// todo support nulls first, nulls last (we don't need collate)
pub type OrderBy<C> = (Expr<C>, Order);

#[derive(Debug, PartialEq, Clone)]
pub enum Order {
    Asc,
    Desc,
}

impl<T: BuildSql> BuildSql for (T, Order) {
    fn build_sql(&self, s: &mut String) {
        self.0.build_sql(s);
        match self.1 {
            Order::Asc => s.push_str(" ASC"),
            Order::Desc => s.push_str(" DESC"),
        };
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum JoinOperator {
    Left,
    Inner,
}

/// A _limited_ (ha ha ...) kind of expression thing for values in the LIMIT clause
/// because I don't want to add more generics to Select to allow limit to be any expression
#[derive(Debug)]
pub enum Limit {
    N(i64),
    Bind,
}

impl Default for Limit {
    fn default() -> Self {
        // I don't really know why this is valid. I think because SQLite's limit value
        // is a signed 64-bit integer, so u64 isn't quite right.
        //
        // And zero is a valid value too I suppose. But there is no difference between
        // zero and anything less than that.
        //
        // Eh... whatever...
        Limit::N(-1)
    }
}

impl From<i64> for Limit {
    fn from(v: i64) -> Limit {
        Limit::N(v)
    }
}

/// the same as [Select::default_tuples()]
pub fn select(
) -> Select<ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>>
{
    Select::default_tuples()
}

////////////////////////////////////////////////////////////////////////////////

/// A useful trait that lets you write "foo"."bar" where "foo" is a ColumnSource.
///
/// This is generic to accomodate selecting columns from aliases or with clauses and stuff.
pub trait ColumnSource {
    type Source;

    fn c<O>(self, _: O) -> SelectColumn<Self::Source, O>
    where
        Self: Sized;
}

#[derive(Debug)]
pub struct Join<Tg, On> {
    pub op: JoinOperator,
    pub target: Tg,
    pub on: On,
}

impl<Tg, On> BuildSql for Join<Tg, On>
where
    Tg: BuildSql,
    On: BuildSql,
{
    fn build_sql(&self, s: &mut String) {
        let op = match self.op {
            JoinOperator::Left => "LEFT JOIN ",
            JoinOperator::Inner => "INNER JOIN ",
        };
        s.push_str(op);
        self.target.build_sql(s);
        s.push_str(" ON ");
        self.on.build_sql(s);
    }
}

#[derive(Debug)]
pub struct Select<Se, Fr, Wi, Fi, Jo, Or> {
    pub columns: Se,
    pub from: Fr,
    pub with: Wi,
    pub filter: Fi,
    pub join: Jo,
    pub limit: Limit,
    pub order_by: Or,
}

impl<Se, Fr, Wi, Fi, Jo, Or> BuildSql for Select<Se, Fr, Wi, Fi, Jo, Or>
where
    Se: BuildSql,
    Fr: BuildSql + IsEmpty,
    Wi: BuildSql + IsEmpty,
    Fi: DelimitSql + IsEmpty,
    Jo: DelimitSql + IsEmpty,
    Or: BuildSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        if !self.with.is_empty() {
            s.push_str("WITH ");
            self.with.build_sql(s);
            s.push_str(" ");
        }

        s.push_str("SELECT ");
        self.columns.build_sql(s);

        if !self.from.is_empty() {
            s.push_str(" FROM ");
            self.from.build_sql(s);
        }

        if !self.join.is_empty() {
            s.push_str(" ");
            self.join.delimit_sql(" ", s);
        }

        if !self.filter.is_empty() {
            s.push_str(" WHERE ");
            self.filter.delimit_sql(" AND ", s);
        }

        if !self.order_by.is_empty() {
            s.push_str(" ORDER BY ");
            self.order_by.build_sql(s);
        }

        match self.limit {
            Limit::N(n) if n >= 0 => {
                // I think LIMIT 0 is a thing, I don't know what the point of it is, but sqlite will
                // only ignore the LIMIT clause if it's negative...
                s.push_str(" LIMIT ");
                // I wonder if writing this string is faster if we convert this to a u64 first?
                use std::fmt::Write;
                let _ = write!(s, "{}", n);
            }
            Limit::N(_) => (),
            Limit::Bind => s.push_str(" LIMIT ?"),
        }
    }
}

impl<Se, Fr, Wi, Fi, Jo, Or> Default for Select<Se, Fr, Wi, Fi, Jo, Or>
where
    Se: Default,
    Fr: Default,
    Wi: Default,
    Fi: Default,
    Jo: Default,
    Or: Default,
{
    fn default() -> Self {
        Select {
            columns: Default::default(),
            from: Default::default(),
            with: Default::default(),
            filter: Default::default(),
            join: Default::default(),
            limit: Limit::from(-1),
            order_by: Default::default(),
        }
    }
}

impl
    Select<ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>>
{
    pub fn default_tuples() -> Self {
        Self::default()
    }
}

impl<Se, Fr, Wi, Fi, Jo, Or> Select<Se, Fr, Wi, Fi, Jo, Or> {
    pub fn column<V>(self, v: V) -> Select<<Se as PushSql<V>>::Out, Fr, Wi, Fi, Jo, Or>
    where
        V: BuildSql,
        Se: PushSql<V>,
        <Se as PushSql<V>>::Out: BuildSql,
    {
        Select {
            columns: self.columns.push_sql(v),
            from: self.from,
            with: self.with,
            filter: self.filter,
            join: self.join,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn from<V>(self, v: V) -> Select<Se, <Fr as PushSql<V>>::Out, Wi, Fi, Jo, Or>
    where
        V: BuildSql,
        Fr: PushSql<V>,
        <Fr as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Select {
            columns: self.columns,
            from: self.from.push_sql(v),
            with: self.with,
            filter: self.filter,
            join: self.join,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn with<V>(self, v: V) -> Select<Se, Fr, <Wi as PushSql<V>>::Out, Fi, Jo, Or>
    where
        V: BuildSql,
        Wi: PushSql<V>,
        <Wi as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Select {
            columns: self.columns,
            from: self.from,
            with: self.with.push_sql(v),
            filter: self.filter,
            join: self.join,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn filter<V>(self, v: V) -> Select<Se, Fr, Wi, <Fi as PushSql<V>>::Out, Jo, Or>
    where
        V: BuildSql,
        Fi: PushSql<V>,
        <Fi as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Select {
            columns: self.columns,
            from: self.from,
            with: self.with,
            filter: self.filter.push_sql(v),
            join: self.join,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn join<V>(self, v: V) -> Select<Se, Fr, Wi, Fi, <Jo as PushSql<V>>::Out, Or>
    where
        V: BuildSql,
        Jo: PushSql<V>,
        <Jo as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Select {
            columns: self.columns,
            from: self.from,
            with: self.with,
            filter: self.filter,
            limit: self.limit,
            join: self.join.push_sql(v),
            order_by: self.order_by,
        }
    }

    pub fn order_by<V>(self, v: V) -> Select<Se, Fr, Wi, Fi, Jo, <Or as PushSql<V>>::Out>
    where
        V: BuildSql,
        Or: PushSql<V>,
        <Or as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Select {
            columns: self.columns,
            from: self.from,
            with: self.with,
            filter: self.filter,
            limit: self.limit,
            join: self.join,
            order_by: self.order_by.push_sql(v),
        }
    }

    pub fn limit<I: Into<Limit>>(self, limit: I) -> Self {
        Select {
            limit: limit.into(),
            ..self
        }
    }
}

impl<Se, Fr, Wi, Fi, Jo, Or> Select<Se, Fr, Wi, Fi, Jo, Or> {
    pub fn add_column<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        // I know this is a higher-ranked trait bound, (or a HRTB as they say on the streets) ...
        // I don't know what this means, but sometimes it makes the compiler happy.
        for<'a> &'a mut Se: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.columns, v);
        self
    }

    pub fn add_from<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Fr: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.from, v);
        self
    }

    pub fn add_with<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Wi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.with, v);
        self
    }

    pub fn add_filter<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Fi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.filter, v);
        self
    }

    pub fn add_join<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Jo: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.join, v);
        self
    }

    pub fn add_order_by<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Or: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.order_by, v);
        self
    }

    pub fn set_limit<I: Into<Limit>>(&mut self, limit: I) -> &mut Self {
        self.limit = limit.into();
        self
    }
}

impl<'a, St, Na, Co> ColumnSource for &'a WithAs<St, Na, Co> {
    type Source = &'a Na;

    fn c<O>(self, o: O) -> SelectColumn<Self::Source, O>
    where
        Self: Sized,
    {
        SelectColumn(&self.name, o)
    }
}

#[derive(Debug)]
pub struct WithAs<St, Na, Co> {
    pub stmt: St,
    pub name: Na,
    pub columns: Co,
}

impl<St, Na, Co> BuildSql for WithAs<St, Na, Co>
where
    St: BuildSql,
    Na: BuildSql,
    Co: BuildSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        self.name.build_sql(s);
        if !self.columns.is_empty() {
            s.push_str(" (");
            self.columns.build_sql(s);
            s.push_str(") AS (");
        } else {
            s.push_str(" AS (");
        }
        self.stmt.build_sql(s);
        s.push_str(")");
    }
}

#[derive(Debug)]
pub struct Update<Ta, St, Wi, Fi, Or> {
    table: Ta,
    set: St,
    with: Wi,
    filter: Fi,
    order_by: Or,
    limit: i64,
}

impl<Ta, St, Wi, Fi, Or> BuildSql for Update<Ta, St, Wi, Fi, Or>
where
    Ta: BuildSql,
    St: BuildSql,
    Wi: BuildSql + IsEmpty,
    Fi: DelimitSql + IsEmpty,
    Or: BuildSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        if !self.with.is_empty() {
            s.push_str("WITH ");
            self.with.build_sql(s);
            s.push_str(" ");
        }

        s.push_str("UPDATE ");

        self.table.build_sql(s);

        s.push_str(" SET ");

        self.set.build_sql(s);

        if !self.filter.is_empty() {
            s.push_str(" WHERE ");
            self.filter.delimit_sql(" AND ", s);
        }
    }
}

impl<Ta, St, Wi, Fi, Or> Default for Update<Ta, St, Wi, Fi, Or>
where
    Ta: Default,
    St: Default,
    Wi: Default,
    Fi: Default,
    Or: Default,
{
    fn default() -> Self {
        Update {
            table: Default::default(),
            set: Default::default(),
            with: Default::default(),
            filter: Default::default(),
            limit: -1,
            order_by: Default::default(),
        }
    }
}

impl Update<ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>> {
    pub fn default_tuples() -> Self {
        Self::default()
    }
}

impl<Ta, St, Wi, Fi, Or> Update<Ta, St, Wi, Fi, Or> {
    pub fn table<V>(self, v: V) -> Update<V, St, Wi, Fi, Or>
    where
        V: BuildSql,
    {
        Update {
            with: self.with,
            table: v,
            set: self.set,
            filter: self.filter,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn set<V>(self, v: V) -> Update<Ta, <St as PushSql<V>>::Out, Wi, Fi, Or>
    where
        V: BuildSql,
        St: PushSql<V>,
        <St as PushSql<V>>::Out: BuildSql,
    {
        Update {
            with: self.with,
            table: self.table,
            set: self.set.push_sql(v),
            filter: self.filter,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn with<V>(self, v: V) -> Update<Ta, St, <Wi as PushSql<V>>::Out, Fi, Or>
    where
        V: BuildSql,
        Wi: PushSql<V>,
        <Wi as PushSql<V>>::Out: BuildSql,
    {
        Update {
            with: self.with.push_sql(v),
            table: self.table,
            set: self.set,
            filter: self.filter,
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn filter<V>(self, v: V) -> Update<Ta, St, Wi, <Fi as PushSql<V>>::Out, Or>
    where
        V: BuildSql,
        Fi: PushSql<V>,
        <Fi as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Update {
            with: self.with,
            table: self.table,
            set: self.set,
            filter: self.filter.push_sql(v),
            limit: self.limit,
            order_by: self.order_by,
        }
    }

    pub fn order_by<V>(self, v: V) -> Update<Ta, St, Wi, Fi, <Or as PushSql<V>>::Out>
    where
        V: BuildSql,
        Or: PushSql<V>,
        <Or as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Update {
            with: self.with,
            table: self.table,
            set: self.set,
            filter: self.filter,
            limit: self.limit,
            order_by: self.order_by.push_sql(v),
        }
    }

    pub fn limit(self, limit: i64) -> Self {
        Update { limit, ..self }
    }

    pub fn add_set<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut St: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.set, v);
        self
    }

    pub fn add_with<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Wi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.with, v);
        self
    }

    pub fn add_filter<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Fi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.filter, v);
        self
    }

    pub fn add_order_by<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Or: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.order_by, v);
        self
    }

    pub fn set_limit(&mut self, limit: i64) -> &mut Self {
        self.limit = limit;
        self
    }
}

#[derive(Debug)]
pub struct Insert<Ta, Co, Ex, Wi, Up> {
    table: Ta,
    columns: Co,
    expr: Ex,
    with: Wi,
    upsert: Up,
}

impl<Ta, Co, Ex, Wi, Up> BuildSql for Insert<Ta, Co, Ex, Wi, Up>
where
    Ta: BuildSql,
    Co: BuildSql,
    Ex: BuildSql,
    Wi: BuildSql + IsEmpty,
    Up: BuildSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        if !self.with.is_empty() {
            s.push_str("WITH ");
            self.with.build_sql(s);
            s.push_str(" ");
        }

        s.push_str("INSERT INTO ");
        self.table.build_sql(s);

        // eh, using is_empty here doesn't really work
        // because an ExprTuple<Vec<_>> is not empty even if the Vec is
        let columns = self.columns.as_sql();
        if !columns.is_empty() {
            s.push_str(" (");
            s.push_str(&columns);
            s.push_str(") ");
        } else {
            s.push_str(" ");
        }

        self.expr.build_sql(s);

        if !self.upsert.is_empty() {
            s.push_str(" ");
            self.upsert.build_sql(s);
        }
    }
}

impl<Ta, Co, Ex, Wi, Up> Default for Insert<Ta, Co, Ex, Wi, Up>
where
    Ta: Default,
    Co: Default,
    Ex: Default,
    Wi: Default,
    Up: Default,
{
    fn default() -> Self {
        Insert {
            table: Default::default(),
            columns: Default::default(),
            expr: Default::default(),
            with: Default::default(),
            upsert: Default::default(),
        }
    }
}

impl Insert<ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>, ExprTuple<()>> {
    pub fn default_tuples() -> Self {
        Self::default()
    }
}

impl<Ta, Co, Ex, Wi, Up> Insert<Ta, Co, Ex, Wi, Up> {
    pub fn table<V>(self, v: V) -> Insert<V, Co, Ex, Wi, Up>
    where
        V: BuildSql,
    {
        Insert {
            with: self.with,
            table: v,
            columns: self.columns,
            expr: self.expr,
            upsert: self.upsert,
        }
    }

    pub fn columns<V>(self, v: V) -> Insert<Ta, <Co as PushSql<V>>::Out, Ex, Wi, Up>
    where
        V: BuildSql,
        Co: PushSql<V>,
        <Co as PushSql<V>>::Out: BuildSql,
    {
        Insert {
            with: self.with,
            table: self.table,
            columns: self.columns.push_sql(v),
            expr: self.expr,
            upsert: self.upsert,
        }
    }
    pub fn expr<V>(self, v: V) -> Insert<Ta, Co, <Ex as PushSql<V>>::Out, Wi, Up>
    where
        V: BuildSql,
        Ex: PushSql<V>,
        <Ex as PushSql<V>>::Out: BuildSql,
    {
        Insert {
            with: self.with,
            table: self.table,
            columns: self.columns,
            expr: self.expr.push_sql(v),
            upsert: self.upsert,
        }
    }

    pub fn with<V>(self, v: V) -> Insert<Ta, Co, Ex, <Wi as PushSql<V>>::Out, Up>
    where
        V: BuildSql,
        Wi: PushSql<V>,
        <Wi as PushSql<V>>::Out: BuildSql,
    {
        Insert {
            with: self.with.push_sql(v),
            table: self.table,
            columns: self.columns,
            expr: self.expr,
            upsert: self.upsert,
        }
    }

    pub fn upsert<V>(self, v: V) -> Insert<Ta, Co, Ex, Wi, V>
    where
        V: BuildSql + IsEmpty,
    {
        Insert {
            with: self.with,
            table: self.table,
            columns: self.columns,
            expr: self.expr,
            upsert: v,
        }
    }

    pub fn add_columns<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Co: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.columns, v);
        self
    }

    pub fn add_expr<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Ex: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.expr, v);
        self
    }

    pub fn add_with<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Wi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.with, v);
        self
    }
}

#[derive(Debug)]
pub struct Delete<Ta, Wi, Fi> {
    table: Ta,
    with: Wi,
    filter: Fi,
}

impl<Ta, Wi, Fi> BuildSql for Delete<Ta, Wi, Fi>
where
    Ta: BuildSql,
    Wi: BuildSql + IsEmpty,
    Fi: DelimitSql + IsEmpty,
{
    fn build_sql(&self, s: &mut String) {
        if !self.with.is_empty() {
            s.push_str("WITH ");
            self.with.build_sql(s);
            s.push_str(" ");
        }

        s.push_str("DELETE FROM ");
        self.table.build_sql(s);

        if !self.filter.is_empty() {
            s.push_str(" WHERE ");
            self.filter.delimit_sql(" AND ", s);
        }
    }
}

impl<Ta, Wi, Fi> Default for Delete<Ta, Wi, Fi>
where
    Ta: Default,
    Wi: Default,
    Fi: Default,
{
    fn default() -> Self {
        Delete {
            table: Default::default(),
            with: Default::default(),
            filter: Default::default(),
        }
    }
}

impl Delete<ExprTuple<()>, ExprTuple<()>, ExprTuple<()>> {
    pub fn default_tuples() -> Self {
        Self::default()
    }
}

impl<Ta, Wi, Fi> Delete<Ta, Wi, Fi> {
    pub fn table<V>(self, v: V) -> Delete<V, Wi, Fi>
    where
        V: BuildSql,
    {
        Delete {
            with: self.with,
            table: v,
            filter: self.filter,
        }
    }

    pub fn with<V>(self, v: V) -> Delete<Ta, <Wi as PushSql<V>>::Out, Fi>
    where
        V: BuildSql,
        Wi: PushSql<V>,
        <Wi as PushSql<V>>::Out: BuildSql,
    {
        Delete {
            with: self.with.push_sql(v),
            table: self.table,
            filter: self.filter,
        }
    }

    pub fn filter<V>(self, v: V) -> Delete<Ta, Wi, <Fi as PushSql<V>>::Out>
    where
        V: BuildSql,
        Fi: PushSql<V>,
        <Fi as PushSql<V>>::Out: BuildSql + IsEmpty,
    {
        Delete {
            with: self.with,
            table: self.table,
            filter: self.filter.push_sql(v),
        }
    }

    pub fn add_with<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Wi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.with, v);
        self
    }

    pub fn add_filter<V>(&mut self, v: V) -> &mut Self
    where
        V: BuildSql,
        for<'a> &'a mut Fi: PushSql<V, Out = ()>,
    {
        PushSql::push_sql(&mut self.filter, v);
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const PARENTS: [(i64, &'static str); 3] = [(1, "Alice A"), (2, "Bob B"), (3, "Charlie C")];

    const CHILDREN: [(i64, i64, &'static str); 4] = [
        (1, 1, "Garfield A"),
        (2, 1, "Barry A"),
        (3, 1, "Vi A"),
        (4, 3, "Karl C"),
    ];

    #[test]
    fn test_sub_query() {
        // for each parent, get at most two children ordered by (parent, name)
        let bind = || bind::<()>();

        let sql = {
            let parents_values = PARENTS
                .iter()
                .map(|_| ExprTuple::from((bind(), bind())))
                .collect::<Values<_>>();
            let with_parents = WithAs {
                stmt: &parents_values as &dyn BuildSql,
                name: ident("parents"),
                columns: vec![ident("id"), ident("name")],
            };
            let parents = with_parents.name.clone().alias(ident("p"));

            let children_values = CHILDREN
                .iter()
                .map(|_| ExprTuple::from((bind(), bind(), bind())))
                .collect::<Values<_>>();
            let with_children = WithAs {
                stmt: &children_values as &dyn BuildSql,
                name: ident("children"),
                columns: vec![ident("id"), ident("parent"), ident("name")],
            };
            let children = with_children.name.clone().alias(ident("c"));

            select()
                .column(children.c(ident("parent")))
                .column(children.c(ident("name")))
                .with(with_parents)
                .with(with_children)
                .from(parents.clone())
                .join(Join {
                    op: JoinOperator::Inner,
                    target: children.clone(),
                    on: {
                        // not 100% sure this is correct because we reuse the aliases
                        let join_cond = || {
                            parents
                                .c(ident("id"))
                                .expr()
                                .eq(children.c(ident("parent")))
                        };
                        // uhhh...
                        let limit = children.c(ident("id")).expr().is_in({
                            let sel = Select::default_tuples()
                                .column(children.c(ident("id")))
                                .from(children.clone())
                                .filter(join_cond())
                                .order_by((children.c(ident("name")), Order::Asc))
                                .limit(2);
                            Expr::Sub(Box::new(sel))
                        });
                        join_cond().and(limit)
                    },
                })
                .order_by((children.c(ident("parent")), Order::Asc))
                .order_by((children.c(ident("name")), Order::Asc))
                .as_sql()
        };

        let expected = r#"WITH "parents" ("id", "name") AS (VALUES (?, ?), (?, ?), (?, ?)), "children" ("id", "parent", "name") AS (VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?), (?, ?, ?)) SELECT "c"."parent", "c"."name" FROM "parents" AS "p" INNER JOIN "children" AS "c" ON ("p"."id" = "c"."parent" AND "c"."id" IN (SELECT "c"."id" FROM "children" AS "c" WHERE "p"."id" = "c"."parent" ORDER BY "c"."name" ASC LIMIT 2)) ORDER BY "c"."parent" ASC, "c"."name" ASC"#;
        assert_eq!(sql, expected, "got:\n{}\nexpected:\n{}", sql, expected);

        let params = PARENTS
            .iter()
            .flat_map(|(id, name)| {
                vec![
                    id as &dyn rusqlite::types::ToSql,
                    name as &dyn rusqlite::types::ToSql,
                ]
                .into_iter()
            })
            .chain(CHILDREN.iter().flat_map(|(id, parent, name)| {
                vec![
                    id as &dyn rusqlite::types::ToSql,
                    parent as &dyn rusqlite::types::ToSql,
                    name as &dyn rusqlite::types::ToSql,
                ]
                .into_iter()
            }))
            .collect::<Vec<_>>();

        let conn = rusqlite::Connection::open_in_memory().unwrap();
        let mut stmt = conn.prepare(&sql).expect("prepare");
        let rows = stmt
            .query_map(params, |row| {
                let parent: i64 = row.get(0)?;
                let name: String = row.get(1)?;
                Ok((parent, name))
            })
            .expect("query")
            .map(|row| row.expect("row"))
            .collect::<Vec<_>>();
        assert_eq!(
            rows,
            vec![
                (1, "Barry A".into()),
                (1, "Garfield A".into()),
                (3, "Karl C".into()),
            ]
        );
    }

    #[test]
    fn test_select_filter() {
        let articles = ident("articles").alias(ident(0));
        let filter = Expr::Binary(
            BinOperator::Eq,
            vec![Expr::Column(articles.c(ident("slug"))), Expr::Bind],
        );
        let meme = select()
            .column(lit::<()>("count(*)"))
            .from(articles)
            .filter(filter)
            .as_sql();
        assert_eq!(
            meme,
            r#"SELECT count(*) FROM "articles" AS _a0 WHERE _a0."slug" = ?"#
        );
    }

    #[test]
    fn test_expr_order_of_evaluation() {
        let sql = lit::<()>("loves_cats")
            .or(lit("loves_cat_pictures").and(lit("loves_memes").eq(lit("'t'"))))
            .as_sql();
        assert_eq!(
            sql,
            "(loves_cats OR (loves_cat_pictures AND loves_memes = 't'))"
        )
    }

    #[test]
    fn test_with() {
        let cool = WithAs {
            stmt: select().column(lit::<()>("1")),
            name: ident("cool"),
            columns: (),
        };

        let stmt: Box<dyn BuildSql> = Box::new(Values::new(vec![lit::<()>("1"), lit::<_>("2")]));
        let beans = WithAs {
            stmt,
            name: ident("beans"),
            columns: (),
        };

        let meme = select()
            .column(cool.c(lit::<()>("*")))
            .column(beans.c(lit::<()>("*")))
            .from(cool.name.clone())
            .from(beans.name.clone())
            .with(&cool)
            .with(&beans)
            .as_sql();
        assert_eq!(
            meme,
            r#"WITH "cool" AS (SELECT 1), "beans" AS (VALUES (1), (2)) SELECT "cool".*, "beans".* FROM "cool", "beans""#
        );
    }

    #[test]
    fn test_ugly_from_with() {
        let with_values = WithAs {
            stmt: Values::new(vec![lit::<()>("0"), lit("1"), lit("2")]),
            name: ident("values"),
            columns: vec![ident("n")],
        };
        let values = with_values.name.clone().alias(ident("v"));
        let sql = select()
            .column(values.c(ident("n")))
            .from(values)
            .with(with_values)
            .as_sql();
        assert_eq!(
            r#"WITH "values" ("n") AS (VALUES (0), (1), (2)) SELECT "v"."n" FROM "values" AS "v""#,
            sql
        );
    }

    #[test]
    fn test_values() {
        assert_eq!(Values::new(vec![bind::<()>()]).as_sql(), "VALUES (?)");

        let meme = Values::<ExprTuple<_>>::new(vec![
            (bind::<()>(), lit::<()>("now()")).into(),
            (bind(), lit("now() + 1d")).into(),
            (bind(), lit("now() + 3d")).into(),
        ])
        .as_sql();
        assert_eq!(meme, "VALUES (?, now()), (?, now() + 1d), (?, now() + 3d)");

        let values = Values::<Vec<Expr<()>>>::new(vec![]);
        assert_eq!(values.as_sql(), "DEFAULT VALUES");

        let values = Values::<Vec<Vec<Expr<()>>>>::new(vec![vec![]]);
        assert_eq!(values.as_sql(), "DEFAULT VALUES");

        let values = Values::<Vec<_>>::new(vec![vec![lit::<()>("0")]]);
        assert_eq!(values.as_sql(), "VALUES (0)");
    }

    #[test]
    fn test_select2() {
        let sel = {
            let foo = ident("foo");
            let bar = SelectColumn(foo.clone(), ident("bar"));

            Select::default_tuples().column(bar).from(foo)
        };
        let sql = sel.as_sql();
        assert_eq!(sql, r#"SELECT "foo"."bar" FROM "foo""#);
    }

    #[test]
    fn test_select2_borrow() {
        let foo = ident("foo");
        let bar = SelectColumn(&foo, ident("bar"));

        let sel = Select::default_tuples().column(&bar).from(&foo);
        let sql = sel.as_sql();
        assert_eq!(sql, r#"SELECT "foo"."bar" FROM "foo""#);
    }

    #[test]
    fn test_select2_wtf() {
        let sel: Select<(), (), (), (), (), ()> = Default::default();
        let sel = sel.column(SimpleExpr::Literal("now()"));
        let from = vec![ident("wacky-tobacky")];
        let sel = sel.from(from);
        assert_eq!(sel.as_sql(), r#"SELECT now() FROM "wacky-tobacky""#);
    }

    #[test]
    fn test_select2_push_vec() {
        let mut sel: Select<Vec<SimpleExpr>, (), (), (), (), Vec<SimpleExpr>> = Default::default();
        sel.add_column(SimpleExpr::Literal("1"));
        sel.add_column(SimpleExpr::Literal("2"));
        assert_eq!(sel.as_sql(), r#"SELECT 1, 2"#);

        sel.add_order_by(SimpleExpr::Literal("1"));
        sel.add_order_by(SimpleExpr::Literal("2"));
        assert_eq!(sel.as_sql(), r#"SELECT 1, 2 ORDER BY 1, 2"#);
    }

    #[test]
    fn test_select2_accum_filter() {
        let mut sel: Select<Vec<SimpleExpr>, (), (), Vec<SimpleExpr>, (), ()> = Default::default();
        sel.add_column(SimpleExpr::Literal("1"));
        sel.add_column(SimpleExpr::Literal("2"));
        assert_eq!(sel.as_sql(), r#"SELECT 1, 2"#);

        sel.add_filter(SimpleExpr::Literal("1 = 1"));
        sel.add_filter(SimpleExpr::Literal("true"));
        assert_eq!(sel.as_sql(), r#"SELECT 1, 2 WHERE 1 = 1 AND true"#);
    }

    #[test]
    fn test_select2_features() {
        let parents = {
            WithAs {
                stmt: PARENTS
                    .iter()
                    .map(|_| ExprTuple((bind::<()>(), bind::<()>())))
                    .collect::<Values<_>>(),
                name: ident("parents"),
                columns: ExprTuple((ident("id"), ident("name"))),
            }
        };

        let children = {
            WithAs {
                stmt: CHILDREN
                    .iter()
                    .map(|_| ExprTuple((bind::<()>(), bind::<()>(), bind::<()>())))
                    .collect::<Values<_>>(),
                name: ident("children"),
                columns: ExprTuple((ident("id"), ident("parent"), ident("name"))),
            }
        };

        let sql = Select::default_tuples()
            .with(&parents)
            .with(&children)
            .from(&parents.name)
            .join(Join {
                op: JoinOperator::Inner,
                target: &children.name,
                on: {
                    let join_cond = || {
                        BinaryExpression(
                            parents.c(ident("id")),
                            BinOperator::Eq,
                            children.c(ident("parent")),
                        )
                    };

                    BinaryExpression(
                        join_cond(),
                        BinOperator::And,
                        InExpression(
                            children.c(ident("id")),
                            Select::default_tuples()
                                .column(children.c(ident("id")))
                                .from(&children.name)
                                .filter(join_cond())
                                .order_by(children.c(ident("name")).expr().asc())
                                .limit(2),
                        ),
                    )
                },
            })
            .column(parents.c(ident("name")))
            .column(children.c(ident("name")))
            .order_by(children.c(ident("parent")).expr().asc())
            .order_by(children.c(ident("name")).expr().asc())
            .as_sql();

        // let expected = r#"WITH "parents" ("id", "name") AS (VALUES (?, ?), (?, ?), (?, ?)), "children" ("id", "parent", "name") AS (VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?), (?, ?, ?)) SELECT "c"."parent", "c"."name" FROM "parents" AS "p" INNER JOIN "children" AS "c" ON "p"."id" = "c"."parent" AND "c"."id" IN (SELECT "c"."id" FROM "children" AS "c" WHERE "p"."id" = "c"."parent" ORDER BY "c"."name" ASC LIMIT 2) ORDER BY "c"."parent" ASC, "c"."name" ASC"#;
        eprintln!("{}", sql);
        // eprintln!("{}", expected);
        // assert_eq!(sql, expected);

        let params = PARENTS
            .iter()
            .flat_map(|(id, name)| {
                vec![
                    id as &dyn rusqlite::types::ToSql,
                    name as &dyn rusqlite::types::ToSql,
                ]
                .into_iter()
            })
            .chain(CHILDREN.iter().flat_map(|(id, parent, name)| {
                vec![
                    id as &dyn rusqlite::types::ToSql,
                    parent as &dyn rusqlite::types::ToSql,
                    name as &dyn rusqlite::types::ToSql,
                ]
                .into_iter()
            }))
            .collect::<Vec<_>>();

        let conn = rusqlite::Connection::open_in_memory().unwrap();
        let mut stmt = conn.prepare(&sql).expect("prepare");
        let rows = stmt
            .query_map(params, |row| {
                let parent: String = row.get(0)?;
                let name: String = row.get(1)?;
                Ok((parent, name))
            })
            .expect("query")
            .map(|row| row.expect("row"))
            .collect::<Vec<_>>();
        assert_eq!(
            rows,
            vec![
                ("Alice A".into(), "Barry A".into()),
                ("Alice A".into(), "Garfield A".into()),
                ("Charlie C".into(), "Karl C".into()),
            ]
        );
    }
}
