//! time posting cli interface stuff

use std::{
    collections::HashMap,
    io::{stdout, Write},
};

use futures::FutureExt;

use crate::{cli, client, domain, Impetuous};
use impress::orm::Patch;

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error("failed to post time: {0}")]
    Ledger(#[from] anyhow::Error),
    #[error("failed updating database: {0}")]
    Client(#[from] client::Error),
}

#[derive(Debug, structopt::StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub struct Post {
    #[structopt(flatten)]
    range: cli::SinceUntil,
    // /// Preview & prompt before posting time
    // #[structopt(short, long)]
    // confirm: bool,
}

impl cli::Command for Post {
    fn run(self, gen: &cli::General, im: &Impetuous) -> anyhow::Result<()> {
        let mut client = im.client(gen.url.clone())?;
        let integrations = &im.config().integrations;
        let secrets = &im.secrets();

        let imprint = im.print();
        let since = self.range.since.datetime_on(imprint.now.date())?;
        let until = self.range.until.datetime_on(imprint.now.date().succ())?;

        let mut postings: Box<Vec<domain::Posting>> = client.find(
            "postings {
                uuid, entity, source, status, detail,
                doing sort(-start) ? and(start.lt.$until, or(end.is.null, end.ge.$since))
            } ? or(status.eq.$unposted, status.eq.$failed)",
            serde_json::json!({
                "since": since,
                "until": until,
                "unposted": domain::PostingStatus::New,
                "failed": domain::PostingStatus::Failed,
            }),
        )?;

        debug!(im.log, "postings:\n{:#?}", &postings);

        let mut results = HashMap::<uuid::Uuid, Result<String, Error>>::new();

        let ledger_results = postings
            .iter()
            .map(|posting| im.open_ledger(posting))
            .collect::<Vec<_>>();

        let mut ledgers: Vec<_> = postings
            .iter_mut()
            .zip(ledger_results.into_iter())
            .map(|(mut posting, result)| {
                // After preparing ledgers to post to, set the posting to pending...
                let result = result.and_then(|ledger| {
                    let patch = domain::Posting::pending(format!(
                        "Posting time to {}",
                        ledger.address_erased()
                    ));
                    client.update(&*posting, &patch)?;
                    patch.patch(&mut posting);
                    Ok(ledger)
                });
                (posting, result)
            })
            .filter_map(|(posting, result)| match result {
                // Ledger are paired with their &mut Posting into a vector
                Ok(ledger) => Some((posting, ledger)),
                // Failures are put in the results map
                Err(e) => {
                    results.insert(posting.uuid.unwrap(), Err(e.into()));
                    None
                }
            })
            .collect();

        let futures = ledgers
            .iter_mut()
            .map(|(posting, ledger)| {
                let fut = ledger.submit_erased((*posting).clone());
                let posting: &mut domain::Posting = *posting;
                Box::pin(fut.map(move |r| (posting, r)))
            })
            .collect::<Vec<_>>();

        im.block_on(async {
            let mut progress = cli::fmt::Progress::new(futures.len());
            let mut complete: Vec<&mut domain::Posting> = vec![];
            let mut remaining = futures;

            while !remaining.is_empty() {
                progress.set_remaining(remaining.len());

                let imprint = im.print();
                let wow = complete
                    .iter()
                    .map(|posting| format!(" {}", imprint.posting(&*posting)))
                    .collect::<String>();
                print!("\r{} ... {}", progress.display(&imprint), wow);
                let _ = stdout().flush();

                let ((posting, ledger_res), _, rest): (
                    (&mut domain::Posting, anyhow::Result<_>),
                    _,
                    _,
                ) = futures::future::select_all(remaining).await;

                let uuid = posting.uuid.expect("posting.uuid");

                // Update the database
                let patch = match &ledger_res {
                    Ok(detail) => domain::Posting::success(detail),
                    Err(e) => domain::Posting::failed(e.to_string()),
                };
                // If our patch fails, log it. Results is too confusing otherwise.
                if let Err(e) = client.update(&*posting, &patch) {
                    error!(
                        im.log,
                        "Failed to update the database with the result of posting {}.", uuid,
                    );
                    error!(im.log, "Impetuous might be out of sync now. :(\n");
                }
                patch.patch(posting);

                results.insert(uuid, ledger_res.map_err(Error::Ledger));

                complete.push(posting);

                remaining = rest;
            }

            let imprint = im.print();
            println!(
                "\r{}\r{}",
                im.print().spaces_for_stdout(),
                imprint.info("We did it!")
            );
        });

        debug!(im.log, "results:\n{:#?}", &results);

        // Show a fina list of posting resutls ...

        for posting in postings.into_iter() {
            let uuid = posting.uuid.as_ref().expect("posting.uuid");
            let entity = posting.entity.as_ref().expect("posting.entity");
            let doing = posting.doing.as_ref().expect("doing");
            let start = doing.start.expect("doing.start");

            let res = results.get(uuid).expect("posting result");
            let (mark, more) = match res {
                Ok(_) => (imprint.good("+"), "".to_owned()),
                Err(e) => (
                    imprint.bad("!"),
                    format!(" {}", e.to_string().lines().next().unwrap_or_default()),
                ),
            };

            let room = (imprint.stdout_width() as usize)
                .checked_sub(1 + 1 + entity.len() + 3 + 9)
                .unwrap_or(0);

            println!(
                "{} {} @ {}{}",
                mark,
                entity,
                imprint.time(start),
                &more[..std::cmp::min(more.len(), room)]
            );
        }

        return Ok(());
    }
}
