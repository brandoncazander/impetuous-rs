//! When handling a query, we encounter parameters in the filter expression.
//!
//! To use the parameters, we need to get their values by inferring their type and
//! deserializing them into some place. Later, we use those parameters in our Q object
//! to run the query.
//!
//! A vector of LateBindings is used to own deserialized parameters. Each time a
//! parameter is first encountered, we store a late binding for the appropriate type in
//! that vector. And we tell our imql::serde::Expectation where to find it.
//!
//! It's possible to use parameters multiple times, while they can only be deserialized
//! once. Since our Q objects aren't using named parameters, we will pass in duplicates.
//! We use reference counting to allow the late bindings to occur multiple in our
//! parameter vector without copying. And a RefCell so that we don't have to give
//! imql::serde::Expectation a mutable borrow to each late binding while we are
//! populating the vector.
//!
//! It's a nasty fucking mess.
use std::any::Any;
use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;

use crate::orm;

use super::{Error, Result};

use rusqlite::types::{ToSql, ToSqlOutput};

pub trait AsAny: Any {
    fn as_any<'l>(&'l self) -> &'l dyn Any;
}

impl<T: Any> AsAny for T {
    fn as_any<'l>(&'l self) -> &'l dyn Any {
        self
    }
}

pub trait Bindable: imql::serde::Expected + orm::ToSqlDebug + AsAny {}

impl<T> Bindable for T where T: imql::serde::Expected + orm::ToSqlDebug + AsAny {}

#[derive(Default)]
pub struct LateBindings {
    map: imql::serde::Expectation<String, Binding>,
}

impl LateBindings {
    pub fn bind<T>(&mut self, name: String) -> Result<Binding>
    where
        T: orm::ToSqlDebug + serde::de::DeserializeOwned + Any + 'static,
    {
        if let Some(dynd) = self.map.get(&name) {
            let borrow = (***dynd).borrow();
            if !(*borrow).as_any().is::<Option<T>>() {
                let reason = format!("The type of parameter ${} is ambigious.", name);
                return Err(Error::InvalidBinding { reason });
            }
            Ok(Binding(Rc::clone(dynd)))
        } else {
            let rc: Rc<RefCell<dyn Bindable>> = Rc::new(RefCell::new(Option::<T>::None));
            self.map.insert(name, Binding(Rc::clone(&rc)));
            Ok(Binding(rc))
        }
    }

    fn keys_string(map: &imql::serde::Expectation<String, Binding>) -> String {
        let mut keys = String::new();
        for key in map.keys() {
            if !keys.is_empty() {
                keys.push_str(", ");
            }
            keys.push_str(key);
        }
        keys
    }

    /// This consumes self because we're not in a valid state to be reused after ...
    ///
    /// ... although we could be if we just cleared our map ...
    pub fn resolve<'de, D: serde::Deserializer<'de>>(self, de: D) -> Result<()> {
        let Self { mut map } = self;

        if map.is_empty() {
            return Ok(());
        }

        if let Err(err) = map.fulfill(de) {
            let reason = format!("failed to read parameter: {}", err.to_string());
            return Err(Error::InvalidBinding { reason });
        }

        if !map.is_empty() {
            let keys = Self::keys_string(&map);
            let reason = format!("These parameters were expected but not found: {}", keys);
            return Err(Error::InvalidBinding { reason });
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct Binding(Rc<RefCell<dyn Bindable>>);

impl Deref for Binding {
    type Target = Rc<RefCell<dyn Bindable>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl imql::serde::Expected for Binding {
    /// this panics if anything has borrowed a mutable reference to this binding
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error> {
        self.0.borrow_mut().deserialize_erased(de)
    }
}

impl ToSql for Binding {
    fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput> {
        let borrow = self.0.borrow();
        Ok(match borrow.to_sql()? {
            ToSqlOutput::Borrowed(valref) => ToSqlOutput::Owned(valref.into()),
            ToSqlOutput::Owned(val) => ToSqlOutput::Owned(val),
            _ => unimplemented!("{:?}", borrow),
        })
    }
}

#[test]
fn test_late_binding() {
    let params = {
        let mut late = LateBindings::default();
        let param = late.bind::<String>("foo".to_owned()).unwrap();

        let mut de = serde_json::Deserializer::from_slice(br#"{ "foo": "value" }"#);
        late.resolve(&mut de).unwrap();
        vec![param]
    };

    let conn = rusqlite::Connection::open_in_memory().unwrap();
    let foo: String = conn
        .query_row("SELECT ?", params, |row| row.get(0))
        .unwrap();
    assert_eq!(foo, "value");
}

#[test]
fn test_type_checking() -> Result<(), Box<dyn std::error::Error>> {
    let mut late = LateBindings::default();
    assert!(late.bind::<String>("foo".into()).is_ok());
    assert!(late.bind::<i64>("foo".into()).is_err());
    assert!(late.bind::<String>("foo".into()).is_ok());
    assert!(late.bind::<Option<String>>("foo".into()).is_err());
    Ok(())
}
