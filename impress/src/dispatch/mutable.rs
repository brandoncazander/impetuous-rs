//! uhg
use crate::dispatch::{self, MapErr, Result};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Change<T> {
    pub old: Option<T>,
    pub new: Option<T>,
}

impl<T> Change<T> {
    pub fn create(new: T) -> Self {
        Change {
            old: None,
            new: Some(new),
        }
    }

    pub fn update(old: T, new: T) -> Self {
        Change {
            old: Some(old),
            new: Some(new),
        }
    }

    pub fn delete(old: T) -> Self {
        Change {
            old: Some(old),
            new: None,
        }
    }

    pub fn as_ref(&self) -> Change<&T> {
        Change {
            old: self.old.as_ref(),
            new: self.new.as_ref(),
        }
    }
}

pub trait Changable {
    fn create(&self) -> Change<&Self>
    where
        Self: Sized,
    {
        Change::create(&self)
    }

    fn update<'a>(&'a self, other: &'a Self) -> Change<&'a Self>
    where
        Self: Sized,
    {
        Change::update(&self, other)
    }

    fn delete(&self) -> Change<&Self>
    where
        Self: Sized,
    {
        Change::delete(&self)
    }
}

impl<T> Changable for T where T: crate::orm::Modeled {}

/// TODO bad name? should be a mutation interaction or something like that?
pub trait Mutable<'l> {
    type Change: for<'a> BorrowChange<'a>;

    fn mutate(
        self,
        changes: &[<Self::Change as BorrowChange<'_>>::Borrowed],
    ) -> Result<dispatch::Reply>
    where
        Self: MapErr + IntoParts<Interaction<'l>>;

    fn mutate_from_de<D>(self, changes: D) -> Result<dispatch::Reply>
    where
        Self: MapErr + IntoParts<Interaction<'l>> + Sized,
        for<'de> D: serde::Deserializer<'de>,
        for<'de> Self::Change: serde::Deserialize<'de>,
    {
        use serde::Deserialize;

        #[derive(Deserialize)]
        #[serde(untagged)]
        enum OneOrVec<T> {
            One(T),
            Vec(Vec<T>),
        }

        match OneOrVec::<Self::Change>::deserialize(changes) {
            Ok(changes) => match changes {
                OneOrVec::One(v) => self.mutate(&[v.as_borrowed()]),
                OneOrVec::Vec(v) => {
                    let refs = v.iter().map(|ch| ch.as_borrowed()).collect::<Vec<_>>();
                    self.mutate(refs.as_slice())
                }
            },
            Err(e) => {
                let why = format!("failed to parse request data: {}", e);
                Err(super::Error::BadRequest { why, index: None })
            }
        }
    }
}

use super::{Interaction, IntoParts};

pub trait Also<'l, I, M>
where
    I: IntoParts<Interaction<'l>>,
{
    /// TODO change the signature so this can return a list of changes?
    fn after_change(
        &mut self,
        _: &<I as IntoParts<Interaction<'l>>>::Lh,
        _: &rusqlite::Transaction,
        _pk: i64,
        _: Change<&M>,
    ) -> Result<()> {
        Ok(())
    }
}

pub trait BorrowChange<'a> {
    type Borrowed: serde::Serialize;

    fn as_borrowed(&'a self) -> Self::Borrowed;
}

#[macro_export]
macro_rules! mutable {
    (
        @on $domain:ident ...
        $(
        $model:ident as $name:tt {
            $( $field:ident, )*
            $( @link $link:ident, )*
            $( @init $init:ident, )*
            $( @also $also:tt, )*
        }
        )*
    ) => {

    // TODO pollution ...
    #[derive(Debug, serde::Serialize)]
    #[serde(tag="on")]
    pub enum TaggedChange<'a> {
        $(
            #[serde(rename = $name)]
            $model($crate::dispatch::Change<&'a $model>),
        )*
    }

    $(
    impl<'a> From<$crate::dispatch::Change<&'a $model>> for TaggedChange<'a> {
        fn from(o: $crate::dispatch::Change<&'a $model>) -> Self {
            TaggedChange::$model(o)
        }
    }
    )*

    // TODO pollution ...
    #[derive(Debug, serde::Deserialize)]
    #[serde(tag="on")]
    pub enum OwnedTaggedChange {
        $(
            // stringify!($name) doesn't work here; so macro uses literals ...
            #[serde(rename = $name)]
            $model($crate::dispatch::Change<$model>),
        )*
    }

    impl<'s> $crate::dispatch::mutable::BorrowChange<'s> for OwnedTaggedChange {
        type Borrowed = TaggedChange<'s>;

        fn as_borrowed(&'s self) -> Self::Borrowed {
            match self {
                $( OwnedTaggedChange::$model(ch) => TaggedChange::$model(ch.as_ref()), )*
            }
        }
    }

    impl<'l> $crate::dispatch::Mutable<'l> for $domain<'l>
    {
        type Change = OwnedTaggedChange;

        fn mutate(self, changes: &[TaggedChange<'_>])
            -> $crate::dispatch::Result<$crate::dispatch::Reply>
        where
            Self: $crate::dispatch::IntoParts<$crate::dispatch::Interaction<'l>>
                + $crate::dispatch::MapErr,
        {
            use std::{cmp, hash, fmt};

            use $crate::{
                dispatch::{self, Result, Error, MapErr, Change, IntoParts},
                orm::{self, Modeled, lookup_primary_keys, SurrogateToPrimary, HasSurrogateKey, HasPrimaryKey},
            };

            #[allow(unused_variables)]
            let (extra, orm) = self.into_parts();
            let log = &orm.log;

            // WARNING! This is basically a cache ...
            //
            // Although we're in a transaction, the values we've cached can be modified
            // by triggers that run as a side-effect of the things we do. In that way,
            // our cache might become invalid without us realizing.
            //
            // But this just maps surrogate to primary keys and that shouldn't change.
            let mut key_maps = Maps::default();

            for (index, change) in changes.iter().enumerate() {
                // Make sure mutations only attempt to change fields mentions in old
                // TODO this should be configurable by the requester
                match change {
                    $(
                    TaggedChange::$model(Change { old: Some(old), new: Some(new) }) => {
                        $(
                        if new.$field.is_some() && old.$field.is_none() {
                            return Err(Error::new_not_in_old(stringify!($field)).at_index(index));
                        }
                        )*

                        $(
                        if new.$link.as_ref().map(|l| HasSurrogateKey::get(&**l)).is_some()
                                && old.$link.as_ref().map(|l| HasSurrogateKey::get(&**l)).is_none() {
                            return Err(Error::new_not_in_old(stringify!($link)).at_index(index));
                        }
                        )*
                    },
                    )*
                    _ => (),
                }

                // Populate key_maps.
                //
                // That is how we map surrogate keys to primary keys for everything we
                // do here.
                //
                // This is important. Not doing this right will cause panics.
                //
                // If something is referenced in this request, it should by referenced
                // by surrogate key and exist as a key in key_maps.  Before performing
                // mutations, we try to load the primary keys for each surrogate from
                // the database. Sometimes we won't find one because it's to be inserted
                // in changes later when performing mutations.
                match change {
                    $(
                    TaggedChange::$model(Change { old, new }) => {
                        match (&old, &new) {
                            (Some(old), None) => {
                                // On delete ...
                                let key = HasSurrogateKey::get(*old)
                                    .ok_or_else(|| Error::missing_surrogate(*old))
                                    .map_err(|e| e.at_index(index))?;
                                key_maps.for_type_mut(&**old).map.insert(key.clone(), None);
                            }
                            (Some(old), Some(new)) => {
                                // On update ...
                                //
                                // Ensure that the surrogate exists on old, if it is
                                // also on new, it must match the old.
                                let key = match (HasSurrogateKey::get(*old), HasSurrogateKey::get(*new)) {
                                    (Some(old_key), None) => old_key,
                                    (Some(old_key), Some(new_key)) if old_key == new_key => old_key,
                                    _ => {
                                        let why = format!(
                                            "the key `{}` must exist on `old` and match that on `new` if specified there too",
                                            <$model as HasSurrogateKey>::column().name,
                                        );
                                        return Err(Error::BadRequest { why, index: Some(index) })
                                    }
                                };

                                key_maps.for_type_mut(&**old).map.insert(key.clone(), None);
                            }
                            _ => (),
                        };

                        // Any linky fields that point to other records by a surrogate
                        // key are pushed into a map that we use to later look up the
                        // primary keys.
                        if let Some(_old) = old {
                            $(
                            if let Some(link) = _old.$link.as_ref() {
                                key_maps.for_type_mut(&**link).add_surrogate(&**link);
                            }
                            )*
                        }

                        if let Some(_new) = new {
                            $(
                            if let Some(link) = _new.$link.as_ref() {
                                key_maps.for_type_mut(&**link).add_surrogate(&**link);
                            }
                            )*
                        }
                    }
                    )*
                };
            }

            let tx = orm.db.transaction()?;

            $(
            if !key_maps.$model.map.is_empty() {
                // For each model that is referrenced in a link, we try to look up its
                // primary key by the surrogate key. Map entries will still be None if
                // they don't exist or haven't been inserted yet.
                //
                // TODO XXX FIXME the lookup here is unconstrained by any policy, so
                // users can reference objects that they shouldn't have access to ...
                lookup_primary_keys(
                    &mut key_maps.$model.map,
                    $model::table_name(),
                    &<$model as HasSurrogateKey>::column(),
                    &<$model as HasPrimaryKey>::column(),
                    &tx,
                    &log,
                )?;
            }
            )*

            debug!(log, "key mapping: {:#?}", key_maps);

            /////////////////////////////////////
            // Actually try to execute on changes
            //
            // We have only prefetched the primary keys from surrogate keys. Each model
            // we try to change below does its own loading. Prefetching the records is a
            // bit of a tricky optimization and could possibly mess up if there are
            // side-effects from modifying records through triggers or something that
            // affects other rows we don't know about?
            for (_index, change) in changes.iter().enumerate() {
                match change {
                    $(
                    TaggedChange::$model(Change { old: None, new: None }) => (),
                    TaggedChange::$model(Change { old: None, new: Some(new) }) => {
                        // Insert ...
                        //
                        // We basically just go through each field in new and copy it to
                        // a new instance. The relationships are mapped to use primary
                        // keys instead of surrogates.
                        let inst = new.materialize_from_new(&key_maps)
                            .map_err(|e| dispatch::Error::from(e).at_index(_index))?;

                        let pk = inst
                            .insert(&tx)
                            .map_err(|e| match e {
                                orm::Error::Db { source, .. } => Self::map_err(source),
                                _ => dispatch::Error::from(e),
                            })
                            .map_err(|e| e.at_index(_index))?;

                        $({
                            use dispatch::mutable::Also;
                            $also.after_change(&extra, &tx, pk, Change { old: None, new: Some(&inst) })?;
                        })*

                        // If this was inserted with a surrogate key, and the surrogate
                        // exists in our surrogate to primary mapping, then a later
                        // record references this surrogate and wants to know the
                        // primary key we just inserted.
                        if let Some(surrogate) = HasSurrogateKey::get(&inst) {
                            let s2p = &mut key_maps.for_type_mut(&inst).map;
                            if let Some(mapped_pk) = s2p.get_mut(surrogate) {
                                debug!(log, "Inserted {} for surrogate {}", pk, surrogate);
                                mapped_pk.replace(pk);
                            }
                        }
                    }
                    TaggedChange::$model(Change { old: Some(old), new: Some(new) }) => {
                        // Update ...
                        //
                        // Earlier, we ensure the new and old reference the same object
                        // by its surrogate. And that all the fields in new exist in
                        // old.
                        //
                        // So, for each field in old, check that the value matches what
                        // is in the database.
                        let pk = old.ensure_matches_db_state(&tx, &key_maps)
                              .map_err(|e| Error::from(e).at_index(_index))?;

                        let inst = new.materialize_from_new(&key_maps)
                            .map_err(|e| dispatch::Error::from(e).at_index(_index))?;

                        <$model as Modeled>::update(&inst, &pk, &tx)
                            .map_err(|e| match e {
                                orm::Error::Db { source, .. } => Self::map_err(source),
                                _ => dispatch::Error::from(e),
                            })
                            .map_err(|e| e.at_index(_index))?;

                        $({
                            use dispatch::mutable::Also;
                            $also.after_change(&extra, &tx, pk, Change { old: Some(*old), new: Some(&inst) })?;
                        })*
                    }
                    TaggedChange::$model(Change { old: Some(old), new: None }) => {
                        // Delete ...
                        let pk = old.ensure_matches_db_state(&tx, &key_maps)
                              .map_err(|e| Error::from(e).at_index(_index))?;

                        <$model as Modeled>::delete(&pk, &tx)
                            .map_err(|e| match e {
                                orm::Error::Db { source, .. } => Self::map_err(source),
                                _ => dispatch::Error::from(e),
                            })
                            .map_err(|e| e.at_index(_index))?;

                        $({
                            use dispatch::mutable::Also;
                            $also.after_change(&extra, &tx, pk, Change { old: Some(*old), new: None })?;
                        })*
                    }
                    )*
                }
            }

            tx.commit()?;

            let content = Box::new(()); // TODO wtf do we have this for?
            return Ok(dispatch::Reply { content });

            #[derive(Default, Debug)]
            #[allow(non_snake_case)]
            struct Maps {
                $(
                $model: SurrogateToPrimary<$model>,
                )*
            }

            /// Instead of using AsRef<T>, this takes an instance of T allowing us to
            /// disambiguate the trait if we don't know what T is...
            trait ForType<T> {
                type Out;

                fn for_type(&self, _: &T) -> &Self::Out;
                fn for_type_mut(&mut self, _: &T) -> &mut Self::Out;
            }

            $(
            impl ForType<$model> for Maps {
                type Out = SurrogateToPrimary<$model>;

                fn for_type(&self, _: &$model) -> &Self::Out {
                    &self.$model
                }

                fn for_type_mut(&mut self, _: &$model) -> &mut Self::Out {
                    &mut self.$model
                }
            }
            )*

            trait EnsureMatchesDatabaseState: HasPrimaryKey {
                fn ensure_matches_db_state(&self, _: &rusqlite::Transaction, _: &Maps) -> Result<<Self as HasPrimaryKey>::Type>;
            }

            $(
            impl EnsureMatchesDatabaseState for $model {
                fn ensure_matches_db_state(&self, tx: &rusqlite::Transaction, key_maps: &Maps) -> Result<<Self as HasPrimaryKey>::Type> {
                    let pk = lookup_pk(self, &key_maps.for_type(self))?
                        .ok_or_else(|| Error::missing_surrogate(self))?;

                    let mut q = $model::q();

                    // TODO this is leaky, we don't know the pk is "id"...
                    q.get.id();

                    // Load the scalars we need to compare.
                    $( if self.$field.is_some() { q.get.$field(); })*

                    // Load relationships we need to compare.
                    // TODO this is an unncessary join ...
                    $(
                    if let Some(link) = self.$link.as_ref().map(|l| &**l) {
                        if let Some(_) = lookup_pk(link, &key_maps.for_type(link))? {
                            // TODO absolutely cheating by putting "id" here...
                            <$model as Modeled>::LinkSet::$link(&mut q).get.id();
                        }
                    }
                    )*

                    let current = q
                        // TODO "id" again, somebody stop this madness ...
                        .filter(|m| m.id().eq(orm::BIND))
                        .bind(Box::new(pk))
                        .one(&tx)?
                        .expect("lookup update target by primary key");

                    $(
                    if current.$field != self.$field {
                        let current = Box::new(current);
                        return Err(Error::Conflict { current, index: None });
                    }
                    )*

                    $(
                    if let Some(self_link) = self.$link.as_ref() {
                        if let Some(self_pk) = lookup_pk(&**self_link, &key_maps.for_type(&**self_link))? {
                            let current_pk = current.$link
                                .as_ref()
                                .and_then(|l| HasPrimaryKey::get(&**l))
                                .expect("current loaded with primary key");
                            if *current_pk != self_pk {
                                let current = Box::new(current);
                                return Err(Error::Conflict { current, index: None });
                            }
                        }
                    }
                    )*

                    Ok(pk)
                }
            }
            )*

            trait MaterializeFromNew {
                fn materialize_from_new(&self, _: &Maps) -> Result<Self>
                    where Self: Sized;
            }

            $(
            impl MaterializeFromNew for $model {
                #[allow(unused_variables)]
                fn materialize_from_new(&self, key_maps: &Maps) -> Result<Self>
                    where Self: Sized
                {
                    // This is a bit dumb where we copy data to a new instance
                    // instead of using the one the user provided.
                    //
                    // This is pretty much in case we don't validate all the fields,
                    // we don't try to do stuff with fields on the model that we are
                    // ignoring or missed. And it requires a copy/move because twe
                    // whole orm/model + manifest interface thing is awful.
                    let mut inst = $model::default();

                    if let Some(surrogate) = HasSurrogateKey::get(self) {
                        HasSurrogateKey::set(&mut inst, surrogate.clone());
                    }

                    $(
                    if let Some(v) = self.$field.as_ref() {
                        inst.$field = Some(v.clone()).into();
                    }
                    )*

                    $(
                    if let Some(link) = self.$link.as_ref() {
                        // If there is no surrogate key on this linked record,
                        // ignore it, otherwise ... look up the primary key.
                        if let Some(pk) = lookup_pk(&**link, &key_maps.for_type(&**link))? {
                            let mut link: Box<_> = Default::default();
                            HasPrimaryKey::set(&mut *link, pk.clone());
                            inst.$link = Some(link).into();
                        }
                    }
                    )*

                    $(
                        $init(&mut inst);
                    )*

                    Ok(inst)
                }
            }
            )*

            /// Look up a primary key from the SurrogateToPrimary cache. Returns:
            /// - None if no surrogate key exists on `thing`.
            /// - panics if the surrogate key was found but was not a key in the mapping
            /// - NotFound the surrogate key is in the mapping but mapped to None
            ///   (probably means whatever is referenced doesn't exist).
            fn lookup_pk<T>(thing: &T, s2p: &SurrogateToPrimary<T>) -> $crate::dispatch::Result<Option<<T as HasPrimaryKey>::Type>>
                where
                    T: HasSurrogateKey + HasPrimaryKey + fmt::Debug,
                    <T as HasSurrogateKey>::Type: cmp::Eq + hash::Hash + fmt::Debug + fmt::Display,
                    <T as HasPrimaryKey>::Type: fmt::Debug,
            {
                let surrogate = match HasSurrogateKey::get(thing) {
                    None => return Ok(None),
                    Some(surrogate) => surrogate,
                };

                let maybe_pk = s2p.map.get(surrogate).expect("surrogate key in s2p map");

                match maybe_pk {
                    None => Err(dispatch::Error::NotFound {
                        reference: surrogate.to_string(),
                        index: None,
                    }),
                    Some(pk) => Ok(Some(pk.clone())),
                }
            }
        }

    }
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    use crate::{
        dispatch::tests::{articles_interaction, TestInteraction},
        orm::{
            tests::{articles_database, Article, Comment, Person},
            Modeled,
        },
    };

    mutable! {
        @on TestInteraction ...
        Article as "articles" {
            title,
            slug,
            published_at,
            @link author,
            // TODO allow returning a validation string error thingy type
            @init default_slug,
        }
        Person as "people" {
            name,
        }
        Comment as "comments" {
            content,
            published_at,
            @link author,
            @link article,
        }
    }

    fn default_slug(a: &mut Article) {
        match (a.title.as_ref(), a.slug.as_ref()) {
            (Some(_title), None) => {
                a.slug = Some("eh-whatever".to_owned()).into();
            }
            _ => (),
        }
    }

    #[test]
    fn test_mutations() -> Result<(), Box<dyn std::error::Error>> {
        let mut db = articles_database()?;

        let people = Person::q()
            .get(|p| p.name().uuid())
            .list(&mut db.transaction()?)?
            .into_iter()
            .map(|p| (p.name.into_option().unwrap(), p.uuid.into_option().unwrap()))
            .collect::<HashMap<_, _>>();

        let patrick = people.get("Patrick Star").unwrap();

        let data = serde_json::json!([
            {
                "on": "articles",
                "new": {
                    "title": "Banana Bread",
                    "uuid": "dd5ca238-4163-11ea-a612-8c1645d8a473",
                    "published_at": 1234,
                    "author": { "uuid": patrick }
                }
            },
            {
                "on": "comments",
                "new": {
                    "uuid": "bcd7daf2-41b1-11ea-8607-8c1645d8a473",
                    "article": { "uuid": "dd5ca238-4163-11ea-a612-8c1645d8a473" },
                    "content": "First!",
                    "published_at": 1235,
                    "author": { "uuid": patrick, }
                }
            },
        ]);

        articles_interaction(&mut db)?.mutate_from_de(data)?;

        let data = serde_json::json!([
            {
                "on": "articles",
                "old": {
                    "uuid": "dd5ca238-4163-11ea-a612-8c1645d8a473",
                    "content": "",
                },
                "new": {
                    "uuid": "dd5ca238-4163-11ea-a612-8c1645d8a473",
                    "content": "Hello world!",
                }
            },
        ]);

        articles_interaction(&mut db)?.mutate_from_de(data)?;

        let data = serde_json::json!([
            {
                "on": "comments",
                "old": {
                    "uuid": "bcd7daf2-41b1-11ea-8607-8c1645d8a473",
                    "content": "First!",
                },
                "new": {
                    "content": "This is a terrible comment.",
                }
            },
        ]);

        articles_interaction(&mut db)?.mutate_from_de(data)?;

        let data = serde_json::json!([
            {
                "on": "comments",
                "old": { "uuid": "bcd7daf2-41b1-11ea-8607-8c1645d8a473" },
            },
        ]);

        articles_interaction(&mut db)?.mutate_from_de(data)?;

        Ok(())
    }
}
