// #![feature(never_type)]
#![cfg_attr(backtrace, feature(backtrace))]

#[macro_use]
extern crate slog;

pub mod dispatch;
pub mod few_or_many;
// pub mod indirect;
pub mod maybe;
pub mod orm;
pub mod sql;

// pub use indirect::{Indirect, Indirection};
pub use maybe::{maybe, maybe_not, Maybe};

#[cfg(test)]
pub fn test_logger() -> slog::Logger {
    use slog::Drain;

    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::CompactFormat::new(decorator).build();
    let drain = std::sync::Mutex::new(drain).fuse();
    slog::Logger::root(drain, o!())
}
