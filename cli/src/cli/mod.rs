use std::io::{self, Write};
use std::fs::File;

use anyhow::{Context, Result};
use chrono::TimeZone;
use impartial::DateishTime;
use structopt::StructOpt;

use impress::{maybe::maybe, orm::Patch};

use crate::{client::Client, domain, logging, time::DateTime, Impetuous};
pub use fmt::{Imprint, Palette, Styles};

mod doing;
mod edit;
mod example_config;
mod fmt;
mod import_py;
mod note;
mod post;
mod show;
mod summary;

static AFTER_HELP: &'static str = r#"
`im doing` is where you should start.
`im edit` to change stuff using an editor set by the EDITOR environment
variable.

There's a bit more help/explanation about how to post time to
Jira/Fresdesk in the example configuration file. It might have been
written to ~/.config/impetuous/config.toml. Or run `im example-config`
to print an example to stdout.
"#;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid date or time")]
    InvalidDateish,
}

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
struct Args {
    #[structopt(flatten)]
    general: General,
    #[structopt(subcommand)]
    action: Actions,
}

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub(crate) struct General {
    // /// Increases output verbosity
    // #[structopt(short, parse(from_occurrences))]
    // pub verbosity: i8,
    /// Logging level.
    #[structopt(short, long,
        parse(try_from_str = logging::parse_level),
        default_value = "warn",
        possible_values = &["debug", "info", "warn"]
    )]
    pub log: logging::Level,
    /// Force colours on or off ...
    #[structopt(long)]
    pub color: Option<bool>,
    // /// Move dates for most inputs back a day (can be repeated)
    // #[structopt(short, long, parse(from_occurrences))]
    // pub yesterday: i8,
    /// URL of an `im serve` to query.
    /// Omit to load & query a local database like normal.
    #[structopt(long)]
    pub url: Option<hyper::Uri>,
}

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub(crate) struct SinceUntil {
    #[structopt(short, long, default_value = "today")]
    pub since: DateTimeInput,
    #[structopt(short, long, default_value = "tomorrow")]
    pub until: DateTimeInput,
}

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
enum Actions {
    Doing(doing::Doing),
    Show(show::Show),
    Summary(summary::Summary),
    Edit(edit::Edit),
    Note(note::Note),
    Post(post::Post),
    Serve(crate::server::Serve),
    ImportPy(import_py::ImportPy),
    ExampleConfig(example_config::ExampleConfig),
}

// todo this trait is super dumb and weird
pub(crate) trait Command {
    fn run(self, _: &General, _: &Impetuous) -> Result<()>;
}

pub(crate) fn main() -> Result<()> {
    let clap = Args::clap().after_help(AFTER_HELP);
    // This panics or quits or something if it doesn't like the arguments
    let args = Args::from_clap(&clap.get_matches());

    let log = logging::please();
    logging::set_level(args.general.log);

    let mut im = match Impetuous::from_system() {
        Ok(im) => im,
        Err(crate::im::Error::InvalidConfig { path, source }) => {
            eprintln!("Your configuration file couldn't be read.");
            eprintln!("Here's an example of what it should look like.");
            eprintln!("{}", crate::conf::EXAMPLE);
            Err(crate::im::Error::InvalidConfig { path, source })?
        }
        Err(e) => Err(e)?,
    };

    if let Some(e) = args.general.color {
        im.config_mut().colors = Some(e);
    }

    info!(log, "The time is {}.", im.now_local().to_rfc2822());

    info!(log, "Your config is {:?}", im.config());

    for (name, i) in im.config().integrations.iter() {
        if i.unknown().is_empty() {
            continue;
        }

        warn!(
            log,
            "The [{}] section in your configuration contains unrecognized mappings.", name
        );
        for (key, wat) in i.unknown().iter() {
            warn!(log, "{} = {}", key, wat);
        }
        warn!(log, "Go fix it at: {}", im.config_path.display());
    }

    // TODO maybe don't do this if the --url option is something
    im.run_migrations_if_any()?;

    match args.action {
        Actions::Doing(sub) => sub.run(&args.general, &im),
        Actions::Show(sub) => sub.run(&args.general, &im),
        Actions::Summary(sub) => sub.run(&args.general, &im),
        Actions::Edit(sub) => sub.run(&args.general, &im),
        Actions::Note(sub) => sub.run(&args.general, &im),
        Actions::Post(sub) => sub.run(&args.general, &im),
        Actions::Serve(sub) => sub.run(&args.general, &im),
        Actions::ImportPy(sub) => sub.run(&args.general, &im),
        Actions::ExampleConfig(sub) => sub.run(&args.general, &im),
    }?;

    Ok(())
}

pub(crate) fn open_in_editor<P: AsRef<std::path::Path>>(
    path: P,
) -> io::Result<std::process::ExitStatus> {
    use std::{env, process::Command};

    let log = crate::logging::please();

    match env::var("EDITOR") {
        Ok(editor) => return Command::new(&editor).args(&[path.as_ref()]).status(),
        Err(env::VarError::NotPresent) => warn!(
            log,
            "EDITOR environment variable is not set, looking for a fallback"
        ),
        Err(e) => warn!(
            log,
            "EDITOR environment variable is not stringy, looking for a fallback"
        ),
    };

    let editors = ["nvim", "vim", "vi", "emacs", "nano", "ed", "oowriter"]
        .iter()
        .cloned();
    for editor in editors {
        match Command::new(editor).args(&[path.as_ref()]).status() {
            Ok(v) => return Ok(v),
            Err(e) if e.kind() == io::ErrorKind::NotFound => warn!(log, "{} not found", editor),
            Err(e) => return Err(e),
        }
    }
    return Err(io::Error::from(io::ErrorKind::NotFound));
}

pub(crate) fn edit_yaml<T, P>(_path: P) -> anyhow::Result<T>
where
    T: serde::de::DeserializeOwned,
    P: AsRef<std::path::Path>,
{
    let path = _path.as_ref();
    match open_in_editor(path) {
        Ok(status) if !status.success() => Err(anyhow::anyhow!("editor did not exit successfully")),
        Ok(status) => Ok(()),
        Err(e) if e.kind() == io::ErrorKind::NotFound => Err(e).context("no editor found {}"),
        Err(e) => Err(e).context(format!("could not edit {}", path.display())),
    }?;

    let reader = File::open(path).context("open temp file")?;
    return Ok(serde_yaml::from_reader(reader)?);
}

#[derive(Default, Debug)]
pub struct InteractiveEditBuilder {
    header: String,
    footer: String,
}

impl InteractiveEditBuilder {
    fn header<I: Into<String>>(&mut self, header: I) -> &mut Self {
        self.header = header.into();
        self
    }

    fn footer<I: Into<String>>(&mut self, footer: I) -> &mut Self {
        self.footer = footer.into();
        self
    }

    fn write<S: serde::Serialize>(&self, stuff: &S) -> anyhow::Result<InteractiveEdit> {
        let tmp = tempfile::Builder::new().suffix(".yaml").tempfile()?;

        writeln!(tmp.as_file(), "{}", &self.header)?;
        serde_yaml::to_writer(tmp.as_file(), stuff)?;
        writeln!(tmp.as_file(), "{}", &self.footer)?;

        tmp.as_file().sync_all()?;

        Ok(InteractiveEdit { tmp })
    }
}

pub struct InteractiveEdit {
    tmp: tempfile::NamedTempFile,
}

impl InteractiveEdit {
    pub fn builder() -> InteractiveEditBuilder {
        InteractiveEditBuilder::default()
    }

    pub fn get_edits<D: serde::de::DeserializeOwned>(&mut self) -> anyhow::Result<D> {
        edit_yaml(self.tmp.path())
    }

    // /// - let a user edit a file
    // /// - pass the contents to take_edits
    // /// - allow the user to retry if take_edits returns an error
    // /// - returns Ok(T) on success or Err(E) if the user gave up
    // pub fn edit<F, D, T>(self, take_edits: F) -> anyhow::Result<T>
    // where
    //     F: Fn(D) -> anyhow::Result<T>,
    //     D: serde::de::DeserializeOwned,
    // {
    //     loop {
    //         match self.get_edits().and_then(|e| take_edits(e)) {
    //             Ok(v) => return Ok(v),
    //             Err(e) => {
    //                 if self.should_retry(e) {
    //                     continue;
    //                 } else {
    //                     return Err(e);
    //                 }
    //             }
    //         }
    //     }
    // }

    pub fn should_retry<E, Tz>(&self, e: &E, imprint: &Imprint<Tz>) -> bool
    where
        E: std::fmt::Display,

        Tz: TimeZone,
        <Tz as TimeZone>::Offset: Copy,
    {
        let mut term = console::Term::stderr();

        let _ = writeln!(term, "{}: {:#}", imprint.bad("Oopsie woopsie"), e);
        let query = format!("Edit {} again?", imprint.path(self.tmp.path()));

        match dialoguer::Confirm::new()
            .with_prompt(&query)
            .interact_on(&term)
        {
            Ok(true) => return true,
            Ok(false) => (),
            Err(e) => {
                let log = crate::logging::please();
                warn!(log, "couldn't prompt user: {}", e);
            }
        };

        let _ = writeln!(term, "Yeah, I'd give up too if I were you.");
        return false;
    }
}

/// A DateishTime but will also handle special terms like "now", "today", "yesterday".
/// When those special values are seen, the [chrono::Local] time zone is used to build
/// the DateishTime.
///
/// ... TODO it would be nice if DateishTime could handle this?
#[derive(Debug, Clone, Copy)]
pub struct DateTimeInput {
    ish: DateishTime,
}

impl DateTimeInput {
    fn datetime_on<Tz: TimeZone>(&self, date: chrono::Date<Tz>) -> Result<DateTime, Error> {
        self.datetime_at(date.and_hms(0, 0, 0))
    }

    fn datetime_at<Tz: TimeZone>(&self, dt: chrono::DateTime<Tz>) -> Result<DateTime, Error> {
        self.ish
            .datetime_with_hint(dt)
            .single()
            .ok_or(Error::InvalidDateish)
            .map(|dt| dt.with_timezone(&chrono::Utc).into())
    }
}

impl std::str::FromStr for DateTimeInput {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        return dateishtime_from_str(s).map(|ish| DateTimeInput { ish });

        fn dateishtime_from_str(s: &str) -> anyhow::Result<DateishTime> {
            if s.eq_ignore_ascii_case("y") | s.eq_ignore_ascii_case("yesterday") {
                return Ok(DateishTime::local_yesterday());
            }
            if s.eq_ignore_ascii_case("now") {
                return Ok(DateishTime::local_now());
            }
            if s.eq_ignore_ascii_case("today") {
                return Ok(DateishTime::local_today());
            }
            if s.eq_ignore_ascii_case("tomorrow") {
                return Ok(DateishTime::local_tomorrow());
            }
            return s.parse::<DateishTime>();
        }
    }
}

/// Find a latest doing in progress before some time and stop it
///
/// Looks for the most recent thing in the past, unless `where` is more specific.
pub(crate) fn in_progress(client: &mut Client, when: DateTime) -> Result<Option<domain::Doing>> {
    client
        .find::<Vec<domain::Doing>, _, _>(
            "doings {uuid, start, note {text}, end}
            ? and(end.is.null, start.le.$since) sort(-start) ..1",
            // I can't figure out how to
            // HashMap::<&'static str, Box<dyn erased_serde::Deserializer + 'static>>::new(),
            serde_json::json!({ "since": when }),
        )
        .map_err(Into::into)
        .map(|i| i.into_iter().next())
}

pub(crate) fn stop_doing(
    client: &mut Client,
    mut doing: &mut domain::Doing,
    when: DateTime,
) -> Result<()> {
    let patch = domain::Doing {
        end: maybe(Some(when)),
        ..Default::default()
    };
    client.update(&*doing, &patch)?;
    patch.patch(&mut doing);
    Ok(())
}

/// Create a new note with the text, and sets it on the existing doing.
pub(crate) fn new_doing_note(
    client: &mut Client,
    mut doing: domain::Doing,
    text: String,
) -> Result<domain::Doing> {
    use impress::dispatch;
    let note = domain::Note {
        uuid: maybe(domain::Uuid::new_v4()),
        text: maybe(text),
        ..Default::default()
    };
    let patch = domain::Doing {
        note: maybe(Box::new(domain::Note {
            uuid: note.uuid.clone(),
            ..Default::default()
        })),
        ..Default::default()
    };
    client.mutate(&[
        dispatch::Change::create(&note).into(),
        dispatch::Change::update(&doing, &patch).into(),
    ])?;
    doing.note = maybe(Box::new(note));
    Ok(doing)
}
