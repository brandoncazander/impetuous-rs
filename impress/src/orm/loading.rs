#[cfg(backtrace)]
use std::backtrace::Backtrace;
use std::iter;
use std::ops::{Deref, DerefMut};

use super::{
    ColumnName, Error, Expr, Modeled, Result, Select, SelectColumn, ToSqlDebug, MAX_PARAMS, Q,
};
use crate::few_or_many::FewOrMany;
use crate::sql::{self, BuildSql, ColumnSource, CursoredGet, FromColumns};

pub trait ModelLoader<M: Modeled>
where
    Self: Sized,
{
    fn add_to_select<'s: 'b, 'b>(&'s self, _: &'b mut Select<'s>) {
        ();
    }

    /// return the parameters to invoke the query with, this should use (and probably always
    /// include) the parameters from the Q that this should borrow from.
    fn take_params(&mut self) -> Option<Vec<&dyn ToSqlDebug>>;

    fn landing<'s: 'b, 'b>(&'s mut self, c: &'_ mut CursoredGet) -> Result<&'s mut Vec<M>>;

    /// TODO I have no idea what this means.
    ///
    /// All I know is it has something to do with loading child records.
    fn weird_backloading_thing<'s: 'b, 'b>(&'s mut self, _: &'b mut Vec<&'s mut M>);

    fn finalize<'a>(self) {
        ();
    }
}

pub struct FlatLoader<'a, M: Modeled> {
    q: Option<&'a Q<M>>,
    out: &'a mut Vec<M>,
}

impl<'a, M: Modeled> FlatLoader<'a, M> {
    pub fn new(q: &'a Q<M>, out: &'a mut Vec<M>) -> Self {
        FlatLoader { q: Some(q), out }
    }
}

impl<'a, M> ModelLoader<M> for FlatLoader<'a, M>
where
    M: Modeled,
    Q<M>: FromColumns<Out = M>,
{
    fn take_params(&mut self) -> Option<Vec<&dyn ToSqlDebug>> {
        // TODO, fail somewhere sooner somehow if there are too many parameters?
        self.q.take().map(|q| q.all_params())
    }

    fn landing<'s: 'b, 'b>(&'s mut self, _: &'_ mut CursoredGet) -> Result<&'s mut Vec<M>> {
        Ok(&mut self.out)
    }

    fn weird_backloading_thing<'s: 'b, 'b>(&'s mut self, v: &'b mut Vec<&'s mut M>) {
        v.extend(self.out.iter_mut())
    }
}

#[allow(type_alias_bounds)]
pub type MergePoint<'a, M: Modeled> = (i64, &'a mut Vec<M>);

pub struct MergePoints<'a, M: Modeled>(Vec<MergePoint<'a, M>>);

impl<'a, M: Modeled> MergePoints<'a, M> {
    /// super sexy constructor for this guy that sorts the given vector
    fn new(mut vec: Vec<MergePoint<'a, M>>) -> Self {
        vec.as_mut_slice().sort_by_key(|(id, _)| *id);
        Self(vec)
    }
}

impl<'a, M: Modeled> Deref for MergePoints<'a, M> {
    type Target = Vec<MergePoint<'a, M>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a, M: Modeled> DerefMut for MergePoints<'a, M> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<'a, M: Modeled> From<Vec<MergePoint<'a, M>>> for MergePoints<'a, M> {
    fn from(other: Vec<MergePoint<'a, M>>) -> Self {
        Self::new(other)
    }
}

pub struct MergeLoader<'a, M: Modeled> {
    q: &'a Q<M>,
    /// since optimizations are best when premature, a FewOrMany is used as the grouping
    /// container because it is might not introduce any indirection for single elements.
    points: Vec<(i64, FewOrMany<&'a mut Vec<M>>)>,
    fetch_size: usize,
    fetch_count: usize,
    /// the child/back table has a column that is a foreign key to the parent/forward table
    column: ColumnName,
    /// with clause
    with_parent: sql::WithAs<
        sql::Values<Expr>,
        sql::Ident<u32>,
        sql::ExprTuple<(sql::Ident<&'static str>,)>,
    >,
    /// filter expression
    in_parent: Expr,
    /// order by
    ordering: sql::OrderBy<SelectColumn>,
}

impl<'a, M: Modeled> MergeLoader<'a, M> {
    pub fn new<G: Into<MergePoints<'a, M>>>(
        q: &'a Q<M>,
        points: G,
        column: ColumnName,
    ) -> Result<MergeLoader<'a, M>> {
        let points = {
            // We're given a sorted vector of pairs ...
            //      P::PK Vec<M>
            // ... where P is the parent model of M.
            //
            // We want to load childen M for each parent P and push them to the vector.
            //
            // But P::PK (in MergePoints) is not unique; so we group the output vectors
            // by the parent here.
            //
            // Later, in landing(), children for each PK are stored in the first vector.
            // Later, in finalize(), we copy children from the first vector to the
            // others in the same group.

            fn peek_while<'i, I, T, F>(
                p: &'i mut std::iter::Peekable<I>,
                pred: F,
            ) -> impl Iterator<Item = T> + 'i
            where
                I: Iterator<Item = T>,
                F: Fn(&T) -> bool + 'i,
            {
                iter::from_fn(move || {
                    let t = p.peek()?;
                    if pred(t) {
                        p.next()
                    } else {
                        None
                    }
                })
            }

            let mut it = points.into().0.into_iter().peekable();

            iter::from_fn(move || {
                let (group_id, first) = it.next()?;
                let group = iter::once(first)
                    .chain(peek_while(&mut it, |(next, _)| *next == group_id).map(|(_, vec)| vec));
                FewOrMany::new(group).map(|fm| (group_id, fm))
            })
            .collect::<Vec<_>>()
        };

        let ordering = (q.t.c(column).expr(), sql::Order::Asc);

        let q_params_len = q.all_params().len();

        let fetch_size = if points.len() + q_params_len <= MAX_PARAMS {
            points.len()
        } else {
            // If we're trying to back load a lot of objects, but more than half of our
            // parameter budget is spent on query paramaters, then decline this request.
            if q_params_len > 500 {
                return Err(Error::TooManyParameters);
            }
            MAX_PARAMS - q_params_len
        };

        if let Some(l) = M::logger() {
            debug!(
                l, "merge loading {}", M::NAME;
                "points" => points.len(),
                "params" => q_params_len,
                "fetch_size" => fetch_size,
            );
        }

        assert!(points.len() > 0);
        assert!(fetch_size + q_params_len <= MAX_PARAMS);

        let with_parent = sql::WithAs {
            stmt: sql::binds::<SelectColumn>(fetch_size).collect::<sql::Values<_>>(),
            name: q.a.borrow_mut().next_alias(),
            columns: sql::ExprTuple::new(sql::ident("id")),
        };

        let in_parent = q.t.c(column).expr().is_in(with_parent.name.to_escaped());

        Ok(MergeLoader {
            q,
            points,
            column,
            with_parent,
            in_parent,
            ordering,
            fetch_size,
            fetch_count: 0,
        })
    }
}

impl<'a, M: Modeled> ModelLoader<M> for MergeLoader<'a, M> {
    fn add_to_select<'s: 'b, 'b>(&'s self, sel: &'b mut Select<'s>) {
        sel.add_column(self.q.t.c(self.column).expr());
        sel.add_with(&self.with_parent as &dyn BuildSql);
        sel.add_filter(&self.in_parent);
        sel.add_order_by(&self.ordering);
    }

    fn take_params(&mut self) -> Option<Vec<&dyn ToSqlDebug>> {
        assert!(self.fetch_size > 0); // can I be a little less paranoid plz? wtf

        let fetched = self.fetch_count * self.fetch_size;

        if fetched >= self.points.len() {
            return None;
        }

        let fetch_ids = self
            .points
            .iter()
            .skip(fetched)
            .take(self.fetch_size)
            .map(|(id, _)| id as &dyn ToSqlDebug);

        // The parent pks are in a WITH ... VALUES clause early on in the query string
        // so they should appear first in our parameters
        let mut params = fetch_ids.collect::<Vec<_>>();
        if params.len() < self.fetch_size {
            // If we ran out of parents, fetch_ids will have fewer items than we need.
            // But we still need the same number of parameters because we're working off
            // the same query string ...
            //
            // Repeat the last fetch_id until params is big enough...
            params.extend(
                params
                    .last()
                    .cloned()
                    .into_iter()
                    .cycle()
                    .take(self.fetch_size - params.len()),
            )
        }
        // Finally include the parameters from the query
        params.extend(self.q.all_params());

        self.fetch_count += 1;
        Some(params)
    }

    fn landing<'s: 'b, 'b>(&'s mut self, c: &'_ mut CursoredGet) -> Result<&'s mut Vec<M>> {
        if self.fetch_count == 0 {
            return Err(Error::Merge {
                reason: "attempt to land rows without fetching any".to_string(),
                #[cfg(backtrace)]
                backtrace: Backtrace::capture(),
            });
        }

        let points = self
            .points
            .iter_mut()
            .skip((self.fetch_count - 1) * self.fetch_size)
            .map(|(id, few_or_many)| (id, few_or_many.first_mut()));

        let row_id: i64 = c.get()?;

        for (here_id, here_ref) in points {
            if *here_id < row_id {
                continue;
            } else if *here_id == row_id {
                return Ok(here_ref);
            } else {
                return Err(Error::Merge {
                    reason: format!("child received out of order ({} > {})", here_id, row_id),
                    #[cfg(backtrace)]
                    backtrace: Backtrace::capture(),
                });
            }
        }

        return Err(Error::Merge {
            reason: "orphaned row".to_string(),
            #[cfg(backtrace)]
            backtrace: Backtrace::capture(),
        });
    }

    fn weird_backloading_thing<'s: 'b, 'b>(&'s mut self, v: &'b mut Vec<&'s mut M>) {
        let iter = self
            .points
            .iter_mut()
            // first_mut since before we finalize, we only append models to the first
            // child vector for each parent pk
            .map(|(_id, few_or_many)| few_or_many.first_mut().iter_mut())
            .flatten();
        v.extend(iter);
    }

    fn finalize(self) {
        for (_, few_or_many) in self.points {
            if let FewOrMany::Many(mut vec) = few_or_many {
                if let Some((first_vec, rest)) = vec.as_mut_slice().split_first_mut() {
                    for rest_vec in rest {
                        rest_vec.extend(first_vec.clone());
                    }
                }
            }
        }
    }
}

/// This for a thing that can load children of M.
///
/// That is, if M is an Article and Articles have Comments such that the Comments table
/// has a foreign key to its Article, then Comment is a child of Article. and
/// BackLoadIntent<Article> is responsible for populating the comments on each article
/// instance that Q<Article> loads.
///
/// (I think?)
pub trait BackLoadIntent<M: Modeled>
where
    Self: Sized,
{
    fn for_select(_: &Q<M>, _: &mut Select) -> Option<Self>
    where
        Self: Sized;

    fn eat_columns(&mut self, _: &mut CursoredGet) -> Result<()>;

    fn tap_model(&mut self, _: &M) -> Result<()>;

    fn load_back_links(
        &self,
        _: &Q<M>,
        _: &mut Vec<&mut M>,
        tx: &rusqlite::Transaction,
    ) -> Result<()>
    where
        Self: Sized;
}
