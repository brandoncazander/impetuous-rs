//!///////////////// stupid thing ahead (why are you still here?)
//! fyi, a shape isn't a set of columns, each column is a kind of expression...
//! so a shape is techincally an expression set
//!
//! below are notes/free association that you may be better off ignoring
//!
//!   how do expressions reference other gatherings? we can't really use memory
//! references because rust's borrow checker will freak out and they're not
//! typically serializable.
//!   if we are supporting GraphQL, we can use child nodes to create a context
//! for what gatherings can be referenced and look them up by their node name
//!  example: gather table_a filter example.a = foo.a and exists bar
//!    foo: gather table_b
//!    bar: gather some_join_or_something where bar.n > 123
//! this quickly turns from a data structure to a scripting language?
//! and is it still possible to do simple things succinctly?
//!  /comments?s=*,article(*)&content=cn.memes
//! ...
//!  gather comments {filter: comments.content contains memes} <- this is serialized?
//!    *
//!    article: gather comments.article
//!      *
//! since a comment's article/parent is a relationship, not a normal field, it must
//! be shaped in order to be serialized?
//!
//! okay, also consider http://htsql.org/doc/tutorial.html
//!
//! /articles{*,author{*},comments{*}?content~'memes'}
//!
//! gather := resource[?filterset][shape]
//! shape := { [meme [, meme]] }
//! meme := either a shape'n gather or a field?
//!
//! todo, eventually generalize the "resource" to an expression? so /1+2 is an
//! endpoint? I don't know if that's valuable though or even "RESTful"
//!
//!
//!
//! todo all the above should be revisited with respect to htsql and graphs
//!
//! TODO how do we add feature negotation? one way is a method like, as_collection,
//! or as_scalar which return Option<T: Collection> where Collection implements
//! Insertable or whatever we want. But this is very specific and weird and kind of
//! hard to do without boxes
//!
//! ----------------------------------------------------------------------------
//!
//! How to do mutations?
//!
//! A very conventionally pretty way of doing this in a way that supports inserting
//! multiple elements of different models is to allow inserting lists with nesting. On
//! the other hand, I'm not sure nesting is really a good thing to do at this layer.
//! I somewhat regret that query builder does it and imql wraps it. Nesting makes sense
//! if you're viewing the data in a UI where things are broken up into trees. But
//! not necessarily in a database or a transfer format.
//!
//! The format for clients requesting mutations to a server should be the same as the
//! format for a server sending realtime update logs to clients and for server to server
//! federation sync.
//!
//! A stream of add or remove operations.
//!
//!   ["add", "article", {title:   "Happy Little Trees"}]
//!   ["rem", "comment", {uuid: ...}]
//!
//! The nice thing about using imql is that type inference means you don't have to
//! include the model in each operation...
//!
//! - How do you state how items are related?
//!
//! Nesting and automatically referencing a/the nearest parent is limited. If using the
//! nearest parent, you can't attach yourself to two different entities, like an article
//! and a user. If you try to use all parents you can't easily disambiguate between
//! instances of the same type. For example, inserting two users, an article by one of
//! them, and comments on the article by each user.
//!
//! - Include reference syntax for link/id fields?
//!
//!   ["add", "articles", {uuid:    {ref: "trees"}, title:   "Happy Little Trees"}]
//!   ["add", "comments", {article: {ref: "trees"}, content: "Trees are useless, they do not even taste good."}]
//!
//! Is this easy for a client to produce?
//!
//! Reference cycles are not an issue since everything is ordered. Each operation cannot
//! reference something beneath it. (Is this is a limitation too. Presumably if models
//! reference each other one field must be nullable/optional or validation is delayed.)
//!
//! Also, a feature not addressed yet is some sort of optimistic locking thing. Either
//! each table has a version revision field that must be matched in an incoming
//! mutation. Or the mutation must include the old value for each modified field.
//!
//!   {old: {title: "Mistakes"}, new: {title: "Happy Accidents"}}
//!
//! To be *clear* the idea of optimistic locking like this is a mistake/proxy/instance
//! for more generally expressing preconditions before making claims about data. With
//! this approach, we can only make claims about the record we're touching. Which means
//! we can't make any claims about the state of the system in an insert. So we can't
//! express, "Alice buys Bob a red birthday card, given that Alice has not bought Bob a
//! birthday card". Or claims that include other objects like the previous with the
//! additional condition; "Bob's favourite colour is red".
//!
//! Insertions and deletions only have new and old respectively.
//!
//! - Is this format practical to be used in federation?
//!
//! If federation works where each instance knows about its peers and pushes out updates
//! to them, it can save a change and keep that change up-to-date. The change basically
//! represents a difference between some peer's knowledge of a particular object and
//! what that object looks like now.
//!
//! But federation peers need to specify uuids for entities as to track them with any
//! consistency. Should we allow clients to do the same?

use std::convert::TryFrom;
use std::fmt::Debug;

pub mod parse;
pub mod serde;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid query: {err}")]
    InvalidQuery { err: String },
}

pub fn parse_query<'q>(query: &'q str) -> Result<Query<&'q str>, Error> {
    match nom::combinator::all_consuming(parse::query)(query.trim()) {
        Ok((_, query)) => Ok(query),
        Err(e) => match e {
            nom::Err::Incomplete(_) => Err(Error::InvalidQuery {
                err: "Not enough input".to_owned(),
            }),
            nom::Err::Error(e) | nom::Err::Failure(e) => Err(Error::InvalidQuery {
                err: nom::error::convert_error(query, e),
            }),
        },
    }
}

impl<'q> TryFrom<&'q str> for Query<&'q str> {
    type Error = Error;

    fn try_from(s: &'q str) -> Result<Self, Self::Error> {
        parse_query(s)
    }
}

macro_rules! from_impl {
    ($from:ident<S> -> $to:ident :: $variant:ident) => {
        impl<S> From<$from<S>> for $to<S> {
            fn from(other: $from<S>) -> $to<S> {
                $to::$variant(other)
            }
        }
    };
}

// TODO s/Op/Operator or something to disambiguate from Operation
#[derive(Debug, PartialEq)]
pub enum ComparisonOp {
    // ILike,
    Eq,
    Ne,
    Gt,
    Ge,
    Lt,
    Le,
    Is, // is null, true, false?
    IsNot,
}

#[derive(Debug, PartialEq)]
pub enum LogicOp {
    And,
    Or,
}

#[derive(Debug, PartialEq)]
pub struct FieldExpr<S>(pub S);

#[derive(Debug, PartialEq)]
pub struct ParamExpr<S>(pub S);

#[derive(Debug, PartialEq)]
pub enum SimpleExpr<S> {
    Null,
    Bool(bool),
    Field(FieldExpr<S>),
    Param(ParamExpr<S>),
}

from_impl!(FieldExpr<S> -> SimpleExpr::Field);
from_impl!(ParamExpr<S> -> SimpleExpr::Param);

#[derive(Debug, PartialEq)]
pub struct ComparisonExpr<S>(pub FieldExpr<S>, pub ComparisonOp, pub SimpleExpr<S>);

#[derive(Debug, PartialEq)]
pub struct LogicExpr<S>(pub LogicOp, pub Vec<ComplexExpr<S>>);

#[derive(Debug, PartialEq)]
pub enum ComplexExpr<S> {
    Logic(LogicExpr<S>),
    Comparison(ComparisonExpr<S>),
}

from_impl!(LogicExpr<S> -> ComplexExpr::Logic);
from_impl!(ComparisonExpr<S> -> ComplexExpr::Comparison);

/// foo.eq.$bar
pub type FilterExpr<S> = ComplexExpr<S>;

#[derive(Debug, PartialEq)]
pub enum Limit<S> {
    N(u64),
    Param(ParamExpr<S>),
}

#[derive(Debug, PartialEq)]
pub enum Direction {
    Asc,
    Desc,
}

/// priority,-published_at
pub type OrderExpr<S> = (Direction, FieldExpr<S>);

pub type MutExpr<S> = ParamExpr<S>;

#[derive(Debug, PartialEq)]
pub enum Pattern<S> {
    Filter(FilterExpr<S>),
    // TODO Should this just flattened to Shape(Query<S>)?
    Shape(Vec<Query<S>>),
    Limit(Limit<S>),
    Order(Vec<OrderExpr<S>>),
    // Mutation(Mutation<S>),
}

/// some important data structure; fundamental to interacting with impetuous data models
#[derive(Debug, PartialEq)]
pub struct Query<S> {
    pub on: S,
    pub patterns: Vec<Pattern<S>>,
}

impl<S> Query<S> {
    pub fn on(on: S) -> Self {
        Query {
            on,
            patterns: Default::default(),
        }
    }
}

// /// an insert or update or a deletion
// #[derive(Debug, PartialEq)]
// pub enum Mutation<S> {
//     Upsert { expr: ParamExpr<S> },
//     Delete { expr: ParamExpr<S> },
// }

// impl<S> Into<Pattern<S>> for Mutation<S> {
//     fn into(self) -> Pattern<S> {
//         Pattern::Mutation(self)
//     }
// }
