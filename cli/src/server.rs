use std::{
    convert::{Into, TryInto},
    fmt,
    rc::Rc,
    str::FromStr,
    time::Instant,
};

use anyhow::Context;
use bytes::buf::ext::BufExt;
use hyper::service::{make_service_fn, service_fn};
use hyper::{header, Body, StatusCode};
use parking_lot::Mutex;
use serde::Serialize;
use structopt::StructOpt;

use impress::dispatch::{self, Findable, Mutable};

use crate::{cli, domain, Impetuous};

type Request = hyper::Request<Body>;
type Response = hyper::Response<Body>;

/// these are all internal errors probably
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("dispatch error: {}", source)]
    Dispatch {
        #[from]
        source: dispatch::Error,
        #[cfg(backtrace)]
        backtrace: std::backtrace::Backtrace,
    },
    #[error("request body read error: {}", source)]
    BodyRead {
        #[from]
        source: hyper::Error,
    },
    #[error("a response builder failed: {}", source)]
    BadResponse {
        #[from]
        source: hyper::http::Error,
    },
    #[error("serialization failure for {}: {}", format, source)]
    Serialization {
        format: ContentType,
        source: Box<dyn std::error::Error + Send + Sync + 'static>,
    },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ContentType {
    Json,
    Cbor,
}

impl fmt::Display for ContentType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.mime())
    }
}
impl std::str::FromStr for ContentType {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // we're using starts_with here as a short-cut for also being able to handle
        // "foo/bar;???" in an Accept header ... TODO this is nasty
        if s.starts_with(ContentType::Cbor.mime()) {
            Ok(ContentType::Cbor)
        } else if s.starts_with(ContentType::Json.mime()) {
            Ok(ContentType::Json)
        } else {
            Err("expected either application/json or application/cbor")
        }
    }
}

impl ContentType {
    fn mime(&self) -> &'static str {
        match self {
            ContentType::Json => "application/json",
            ContentType::Cbor => "application/cbor",
        }
    }

    fn serialize<V: Serialize>(&self, v: &V) -> Result<Vec<u8>> {
        match self {
            ContentType::Json => serde_json::to_vec(v).map_err(Into::into),
            ContentType::Cbor => serde_cbor::to_vec(v).map_err(Into::into),
        }
        .map_err(|source| Error::Serialization {
            format: *self,
            source,
        })
    }

    fn header_value(&self) -> header::HeaderValue {
        header::HeaderValue::from_static(self.mime())
    }
}

#[derive(Debug)]
enum Acceptable {
    Whatever,
    Prefer(ContentType),
}

impl std::str::FromStr for Acceptable {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("*/") || s.starts_with("application/*") {
            Ok(Acceptable::Whatever)
        } else if s.starts_with(ContentType::Cbor.mime()) {
            Ok(Acceptable::Prefer(ContentType::Cbor))
        } else if s.starts_with(ContentType::Json.mime()) {
            Ok(Acceptable::Prefer(ContentType::Json))
        } else {
            Err(())
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub struct Serve {
    /// address to listen on, defaults to `[serve] address` in your config file or 127.0.0.1:7193
    #[structopt(short, long)]
    pub address: Option<std::net::SocketAddr>,
}

impl cli::Command for Serve {
    fn run(self, gen: &cli::General, im: &Impetuous) -> anyhow::Result<()> {
        // For some reason hyper or futures or the borrow checker or something prevents
        // us from using a Mutex<&Impetuous> with hyper server's server()...
        // It seems to require that everything is borrowed for the static lifetime and I
        // can't figure out why.
        let im = Impetuous::from_system()?;

        let address = self.address.unwrap_or(im.config().serve.address);

        let mut rt = im.tokio()?;

        let db = im.db().context("open database")?;
        let db = Rc::new(Mutex::new(db));
        let im = Rc::new(Mutex::new(im));
        let log = crate::logging::please().clone();
        let handler = DomainHandler { log, db, im };

        // ehhhhhhhhh......
        let make_service = make_service_fn(move |_| {
            let handler = handler.clone();
            async move {
                Ok::<_, Error>(service_fn(move |req| {
                    let h = handler.clone();
                    info!(h.log, "Hello!"; "uri" => req.uri().to_string());

                    let then = Instant::now();

                    async move {
                        let mut resp = match h.handle_request(req).await {
                            Ok(resp) => resp,
                            Err(e) => {
                                #[cfg(backtrace)]
                                let bt = std::error::Error::backtrace(&e).map(|bt| bt.to_string());
                                #[cfg(not(backtrace))]
                                let bt = Option::<String>::None;
                                error!(h.log, "Failed to handle request: {}", e; "backtrace" => bt);

                                let mut resp = hyper::Response::new(Body::empty());
                                *resp.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                resp
                            }
                        };

                        let extra_headers = h.im.lock().config().serve.headers.clone();

                        resp.headers_mut().extend(extra_headers);

                        let ms = Instant::now()
                            .checked_duration_since(then)
                            .map(|d| d.as_millis())
                            .unwrap_or(0);
                        info!(h.log, "Bye!"; "ms" => ms);

                        Ok::<_, Error>(resp)
                    }
                }))
            }
        });

        // Start a server?
        return tokio::task::LocalSet::new().block_on(&mut rt, async move {
            let log = &crate::logging::please();

            // Even though some of this isn't async, it will panic at runtime if we
            // don't run it in a future under tokio ... because of reasons and global
            // state I guess ......

            let (tx, rx) = tokio::sync::oneshot::channel::<()>();

            let server = hyper::Server::try_bind(&address)
                .with_context(|| format!("bind to: {}", address))?
                .executor(LocalExec)
                .serve(make_service);

            let bind_address = server.local_addr();

            let server = server.with_graceful_shutdown(async {
                let _ = rx.await;
            });

            warn!(log, "Server started"; "address" => &bind_address);

            if let Err(e) = server.await {
                error!(log, "server error: {}", e);
            }

            Ok(())
        });

        // Apparently this shit is needed to ... I don't know
        // ... spawn futures into a runtime executor task set?
        //
        // I guess people thought threading was hard so they decided to invent tokio to
        // simplify things and tokio did such a great job that somehow I need tokio for
        // everything now even if I don't want threading.
        //
        // Did you know that if you try to run this in a tokio runtime when you didn't
        // call enable_io() on the runtime builder, something will panic saying:
        //
        // thread 'main' panicked at 'no current reactor', src/libcore/option.rs:1188:5
        //
        // ... What the fuck is a reactor?
        #[derive(Clone, Copy, Debug)]
        struct LocalExec;

        impl<F> hyper::rt::Executor<F> for LocalExec
        where
            F: std::future::Future + 'static,
        {
            fn execute(&self, fut: F) {
                tokio::task::spawn_local(fut);
            }
        }
    }
}

fn bad_request<S: AsRef<str>>(msg: S, ct: ContentType) -> Result<Response> {
    reply_with(StatusCode::BAD_REQUEST, &msg.as_ref(), ct)
}

fn reply_with<S>(status: StatusCode, msg: &S, ct: ContentType) -> Result<Response>
where
    S: Serialize,
{
    let mut resp = hyper::Response::builder()
        .status(status)
        .body(Body::from(ct.serialize(msg)?))?;
    resp.headers_mut().insert("content-type", ct.header_value());
    Ok(resp)
}

fn maybe_read_header<F>(h: header::HeaderName, request: &Request) -> Option<Result<F, ()>>
where
    F: FromStr,
{
    request.headers().get(h).map(|hv| {
        // lol ... errors? ... what errors?
        hv.to_str()
            .map_err(|_| ())
            .and_then(|a| F::from_str(a).map_err(|_| ()))
    })
}

#[derive(Debug, Clone)]
struct DomainHandler {
    log: slog::Logger,
    db: Rc<Mutex<rusqlite::Connection>>,
    im: Rc<Mutex<Impetuous>>,
}

impl DomainHandler {
    /// be a bit careful about the order in which db and im are locked to avoid
    /// deadlocks?
    async fn handle_request(&self, request: Request) -> Result<Response> {
        let default_content_type = {
            let im = self.im.lock();
            im.config().serve.default_content_type.clone()
        };

        let accept: ContentType =
            match maybe_read_header(header::ACCEPT, &request).unwrap_or(Ok(Acceptable::Whatever)) {
                Ok(Acceptable::Whatever) => default_content_type,
                Ok(Acceptable::Prefer(this)) => this,
                Err(_) => return bad_request(
                    "Invalid accept header -- I can give you application/json or application/cbor",
                    default_content_type,
                ),
            };

        let uri = match request
            .uri()
            .path_and_query()
            .map(|pq| pq.as_str())
            .map(|uri| {
                percent_encoding::percent_decode_str(uri.trim_start_matches('/'))
                    .decode_utf8_lossy()
                    .trim()
                    .to_string()
            }) {
            Some(uri) => uri,
            None => return bad_request("The request is missing a query", accept),
        };

        let data_type: ContentType =
            match maybe_read_header(header::CONTENT_TYPE, &request).unwrap_or(Ok(accept)) {
                Ok(ct) => ct,
                Err(()) => return bad_request(
                    "Invalid content-type header -- expected application/json or application/cbor",
                    default_content_type,
                ),
            };

        let body = hyper::body::aggregate(request).await?.reader();

        let db = &mut *self.db.as_ref().lock();
        let im = &*self.im.as_ref().lock();
        let interaction = domain::Interaction {
            im,
            orm: dispatch::Interaction::new(db, crate::logging::please().clone()),
        };

        let result: dispatch::Result<dispatch::Reply> = if uri.is_empty() {
            // mutate
            match data_type {
                ContentType::Json => {
                    let mut de = serde_json::de::Deserializer::from_reader(body);
                    interaction.mutate_from_de(&mut de)
                }
                ContentType::Cbor => {
                    let mut de = serde_cbor::de::Deserializer::from_reader(body);
                    interaction.mutate_from_de(&mut de)
                }
            }
        } else {
            // find
            let query: imql::Query<&str> = match uri.as_str().try_into() {
                Ok(q) => q,
                Err(e) => return bad_request(format!("{}", e), accept),
            };

            // TODO I can't figure out how to put this into a box or anything ...
            match data_type {
                ContentType::Json => {
                    let mut de = serde_json::de::Deserializer::from_reader(body);
                    interaction.find(query, &mut de)
                }
                ContentType::Cbor => {
                    let mut de = serde_cbor::de::Deserializer::from_reader(body);
                    interaction.find(query, &mut de)
                }
            }
        };

        let reply = match result {
            Ok(reply) => reply,
            Err(e) => match e {
                dispatch::Error::BadRequest { .. }
                | dispatch::Error::Unsupported { .. }
                | dispatch::Error::InvalidBinding { .. } => {
                    return bad_request(format!("{}", e), accept)
                }
                dispatch::Error::NotFound { .. } => {
                    let msg = e.to_string();
                    return reply_with(StatusCode::NOT_FOUND, &msg, accept);
                }
                dispatch::Error::Conflict { current, .. } => {
                    return reply_with(StatusCode::CONFLICT, &current, accept)
                }
                dispatch::Error::Db { .. } | dispatch::Error::QueryFailed { .. } => Err(e)?,
            },
        };

        let body = Body::from(accept.serialize(&reply.content)?);
        let mut resp = Response::new(body);
        resp.headers_mut()
            .insert("content-type", accept.header_value());
        Ok(resp)
    }
}
