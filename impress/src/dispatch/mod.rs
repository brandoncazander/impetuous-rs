//! There was planning here, but then the result didn't look anything like the plan. So
//! now this is there is.  :)
use std::any::Any;
#[cfg(backtrace)]
use std::backtrace::Backtrace;

mod binding;
pub mod mutable;

pub use crate::orm::{self, HasPrimaryKey, HasSurrogateKey};
pub use binding::{Bindable, LateBindings};
pub use mutable::{BorrowChange, Changable, Change, Mutable};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("unsupported/not found: {what}")]
    // TODO this needs to be fixed as to add more context ...
    Unsupported { what: String },
    #[error("invalid binding: {reason}")]
    InvalidBinding { reason: String },
    #[error("bad request: {why}")]
    BadRequest { why: String, index: Option<usize> },
    #[error("current state differs from expected")]
    Conflict {
        current: Box<dyn SerializeDebug + Send + Sync>,
        index: Option<usize>,
    },
    #[error("not found: `{reference}`")]
    NotFound {
        // thing: String,
        reference: String,
        index: Option<usize>,
    },
    /// internal error, not usually shown to users
    #[error("failed to perform query: {source}")]
    QueryFailed {
        #[from]
        source: orm::Error,
        #[cfg(backtrace)]
        backtrace: Backtrace,
    },
    /// internal error, not usually shown to users
    #[error("database error: {source}")]
    Db {
        #[from]
        source: rusqlite::Error,
        #[cfg(backtrace)]
        backtrace: Backtrace,
    },
}

impl Error {
    pub fn at_index(self, index: usize) -> Error {
        match self {
            Error::BadRequest { why, index: _ } => Error::BadRequest {
                index: Some(index),
                why,
            },
            Error::Conflict { current, index: _ } => Error::Conflict {
                index: Some(index),
                current,
            },
            Error::NotFound {
                reference,
                index: _,
            } => Error::NotFound {
                index: Some(index),
                reference,
            },
            _ => self,
        }
    }

    pub fn new_not_in_old(field: &'static str) -> Error {
        let why = format!("new field `{}` not in old", field);
        Error::BadRequest { why, index: None }
    }

    pub fn missing_surrogate<T>(_: &T) -> Error
    where
        T: HasSurrogateKey,
    {
        let why = format!(
            "the surrogate key `{}` must be specified",
            <T as HasSurrogateKey>::column().name,
        );
        Error::BadRequest { why, index: None }
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

pub trait SerializeDebug: erased_serde::Serialize + std::fmt::Debug {}

impl<T> SerializeDebug for T where T: erased_serde::Serialize + std::fmt::Debug {}

// If erased_serde::Serialize implements serde::Serialize, then shouldn't anything with
// erased_serde::Serialize as a super-trait also implement it? I really have no idea
// what's going on.
//
// todo, look at this macro definition at some point and figure out what it does ...
erased_serde::serialize_trait_object!(SerializeDebug);

/// Used to downcast ReplyContent into a concrete type
pub trait IntoAnyBox {
    fn into_any_box(self: Box<Self>) -> Box<dyn Any>;
}

impl<T: Any> IntoAnyBox for T {
    fn into_any_box(self: Box<Self>) -> Box<dyn Any> {
        self
    }
}

pub trait ReplyContent: erased_serde::Serialize + IntoAnyBox {}

erased_serde::serialize_trait_object!(ReplyContent);

impl<T> ReplyContent for T where T: erased_serde::Serialize + IntoAnyBox {}

pub struct Reply {
    pub content: Box<dyn ReplyContent>,
}

pub trait Findable<'de> {
    /// This consumes self in order to consume the late bindings?
    ///
    /// But this interface should probably be extended to support multiple verbs in one
    /// interaction. Particularly for mutability if that isn't going to be part of imql.
    ///
    /// So somehow we should be able to separate out interactions and requests?
    fn find<D>(self, query: imql::Query<&str>, data: D) -> Result<Reply>
    where
        D: serde::de::Deserializer<'de>;
}

/// Used by [mutable::Mutable] to help handle errors ...
pub trait MapErr {
    // TODO maybe handle constraint violations explicitly with some variant or something
    //
    // Also, if we handle lower down, we can maintain the index?
    fn map_err(e: rusqlite::Error) -> Error {
        Error::from(e)
    }
}

/// this whole API is so screwy; basically, users of this module have some "Interaction" type that
/// the macros here implement [Findable] and [Mutable] on. But in order to do that, they need an
/// [Interaction]. So they need to get one from the user's type that we're implementing these
/// traits on. We can use Into<Interaction> but then we lose everything on the user type so what's
/// the point in that. Instead we split the thing into (user data, Interaction).
///
/// The whole type thing is up-side down thatour Interaction should have a generic field on it that
/// the user data can be provided in through there. But I'm not sure I can do that without also
/// solving the problem that the user interaction type can't have/must only have one lifetime. And
/// I dont' know how to solve that. It's a fucking nightmare.
pub trait IntoParts<Rh> {
    type Lh;

    fn into_parts(self) -> (Self::Lh, Rh);
}

pub struct Interaction<'db> {
    pub db: &'db mut rusqlite::Connection,
    pub log: slog::Logger,
    pub bindings: LateBindings,
}

impl<'db> Interaction<'db> {
    pub fn new(db: &'db mut rusqlite::Connection, log: slog::Logger) -> Self {
        Interaction {
            db,
            log,
            bindings: Default::default(),
        }
    }
}

/// A private trait to help implementing dispatch interactions around the db/orm query builder
pub trait ExprForField {
    fn expr_for_field(&self, field: &str) -> Result<orm::Expr>;

    fn use_param<'f>(&mut self, with: &str, param: &'f str) -> Result<()>;

    fn shape(&mut self, query: imql::Query<&str>) -> Result<()>;

    fn filter(&mut self, filter: imql::FilterExpr<&str>) -> Result<orm::Expr> {
        use crate::sql;

        let expr = match filter {
            imql::ComplexExpr::Logic(logic) => {
                let imql::LogicExpr(op, complexes) = logic;

                let mut exprs = Vec::<orm::Expr>::with_capacity(complexes.len());
                for complex in complexes {
                    exprs.push(ExprForField::filter(self, complex)?);
                }

                let op = match op {
                    imql::LogicOp::And => sql::BinOperator::And,
                    imql::LogicOp::Or => sql::BinOperator::Or,
                };

                orm::Expr::Binary(op, exprs)
            }
            imql::ComplexExpr::Comparison(cmp) => {
                let imql::ComparisonExpr(imql::FieldExpr(field), op, simple) = cmp;

                let lh = self.expr_for_field(field)?;

                let rh: orm::Expr = match simple {
                    imql::SimpleExpr::Null => orm::NULL,
                    imql::SimpleExpr::Bool(true) => orm::TRUE,
                    imql::SimpleExpr::Bool(false) => orm::FALSE,
                    imql::SimpleExpr::Field(imql::FieldExpr(field)) => {
                        self.expr_for_field(field)?
                    }
                    imql::SimpleExpr::Param(imql::ParamExpr(param)) => {
                        self.use_param(field, param)?;
                        orm::BIND
                    }
                };

                match op {
                    imql::ComparisonOp::Eq => lh.eq(rh),
                    imql::ComparisonOp::Ne => lh.ne(rh),
                    imql::ComparisonOp::Gt => lh.gt(rh),
                    imql::ComparisonOp::Ge => lh.ge(rh),
                    imql::ComparisonOp::Lt => lh.lt(rh),
                    imql::ComparisonOp::Le => lh.le(rh),
                    imql::ComparisonOp::Is => lh.is(rh),
                    imql::ComparisonOp::IsNot => lh.is_not(rh),
                }
            }
        };

        Ok(expr)
    }
}

// todo, move into ExprForField and name that CanQuery or something ...?
pub trait CanQuery {
    fn query(self, _: imql::Query<&str>) -> Result<()>;
}

/// This shouldn't be a macro. It would be so much better if the dispatch could be
/// defined by a crazy builder thing like how the warp library allows you to build
/// request handlers to adapt some fancy imql query vistor to your domain. But I'm not
/// smart enough to make that.
///
/// When all you have are nails, everything looks like a hammer ... Right?
#[macro_export]
macro_rules! orm_interaction {
    (
        @on $domain:ident ...
        $(
        $model:ident {
            $( $field:ident: $type:ty, )*
            $( @link $link:ident -> $link_model:ident, )*
            $( @default-sort $default_sort:tt, )?
            $( @policy $policy:tt, )*
        }
        )*
    ) => {
    /// Implement something that can serve an interaction?
    impl<'db, 'de> $crate::dispatch::Findable<'de> for $domain<'db> {
        fn find<D>(mut self, query: imql::Query<&str>, data: D) -> $crate::dispatch::Result<$crate::dispatch::Reply>
        where
            D: serde::de::Deserializer<'de>,
            Self: $crate::dispatch::IntoParts<$crate::dispatch::Interaction<'db>>,
        {
            use std::convert::TryFrom;

            use $crate::orm;
            use $crate::dispatch::{self, Result, Error, ExprForField, CanQuery, IntoParts};

            return match query.on {
                $(
                <$model as orm::Modeled>::NAME => {
                    let mut q = <$model as orm::Modeled>::q();

                    Step(&mut self, On::$model(&mut q)).query(query)?;

                    let (_, interaction): (_, dispatch::Interaction) = self.into_parts();

                    interaction.bindings.resolve(data)?;

                    let mut tx = interaction.db.transaction()?;

                    let items = q.list(&mut tx)?;

                    let content = Box::new(items);
                    Ok(dispatch::Reply { content })
                },
                )*
                _ => return Err(Error::Unsupported { what: query.on.to_string() }),
            };

            enum On<'q> {
                #[allow(unused)]
                Field,
                $( $model (&'q mut orm::Q<$model>), )*
            }

            struct Step<'db, 'q>(&'q mut $domain<'db>, On<'q>);

            impl<'db, 'q> dispatch::CanQuery for Step<'db, 'q> {
                fn query(self, query: imql::Query<&str>) -> Result<()> {
                    let Step(mut interaction, on) = self;
                    match on {
                    $(
                        On::$model(mut q) => {
                            for pat in query.patterns {
                                let mut step = Step(interaction, On::$model(q));
                                match pat {
                                    imql::Pattern::Shape(shape) => {
                                        for sub_query in shape {
                                            step.shape(sub_query)?;
                                        }
                                    },
                                    imql::Pattern::Filter(filter) => {
                                        let expr = ExprForField::filter(&mut step, filter)?;
                                        q.filter.push(expr);
                                    },
                                    imql::Pattern::Limit(limit) => {
                                        match limit {
                                            imql::Limit::N(limit) => {
                                                q.limit = i64::try_from(limit)
                                                    .map(orm::Limit::from)
                                                    .map(Some)
                                                    .map_err(|_| {
                                                        let what = format!("limit {} out of range (max {})", limit, i64::max_value());
                                                        Error::Unsupported { what }
                                                    })?;
                                            }
                                            imql::Limit::Param(imql::ParamExpr(param)) => {
                                                let orm: &mut dispatch::Interaction = interaction.as_mut();
                                                let place = orm.bindings.bind::<i64>(param.to_owned())?;
                                                q.limit(orm::Limit::Bind(Box::new(place)));
                                            }
                                        }
                                    },
                                    imql::Pattern::Order(ordering) => {
                                        let mut exprs = Vec::with_capacity(ordering.len());
                                        for order in ordering {
                                            let (dir, imql::FieldExpr(field)) = order;
                                            let field = ExprForField::expr_for_field(&mut step, field)?;
                                            let expr = match dir {
                                                imql::Direction::Asc => field.asc(),
                                                imql::Direction::Desc => field.desc(),
                                            };
                                            exprs.push(expr);
                                        }
                                        q.order_by.extend(exprs);
                                    },
                                }
                            }

                            $( ($policy)(&mut interaction, &mut q); )*

                            $(
                                if q.order_by.is_empty() {
                                    q.order_by(|e| ($default_sort)(&mut interaction, e));
                                }
                            )?

                            return Ok(());
                        }
                    )*
                        On::Field => return Err(Error::Unsupported { what: query.on.to_string() }),
                    }
                }
            }

            impl<'db, 'q> ExprForField for Step<'db, 'q> {

                fn shape(&mut self, query: imql::Query<&str>) -> Result<()> {
                    let Step(interation, on) = self;

                    match on {
                        On::Field => return Err(Error::Unsupported { what: query.on.into() }),
                        $(
                        On::$model (q) => {
                            match query.on {
                                $(
                                stringify!($field) => {
                                    q.get(|fields| fields.$field());

                                    for _ in query.patterns {
                                        return Err(Error::Unsupported { what: query.on.into() });
                                    }
                                },
                                )*
                                $(
                                stringify!($link) => {
                                    let mut link_q = <$model as $crate::orm::Modeled>::LinkSet::$link(q);
                                    let step = Step(interation, On::$link_model(&mut link_q));
                                    step.query(query)?;
                                }
                                )*
                                _ => return Err(Error::Unsupported { what: query.on.into() }),
                            };
                        },
                        )*
                    }

                    Ok(())
                }

                fn expr_for_field(&self, field: &str) -> Result<orm::Expr> {
                    let Step(_, on) = self;

                    match on {
                        On::Field => return Err(Error::Unsupported { what: field.into() }),
                        $(
                        On::$model (q) => match field {
                            $(
                            stringify!($field) => Ok(q.expr().$field()),
                            )*
                            _ => return Err(Error::Unsupported { what: field.into() }),
                        },
                        )*
                    }
                }

                fn use_param<'f>(&mut self, with: &str, param: &'f str) -> Result<()> {
                    let Step(interaction, on) = self;
                    let orm: &mut dispatch::Interaction = interaction.as_mut();

                    match on {
                        On::Field => return Err(Error::Unsupported { what: with.into() }),
                        $(
                        On::$model (q) => match with {
                            $(
                            stringify!($field) => {
                                let place = orm.bindings.bind::<$type>(param.to_owned())?;
                                // This is a little goofy but ... hey, it works.
                                q.bind(Box::new(place));
                                Ok(())
                            }
                            )*
                            _ => return Err(Error::Unsupported { what: with.into() }),
                        },
                        )*
                    }
                }
            }
        }
    }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::convert::AsMut;
    use std::convert::TryInto;

    use orm::tests::{articles_database, Article, Person};

    pub type Result<T = (), E = Box<dyn std::error::Error>> = std::result::Result<T, E>;

    pub struct TestInteraction<'db> {
        orm: Interaction<'db>,
        whoami: String,
        now: i64,
    }

    impl<'db> MapErr for TestInteraction<'db> {}

    impl<'db> AsMut<Interaction<'db>> for TestInteraction<'db> {
        fn as_mut(&mut self) -> &mut Interaction<'db> {
            &mut self.orm
        }
    }

    impl<'db> IntoParts<Interaction<'db>> for TestInteraction<'db> {
        type Lh = ();
        fn into_parts(self) -> (Self::Lh, Interaction<'db>) {
            ((), self.orm)
        }
    }

    orm_interaction! {
        @on TestInteraction ...
        Article {
            uuid: uuid::Uuid,
            title: String,
            content: String,
            published_at: i64,
            @link author -> Person,

            @policy (|int: &mut TestInteraction, q: &mut orm::Q<Article>| {
                // only authors can see post-dated articles.

                // holy shit this is so fucking convoluted
                if q.links.author.is_none() {
                    let author = q.links.author.get_or_insert(Box::new(q.again()));
                    author.is_silent = true;
                }
                let author = q.links.author.as_ref().unwrap();

                let article = q.expr();
                let filter = article
                    .published_at()
                    .le(orm::BIND)
                    .or(author.expr().name().eq(orm::BIND));
                q.filter(|_| filter);
                q.bind(Box::new(int.now));
                // I can't borrow from the interaction into the Q ...
                // It's my fault for using macros where they're not appropriate.
                q.bind(Box::new(int.whoami.clone()));
            }),
        }
        Person {
            uuid: uuid::Uuid,
        }
    }

    pub fn articles_interaction<'a>(
        db: &'a mut rusqlite::Connection,
    ) -> Result<TestInteraction<'a>> {
        Ok(TestInteraction {
            orm: Interaction {
                log: crate::test_logger(),
                db,
                bindings: Default::default(),
            },
            whoami: "Spongebob".to_owned(),
            now: 3000,
        })
    }

    #[test]
    fn test_dispatch() -> Result {
        fn do_the_thing<'de, D>(query: imql::Query<&str>, data: D) -> Result<serde_json::Value>
        where
            D: serde::de::Deserializer<'de>,
        {
            let mut db = articles_database()?;
            let i = articles_interaction(&mut db)?;
            let res = i.find(query, data)?;
            let json = serde_json::to_value(&res.content)?;
            Ok(json)
        }

        let query: imql::Query<&str> =
            match "articles ? published_at.ge.$since {title, content, published_at}".try_into() {
                Ok(query) => query,
                Err(bad) => {
                    eprintln!("{}", bad);
                    panic!(bad);
                }
            };

        let mut data = serde_json::Deserializer::from_slice(br#"{"since": 2000}"#);
        let got = do_the_thing(query, &mut data)?;
        let expected = serde_json::json!([
            {
              "title": "Happy Little Trees",
              "content": "",
              "published_at": 2345
            },
            {
              "title": "starfish title",
              "content": "starfish content",
              "published_at": 2346
            },
            {
              "title": "Sponges are the best",
              "content": "... because they are so absorbent",
              "published_at": 4567
            },
        ]);

        assert_eq!(
            got, expected,
            "got:\n{:#?}\nexpected:\n{:#?}",
            got, expected
        );

        Ok(())
    }
}
