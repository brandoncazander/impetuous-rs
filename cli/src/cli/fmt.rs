use std::{cell::Cell, fmt};

use ansi_term::{Colour as Color, Style};
use chrono::{Duration, Local, TimeZone};

use crate::{domain, DateTime, Impetuous};

const PADDING: &'static str = "                                                                                                                                                                                                                            ";

/// Implements display by conditionally
pub(crate) struct DisplayOrElse<T: fmt::Display>(Option<T>, &'static str);

impl<T: fmt::Display> fmt::Display for DisplayOrElse<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(t) = &self.0 {
            t.fmt(f)
        } else {
            self.1.fmt(f)
        }
    }
}

/// A bunch of [Color]s used to build [Styles] from.
#[derive(Debug, Clone)]
pub struct Palette {
    pub red: Color,
    pub green: Color,
    // pub yellow: Color,
    pub blue: Color,
    pub magenta: Color,
    pub cyan: Color,
}

impl Default for Palette {
    fn default() -> Self {
        let red = Color::Fixed(9);
        let green = Color::Fixed(10);
        let blue = Color::Fixed(12);
        let magenta = Color::Fixed(13);
        let cyan = Color::Fixed(14);
        Palette {
            red,
            green,
            blue,
            magenta,
            cyan,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Styles {
    // types
    pub time: Style,
    pub date: Style,
    pub path: Style,
    // contexts
    pub start: Style,
    pub end: Style,
    pub duration: Style,
    pub gap: Style,
    pub key: Style,
    pub value: Style,
    // moods
    pub info: Style,
    pub ask: Style,
    pub good: Style,
    pub bad: Style,
    pub old: Style,
    pub weird: Style,
    pub underline: Style,
}

impl Styles {
    pub fn new() -> Self {
        Self::from_palette(Palette::default())
    }

    pub fn from_palette(p: Palette) -> Self {
        Styles {
            time: p.green.bold(),
            date: p.cyan.normal(),
            path: p.green.normal(),

            start: p.green.bold(),
            end: p.green.normal(),
            duration: p.blue.normal(),
            gap: p.magenta.normal(),

            key: p.blue.normal(),
            value: p.blue.bold(),

            info: p.blue.bold(),
            ask: Style::new().bold(),
            good: p.green.bold(),
            bad: p.red.bold(),
            old: Color::Fixed(244).normal(), // some kinda grey
            weird: p.magenta.bold(),
            underline: Style::new().underline(),
        }
    }

    pub fn unstyled() -> Self {
        Self::normal()
    }

    pub fn normal() -> Self {
        Styles {
            time: Style::new(),
            date: Style::new(),
            path: Style::new(),

            start: Style::new(),
            end: Style::new(),
            duration: Style::new(),
            gap: Style::new(),

            key: Style::new(),
            value: Style::new(),

            info: Style::new(),
            ask: Style::new(),
            good: Style::new(),
            bad: Style::new(),
            old: Style::new(),
            weird: Style::new(),
            underline: Style::new(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Size {
    pub w: u16,
    pub h: u16,
}

impl Size {
    pub fn zero() -> Self {
        Size { w: 0, h: 0 }
    }
}

/// Terminal formatting object thing. Mostly for colours.
///
/// This type is a bit stateful, like it cares about the current time. Maybe in the
/// future it will care about your current configuration and terminal properties. So the
/// longer you hold on to this the less valid its state might be.
#[derive(Debug, Clone)]
pub struct Imprint<Tz: TimeZone> {
    pub now: chrono::DateTime<Tz>,
    pub style: Styles,
    pub show_gaps: Option<chrono::Duration>,
    pub term_size: Cell<Option<Size>>,
}

impl Imprint<chrono_tz::Tz> {
    pub fn from_im(im: &Impetuous) -> Self {
        Imprint {
            now: im.now_local(),
            style: Styles::new(),
            show_gaps: im.config().show_gap_duration(),
            term_size: Default::default(),
        }
    }
}

/// used to define style functions that don't apply special formatting, only ansi stuff
macro_rules! style_through {
    ( $style:ident ) => {
        // pub fn $style<'a, I>(&self, i: I) -> impl fmt::Display + 'a
        pub fn $style<'a, I>(&self, i: I) -> ansi_term::ANSIString<'a>
        where
            I: Into<std::borrow::Cow<'a, str>>,
        {
            self.style.$style.paint(i)
        }
    };
}

impl<Tz> Imprint<Tz>
where
    Tz: TimeZone,
    <Tz as TimeZone>::Offset: Copy,
{
    pub fn tz(&self) -> Tz {
        self.now.timezone()
    }

    pub fn unstyled(self) -> Self {
        Imprint {
            style: Styles::normal(),
            ..self
        }
    }

    pub fn stdout_size(&self) -> Size {
        if let Some(size) = self.term_size.get() {
            return size;
        }

        let size = match termion::terminal_size() {
            Ok((w, h)) => Size { w, h },
            Err(e) => {
                warn!(crate::logging::please(), "failed to get stdout size: {}", e);
                Size::zero()
            }
        };
        self.term_size.set(Some(size));
        size
    }

    pub fn stdout_width(&self) -> u16 {
        self.stdout_size().w
    }

    pub fn fit_to_stdout<'a>(&self, s: &'a str) -> &'a str {
        &s[..std::cmp::min(s.len(), self.stdout_width() as usize)]
    }

    pub fn spaces_for_stdout(&self) -> &'static str {
        self.fit_to_stdout(PADDING)
    }

    /// keeping in mind, this expects items are newest to oldest
    pub fn print_doings(&self, doings: &[domain::Doing]) {
        let mut last_date: chrono::Date<Local> = self.now.with_timezone(&chrono::Local).date();
        let mut last_start: Option<DateTime> = None;

        for doing in doings.iter() {
            let new_date = doing
                .start
                .map(|dt| dt.with_timezone(&chrono::Local).date())
                .filter(|date| date != &last_date);

            // Between doings we might show a new date if the date changes,
            // or a gap of not doing anything if they're on the same day.
            if let Some(new_date) = new_date {
                // Show the date on a new line ...
                last_date = new_date;
                let fmt = last_date.format("%A, %B %-d %Y");
                println!("{}", self.style.date.paint(fmt.to_string()));
            } else if let Some(show_gaps) = self.show_gaps {
                // Maybe show a gap between the previous doing on the same day
                if let (Some(Some(end)), Some(last_start)) = (*doing.end, last_start) {
                    let gap = *last_start - *end;
                    if gap >= show_gaps {
                        let fmt = format!("({})", format_duration_hms(gap));
                        println!("{:>9}", self.style.gap.paint(fmt));
                    }
                }
            }

            println!("{}", self.doing(doing));
            last_start = *doing.start;
        }
    }

    pub fn doing(&self, doing: &domain::Doing) -> impl fmt::Display {
        let start = format_time_opt(*doing.start);
        let duration = format_duration(
            *doing.start,
            (*doing.end).map(|end| end.unwrap_or(self.now.into())),
        );
        let end = format_time_opt((*doing.end).flatten());
        let end_date = DisplayOrElse(
            match (*doing.start, doing.end.flatten()) {
                (Some(start), Some(end))
                    if start.with_timezone(&self.tz()).date()
                        != end.with_timezone(&self.tz()).date() =>
                {
                    Some(format!("{} ", format_date(end)))
                }
                _ => None,
            },
            "",
        );
        let text = doing
            .note
            .as_ref()
            .and_then(|n| n.text.as_ref())
            .map(|t| t.as_str())
            .unwrap_or("???");

        let postings = doing
            .postings
            .as_ref()
            .map(|postings| {
                postings
                    .iter()
                    .map(|p| format!(" {}", &self.posting(p).to_string()))
                    .collect::<String>()
            })
            .unwrap_or_default();

        format!(
            "{} / {} / {} {} {}{}",
            self.style.start.paint(format!("{:>8}", start)),
            // TODO the format thing here can't go above because ansi_term is a dumb
            self.style.duration.paint(format!("{:>9}", duration)),
            self.style.end.paint(format!("{:>8}", end)),
            self.style.date.paint(end_date.to_string()),
            text,
            postings,
        )
    }

    pub fn posting(&self, posting: &domain::Posting) -> impl fmt::Display {
        let symbol =
            self.posting_status(posting.status.unwrap_or(domain::PostingStatus::Unknown(0)));
        let entity = DisplayOrElse(posting.entity.as_ref(), "???");
        format!("{}{}", symbol, entity)
    }

    pub fn posting_status(&self, status: domain::PostingStatus) -> impl fmt::Display {
        use domain::PostingStatus::*;
        match status {
            Unknown(_) => self.weird("?"),
            New => self.info("*"),
            Pending => self.weird("~"),
            Success => self.good("+"),
            Failed => self.bad("!"),
        }
    }

    pub fn date(&self, dt: DateTime) -> impl fmt::Display {
        self.style.date.paint(format_date(dt).to_string())
    }

    pub fn time(&self, dt: DateTime) -> impl fmt::Display {
        self.style.time.paint(format_time(dt).to_string())
    }

    pub fn chrono_duration(&self, d: Duration) -> impl fmt::Display {
        self.style
            .duration
            .paint(format_duration_hms(d).to_string())
    }

    pub fn path<P: AsRef<std::path::Path>>(&self, path: P) -> impl fmt::Display {
        self.style.path.paint(path.as_ref().display().to_string())
    }

    pub fn start_opt(&self, start: Option<DateTime>) -> impl fmt::Display {
        let start = crate::cli::fmt::format_time_opt(start);
        self.style.start.paint(format!("{:>8}", start))
    }

    /// hms formatted and styled duration
    pub fn duration_opt(&self, dur: Option<chrono::Duration>) -> impl fmt::Display {
        let disp = DisplayOrElse(dur.map(format_duration_hms), "...");
        self.style.duration.paint(format!("{:>9}", disp))
    }

    style_through!(start);
    style_through!(end);
    style_through!(key);
    style_through!(value);
    style_through!(info);
    style_through!(ask);
    style_through!(good);
    style_through!(bad);
    style_through!(old);
    style_through!(weird);
    style_through!(underline);
}

pub(crate) fn format_time(dt: DateTime) -> impl fmt::Display {
    let local: chrono::DateTime<chrono::Local> = dt.into_chrono().into();
    local.format("%-H:%M:%S")
}

pub(crate) fn format_date(dt: DateTime) -> impl fmt::Display {
    let local: chrono::DateTime<chrono::Local> = dt.into_chrono().into();
    local.format("%Y-%m-%d")
}

pub(crate) fn format_time_opt(dt: Option<DateTime>) -> impl fmt::Display {
    DisplayOrElse(dt.map(format_time), "--:--:--")
}

pub(crate) fn format_date_opt(dt: Option<DateTime>) -> impl fmt::Display {
    DisplayOrElse(dt.map(format_date), "??-??-??")
}

pub(crate) fn format_duration(start: Option<DateTime>, end: Option<DateTime>) -> impl fmt::Display {
    let disp = if let (Some(end), Some(start)) = (end, start) {
        let duration = end.signed_duration_since(*start);
        Some(format_duration_hms(duration))
    } else {
        None
    };
    DisplayOrElse(disp, "...")
}

pub(crate) fn format_duration_hms(duration: Duration) -> impl fmt::Display {
    let (sign, s) = match duration.num_seconds() {
        s if s < 0 => (-1, -s),
        s => (1, s),
    };

    let (m, s) = divmod(s, 60);
    if m == 0 {
        return format!("{}s", sign * s);
    }

    let (h, m) = divmod(m, 60);
    if h == 0 {
        return format!("{}m{:>2}s", sign * m, s);
    }

    return format!("{}h{:>2}m{:>2}s", sign * h, m, s);
}

fn divmod(n: i64, m: i64) -> (i64, i64) {
    assert!(m != 0);
    (n / m, n % m)
}

pub struct Progress {
    pub current: usize,
    total: usize,
    total_str: String,
}

impl Progress {
    pub fn new(total: usize) -> Self {
        Progress {
            current: 0,
            total,
            total_str: total.to_string(),
        }
    }

    pub fn total(&self) -> usize {
        self.total
    }

    pub fn set_remaining(&mut self, remaining: usize) {
        self.current = self.total.checked_sub(remaining).unwrap_or(0);
    }

    pub fn display<Tz>(&self, imprint: &Imprint<Tz>) -> impl fmt::Display
    where
        Tz: TimeZone,
        <Tz as TimeZone>::Offset: Copy,
    {
        let current = self.current.to_string();
        let padding = if let Some(n) = self.total_str.len().checked_sub(current.len()) {
            &PADDING[..std::cmp::min(n, PADDING.len())]
        } else {
            ""
        };
        format!(
            "{}{} of {}",
            padding,
            imprint.info(current),
            imprint.info(&self.total_str)
        )
    }
}
