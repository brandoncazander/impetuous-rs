use std::fmt::Display;
use std::str::FromStr;

use serde::{Deserialize, Deserializer, Serialize, Serializer};

pub mod fromstr {
    use super::*;

    pub fn serialize<S, T>(t: &T, er: S) -> Result<S::Ok, S::Error>
    where
        T: Display,
        S: Serializer,
    {
        t.to_string().serialize(er)
    }

    pub fn deserialize<'de, D, T>(deserializer: D) -> Result<T, D::Error>
    where
        T: FromStr,
        <T as FromStr>::Err: Display,
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        FromStr::from_str(&s).map_err(serde::de::Error::custom)
    }
}

/// helper thingy for deserializables when we don't care about other data formats
pub trait FromToml {
    fn from_toml(_: &str) -> Result<Self, toml::de::Error>
    where
        Self: Sized;

    fn from_value(_: toml::Value) -> Result<Self, toml::de::Error>
    where
        Self: Sized;
}

impl<T> FromToml for T
where
    T: serde::de::DeserializeOwned,
{
    fn from_toml(s: &str) -> Result<Self, toml::de::Error>
    where
        Self: Sized,
    {
        toml::from_str(s)
    }

    fn from_value(toml: toml::Value) -> Result<Self, toml::de::Error>
    where
        Self: Sized,
    {
        toml.try_into()
    }
}

/// helper thingy for serializables when we don't care about other data formats
pub trait ToToml {
    fn to_toml(&self) -> Result<String, toml::ser::Error>;
}

impl<T> ToToml for T
where
    T: Serialize,
{
    fn to_toml(&self) -> Result<String, toml::ser::Error> {
        toml::to_string_pretty(self)
    }
}
