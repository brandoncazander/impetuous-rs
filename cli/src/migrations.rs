/// very basic, non-reversable, database migrations for impetuous
use std::iter::Iterator;

use rusqlite::{
    // backup::{Backup, Progress},
    params,
    Connection,
    Transaction,
    NO_PARAMS,
};

pub static MIGRATIONS_SCHEMA: &'static str = r#"
CREATE TABLE "__migrations" (
    "id"        integer primary key,
    "version"   text    not null,
    "timestamp" text    not null default current_timestamp
);"#;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("database error")]
    Db(#[from] rusqlite::Error),
    #[error(
        "The database version {0:?} is not known. Are you using an older version of impetuous?"
    )]
    UnknownVersion(Option<String>),
}

type Result<T, E = Error> = std::result::Result<T, E>;

pub struct Migration<S>
where
    S: AsRef<str>,
{
    version: S,
    sql: S,
}

impl<S> Migration<S>
where
    S: AsRef<str>,
{
    pub fn new(version: S, sql: S) -> Self {
        Migration { version, sql }
    }

    fn apply(&self, tx: &Transaction) -> rusqlite::Result<()> {
        tx.execute_batch(self.sql.as_ref())?;
        tx.execute(
            r#"INSERT INTO "__migrations" ("version") VALUES (?)"#,
            params![self.version.as_ref()],
        )?;
        Ok(())
    }
}

/// Check the database's migration table and return the last migration recorded.
pub fn current_version(conn: &Connection) -> rusqlite::Result<String> {
    // TODO This won't work if ids are reused ...
    // that can happen if migration rows are deleted ...
    conn.query_row(
        r#"SELECT "version" FROM "__migrations" ORDER BY "id" DESC LIMIT 1"#,
        NO_PARAMS,
        |row| row.get::<_, String>(0),
    )
}

/// Check that the database has a table for migrations that we recognize, return a
/// MigrationRequired object if migration must be run before the database can be used.
pub fn needs_migrations<'c, 'm>(
    conn: &'c Connection,
    migrations: &'m [Migration<&'static str>],
) -> Result<Option<MigrationRequired<'m>>> {
    match current_version(conn) {
        Ok(version) => {
            let (idx, _) = migrations
                .iter()
                .enumerate()
                .rev()
                .find(|(_, m)| m.version == version)
                .ok_or(Error::UnknownVersion(Some(version.to_owned())))?;
            let (_, todo) = migrations.split_at(idx + 1);
            if todo.is_empty() {
                Ok(None)
            } else {
                Ok(Some(MigrationRequired { migrations: todo }))
            }
        }
        Err(rusqlite::Error::QueryReturnedNoRows) => Err(Error::UnknownVersion(None)),
        Err(rusqlite::Error::SqliteFailure(_, Some(msg))) if msg.contains("no such table") => {
            // probably a new database ... (or we're ruining someone else's...)
            // TODO, actually distinguish between the two above ...
            Ok(Some(MigrationRequired { migrations }))
        }
        Err(e) => Err(e.into()),
    }
}

/// A slice of migrations that need to be run.
pub struct MigrationRequired<'a> {
    migrations: &'a [Migration<&'static str>],
}

impl<'a> MigrationRequired<'a> {
    pub fn display(&self) -> impl std::fmt::Display {
        self.migrations
            .iter()
            .map(|m| m.version)
            .collect::<Vec<_>>()
            .join(" -> ")
    }

    /// From a transaction, create an [Iterator] that applies these migrations.
    pub fn start<'t>(self, tx: &'a Transaction<'t>) -> MigrationStarted<'a, 't> {
        MigrationStarted {
            tx,
            at: 0,
            migrations: self.migrations,
        }
    }
}

impl<'a> From<&'a [Migration<&'static str>]> for MigrationRequired<'a> {
    fn from(migrations: &'a [Migration<&'static str>]) -> Self {
        MigrationRequired { migrations }
    }
}

impl<'a> std::ops::Deref for MigrationRequired<'a> {
    type Target = [Migration<&'static str>];

    fn deref(&self) -> &Self::Target {
        self.migrations
    }
}

/// Implements [Iterator] which applies one migration for each iteration.
/// The iterator item a pair of the migration version and migration result.
pub struct MigrationStarted<'a, 't> {
    tx: &'a Transaction<'t>,
    at: usize,
    migrations: &'a [Migration<&'static str>],
}

impl<'a, 't> Iterator for MigrationStarted<'a, 't> {
    type Item = (&'static str, rusqlite::Result<()>);

    fn next(&mut self) -> Option<Self::Item> {
        let migration = self.migrations.get(self.at)?;
        self.at += 1;
        Some((migration.version, migration.apply(&self.tx)))
    }
}

// /// Perform a backup of a database with an existing connection to the source.
// ///
// /// See also: https://docs.rs/rusqlite/0.20.0/rusqlite/backup/index.html
// pub fn online_backup<P: AsRef<Path>>(
//     src: &Connection,
//     dst: P,
//     progress: fn(Progress),
// ) -> rusqlite::Result<()> {
//     let mut dst = Connection::open(dst)?;
//     let backup = Backup::new(src, &mut dst)?;
//     backup.run_to_completion(5, std::time::Duration::from_millis(100), Some(progress))
// }
