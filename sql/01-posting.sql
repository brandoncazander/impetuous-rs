-- Worklogs for Jira or Derpdesk or whatever
--
-- This is a bit awkward to model for two reasons:
--
-- 1. Configuration for things we can post to/integrations, lives outside of the
--    database so we fall out of sync with it.
--
--    The table has a "source" column which matches "url" in the section for whatever
--    integration the posting is for.
--
-- 2. Postings that aren't "New" (so they have been or are trying to be submitted)
--    should probably always be kept around even if the tag that brought them into
--    existence isn't anymore. Normally, unposted postings are cleaned up if the content
--    they matched is removed.

create table "postings" (
    "id"        integer     primary key,
    "uuid"      blob        not null default (uuid_generate_v4()),

    -- or just reference a doing_participants ?
    -- "identity"  integer not null references "identities",

    -- TODO: the `on delete cascade` here is quite unfortunate...
    -- - if we failed to post time, deleting this is fine
    -- - if time was posted successfully, deleting this might be inadvertent data loss
    -- - if if this is pending, we probably don't want to post it unless it's stuck, and
    --   there needs to be a way to unstuck it ...
    "doing"     integer     not null references "doings" on delete cascade,

    -- not used in the application, only used by the trigger below
    "doing_tag" integer     references "doing_tags" on delete set null,

    -- eh ... a way to associate postings to some integration in the user's
    -- configuration file? probably this is a URL to the service that we're posting to
    -- or something, so if this ever supports multi-user stuff, postings from different
    -- people to the same service can be connected
    "source"    text        not null,
    -- from the tag value ...
    "entity"    blob        not null,

    "created_at" integer    not null default (utc_now()),

    -- New => 0,
    -- Pending => 1,
    -- Success => 2,
    -- Failed => -1,
    "status"    integer     not null,
    -- a string accompanying the above
    "detail"    text        not null default ''
    -- todo add a content-type for "detail"? because rich whatevers?

    constraint "postings__unposted_has_content" check ("status" != 0 or "doing_tag" is not null)
);

create unique index "postings__doing_source_entity" on "postings" ("doing", "source", "entity");

create index "postings__doing_tag" on "postings" ("doing_tag");

      -- When a tag is no longer associated to a doing, remove any New postings for it
      --
      -- This is half of the stuff that maintains postings for doings. The other half is
      -- in the application as it uses state from outside the database.
      create trigger "delete_new_postings_on_tag_disassociation"
    before delete on "doing_tags"
        for each row
    begin
         delete from "postings"
               where "doing_tag" = "old"."id"
                 and "status" = 0
            ;
    end;
