Name:           impetuous
Version:        0.3.6
Release:        0%{?dist}
Summary:        Impetuous
URL:            https://gitlab.com/sqwishy/impetuous-rs

License:        Apache-2.0
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  sqlite-devel openssl-devel cargo rust
# todo figure out the minimum sqlite version required
Requires:       sqlite-devel

%description

%prep
%setup

%build
CARGO_HOME=node_modules cargo build \
    --manifest-path cli/Cargo.toml \
    --release \
    --no-default-features \
    --offline \
    %{?_smp_mflags}

%install
mkdir -p %{buildroot}/usr/bin/
install -m 0755 cli/target/release/im %{buildroot}/usr/bin/im

%files
/usr/bin/im

%license
%doc
%changelog
